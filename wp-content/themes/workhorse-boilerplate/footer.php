<?php global $bloginfo; ?></section><!-- #main -->

<?php if(!is_page(70)){ ?>
<footer role="contentinfo">
	<div class="inner">		
		<div class="logo">
			<a href="http://www.lennarmultifamilycommunities.com/" title="Lennar Properties" target="_blank">
				<img alt="<?php echo $blog_title = get_bloginfo('name'); ?>" src="<?php the_field('footer_logo','option'); ?>" />
				<h4> A LENNAR COMMUNITY</h4>
			</a>
			<a style="text-decoration: none; color: inherit;" href="https://goo.gl/maps/EhuLoHav9NG2" target="_blank"><?php echo get_field('street_address','option'); ?></a><br />
			<a style="text-decoration: none; color: inherit;" href="https://goo.gl/maps/EhuLoHav9NG2" target="_blank"><?php echo get_field('city_st_zip','option'); ?></a><br />
			<a style="text-decoration: none; color: inherit;" href="tel:8559120506" target="_blank"><?php echo get_field('phone_#','option'); ?></a><br />
		</div>
		<div class="footer-left-menu">
			<nav id="footer-left-menu" role="navigation">
				<?php wp_nav_menu( array('menu' => 'Footer Left Menu' )); ?>
			</nav><!-- #access -->
		</div>
		
		<div class="footer-right-menu">
			<nav id="footer-right-menu" role="navigation">
				<?php wp_nav_menu( array('menu' => 'Footer Right Menu' )); ?>
			</nav><!-- #access -->		
		</div>
		
		<div class="social-menu">
			<?php spitOutSocial(get_field('social_links','option')); ?>	
		</div>
		
		<div class="equal-housing">
			<img alt="Equal Housing Opportunity" src="/wp-content/themes/workhorse-boilerplate/_images/global/logo-eqh.png" /><br>
			<div class="threshold">
			<a href="http://thresholdagency.com/" target="_blank"><img alt="Threshold Agency" src="/wp-content/themes/workhorse-boilerplate/_images/global/logo-threshold.png" /></a>	
			
			</div>
		
            <div class="privacy-menu">
                    <?php wp_nav_menu( array('menu' => 'Privacy Menu' )); ?>
            </div>


             <a target="_blank" href="http://www.lmclovespets.com/"><img style="margin-top: 10px;" src="/wp-content/uploads/2017/01/pet_friendly.png"></a>
            		
		</div>
        
        <div class="copyright" style="padding-top: 60px;">
               
                &copy; <?php echo date('Y'); ?> <?php echo $bloginfo['name']; ?>. All rights reserved. 
         <p class="fine-print">
			The owner and management company for this community comply fully with the provisions of the equal housing opportunity laws and nondiscrimination laws. The apartment homes have been designed and constructed to be accessible in accordance with those laws.
			</p>
			<?php //echo $bloginfo['real_page'];?>
        </div>	
		
		
	</div>			<!-- inner -->
</footer><!-- footer -->
<?php  ;} ?>
<script>
window.theID = "<?php the_ID(); ?>";
window.templateURL = "<?php echo $bloginfo['template_url'] ?>";
window.popupIMG = "<?php echo the_field('popup_image', 'option'); ?>";
window.popupURL = "<?php echo the_field('popup_links_to', 'option'); ?>";
window.popupLinkTarget = '<?php if (get_field('popup_link_target', 'option')) { echo ' target="_blank"' ;}; ?>';
window.popupMobile = <?php if(get_field('hide_on_mobile', 'option')){echo "1";}else{echo "0";}; ?>;
window.cookieExpires = "<?php the_field('cookie_lasts_for', 'option'); ?>";
window.cookieDisabled = <?php if(get_field('disable_cookie', 'option')){echo "1";}else{echo "0";}; ?>;

window.rentersVoiceURL = "<?php echo the_field('renters_voice', 'option'); ?>";


</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>-->
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> -->
<script src="<?php echo $bloginfo['template_url']; ?>/circle-slider/js/jquery.circular-carousel.js"></script> 
<script src="<?php echo $bloginfo['template_url']; ?>/circle-slider/js/script.js"></script>

<script src="<?php echo $bloginfo['template_url']; ?>/_js/fastclick.js"></script>
<script type="text/javascript" src="<?php echo $bloginfo['template_url']; ?>/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
<!--<script src="<?php echo $bloginfo['template_url']; ?>/_js/bx.js"></script>-->
<script src="<?php echo $bloginfo['template_url']; ?>/_js/js.js"></script>

<!--<script src="<?php //echo $bloginfo['template_url']; ?>/slider/jquery-1.9.1.min.js"></script>-->
<script src="<?php echo $bloginfo['template_url']; ?>/slider/owl.carousel.js"></script>

<script src="<?php echo $bloginfo['template_url']; ?>/_js/pikaday.js"></script>
<script type="text/javascript" src="<?php echo $bloginfo['template_url']; ?>/_js/popup-script.js"></script>
<script type="text/javascript" src="<?php echo $bloginfo['template_url']; ?>/_js/apt-popup.js"></script>
<!-- 945=> Park Residence ID , 350 => Glencoer Suite Selector ID , 68=> Glencoe Residence ID -->
<?php if( is_page(945) || is_page(350) || is_page(68) ) { ?>
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<?php } ?>
<script>

    var picker = new Pikaday({ field: document.getElementById('appointment_date') });
    var picker1 = new Pikaday({ 
    	field: document.getElementById('appointment_date_new'),
    	onSelect: function(date) {
        	var s = String(date);
        	var chosenDate = s.slice(0, 3).toLowerCase();
        	$('.time-options select option').hide().prop('disabled', 'disabled');
        	$('.time-options select option[data-day="'+chosenDate+'"]').show().prop('disabled', false);
        	//$('.time-options select').hide().prop('disabled', 'disabled');
        	$('#desired_tour_time').show();
        	$('#desired_tour_time').val( 'No Time Selected' );
    	}
    });
    var picker2 = new Pikaday({ field: document.getElementById('planned_move_in_date') });

</script>


        
    <!-- Demo -->


      

</section>


<script>

	
$(document).ready(function() {
	
	function openRealP(){
		
		
		$.getScript( 'https://property.onesite.realpage.com/oll/eol/widget?siteId=<?php echo $bloginfo['real_page'] ?>&container=availabilityWidget', function( data, textStatus, jqxhr ) {
        // do some stuff after script is loaded
    	} );	
		
		$('#openUpThisOne').fadeIn();	
	}
	
	function openCallAvail(){
		
		
		$("#widget-holder").load("/apply/ .entry-content");
		$("#openUpThisOne").addClass('mobile-widget');
			
		
		$('#openUpThisOne').fadeIn();	
	}
	

	$(".various, #menu-item-279 a, .open-apply").click(function(event){
			event.preventDefault();
			//window.history.replaceState( {} , 'Apply Widget', '/test/?MoveInDate=03/15/2015&UnitId=1206&SearchUrl=http://crestatglencoe.com/test/' );
				var windowWidth = $(window).width();
				if (windowWidth>500){
					openRealP();
					} else {
					openCallAvail();
					}	
	})
	
	$('#openUpThisOne').click(function(){
		
			$('#openUpThisOne').fadeOut();
		
	});
	
	<?php if(isset($_GET['rp-oll-page'])){ ?>
		
		openRealP();
		
	<?php }
	?>
});

</script>


<?php wp_footer(); ?>

</div> <!-- End #all da_site-->
</body>
</html>
