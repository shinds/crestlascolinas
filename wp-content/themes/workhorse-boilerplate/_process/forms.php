<?php

	
require '../../../../wp-load.php';
global $bloginfo;

if(isset($_POST['fill_this_out']) && $_POST['fill_this_out']==""){
	

	$message = "";
	
	foreach($_POST as $key => $value)
		{
			if($key!="fill_this_out" && $key!="page_id"){
				
				$message .= str_replace('_',' ',strtoupper($key)) .": ".$value."\n";
			
			}
		}
	
	$sendto = get_field('send_email_to',$_POST['page_id']);
	
	$headers = 'From: '.get_bloginfo('name').' Contact Form <no-reply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n";
	wp_mail($sendto, 'Email From '.get_bloginfo('name'). ' Contact Form', $message, $headers );
	
	$post = array(
		'post_title'	=> $_POST['form-title'].': '.$_POST['email'],
		'post_content'	=> $message,
		'post_status'	=> 'private',
		'post_type'	=> 'contact_form'  // Use a custom post type if you want to
	);
	
	$post_id = wp_insert_post($post);
 	
	echo get_field('thank_you_message',$_POST['page_id']);
	
}
	?>