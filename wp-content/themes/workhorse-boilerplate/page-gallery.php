<?php
/**
 * Template Name: Page Gallery
 */
get_header(); 
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<?php
	if(get_field('photo'))
	{
	?>
		<div id="owl-demo" class="owl-carousel owl-theme">
  			<?php
  			while(has_sub_field('photo'))
  			{
     			if(get_sub_field('image'))
 				{?>
					<div class="item" style="background-image: url('<?php echo get_sub_field('image'); ?>'); "></div>			 
 				<?php
     			}
  			}
  			?>
  		</div>
	<?php
	}
?>
    <div class="extra-padding">            
        <div id="content" style="padding-top:0px;">
            <div class="inner">
	            <h4 class="gallery-title"><?php the_content(); ?></h4>
            </div>
		</div>
	</div><!--extra-padding-->
	<div class="clearfix"></div>
<?php endwhile; ?>

<?php get_footer(); ?>