    <?php
/**
 * Template Name: Suite Selector
 */ 
get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="inner">
    <div id="suite-selector" class="clearfix">
        <div class="ss-left equalheight">
            <div id="suite-selector-menu">
                <ul>
                    <li class="ss-main bed"><span class="title">0+beds</span> <span class="fa down">&#xf0d7;</span><span class="fa up">&#xf0d8;</span>
                        <ul class="sub-bed" >
                            <li><input type="checkbox" id="bed1" name="bed" value="1" /><label for="bed1"><span>1</span></label></li>
                            <li><input type="checkbox" id="bed2" name="bed" value="2" /><label for="bed2"><span>2</span></label></li>
                        </ul>
                    </li>
                    <li class="ss-main bath"><span class="title">baths</span><span class="fa down">&#xf0d7;</span><span class="fa up">&#xf0d8;</span>
                        <ul class ="sub-bath" >
                            <li><input type="checkbox" id="bath1" name="bath" value="1" /><label for="bath1"><span>1</span></label></li>
                            <li><input type="checkbox" id="bath2" name="bath" value="2" /><label for="bath2"><span>2</span></label></li>
                        </ul>
                    </li>
                    <li class="ss-main view">view <span class="fa down">&#xf0d7;</span><span class="fa up">&#xf0d8;</span>
                        <ul>
                            <li><input type="checkbox" id="c1" /><label for="c1"><span>pool</span></label><span>pool</span></li>
                            <li><input type="checkbox" id="c2" /><label for="c2"><span>garden</span></label></li>
                            <li><input type="checkbox" id="c3" /><label for="c3"><span>street</span></label></li>
                        </ul>
                    </li>
                    <li class="ss-main price">price <span class="fa down">&#xf0d7;</span><span class="fa up">&#xf0d8;</span>
                        <ul class="sub-price" >
                            <!--<li><input type="checkbox" id="c4" /><label for="c4"><span>$500-$700</span></label></li>
                            <li><input type="checkbox" id="c5" /><label for="c5"><span>$701-$1000</span></label></li>
                            <li><input type="checkbox" id="c6" /><label for="c6"><span>$1001-$1500</span></label></li>-->
                        </ul>
                    </li>
                </ul>
            </div>
            <div id="ss-map">
                <img src="<?php echo $bloginfo['template_url'] ?>/_images/suite-selector/glencoe/site-map.jpg" id="map-image">
                <div id="popup-building-type-1" class="pool building-type-1 building-1 building "></div>
                <div id="popup-building-type-2" class="pool building-type-1 building-2 building"></div>
                <div id="popup-building-type-3" class="pool street building-type-3 building-4 building"></div>

                <div id="popup-building-type-4" class="building-type-2 building-3 building"></div>
                <div id="popup-building-type-5" class="building-type-2 building-5 building"></div>
                <div id="popup-building-type-6" class="building-type-2 building-6 building"></div>

                <div id="building popup-building-type-7" class="pool street building-type-4 building-7"></div>
                <div id="popup-building-type-8" class="pool street building-type-5 building-8 building"></div>
                <div id="popup-building-type-9" class="pool street building-type-6 building-9 building"></div>
                <div id="popup-building-type-10" class="pool building-type-7 building-10 building"></div>
                <div id="popup-building-type-11" class="pool building-type-7 building-11 building"></div>
                <div id="popup-building-type-12" class="pool building-type-7 building-12 building"></div>
            
            <?php
                if(get_field('parent'))
                {
                  $count = 1;
                  while(has_sub_field('parent'))
                  {
                    ?>
                        <div class="popup-building-type-<?php echo $count; ?> popup-sec popup-type-<?php echo $count; ?>">
                            <div class="close"></div>
                            <div class="popup-content">
                                <div class="popup-content-sec clearfix">
                                    <div class="popup-left">
                                        <h3><?php echo get_sub_field('building_name'); ?></h3>
                                        <?php echo get_sub_field('amenities'); ?>
                                    </div>
                                    <div class="popup-right">
                                        <img src="<?php echo get_sub_field('building_image'); ?>" alt="">
                                    </div>
                                </div>
                                <div class="popup-btn"><a class="orange-btn" href="<?php echo get_sub_field('availability_link'); ?>">AVAILABLE APARTMENTS</a></div>
                            </div>
                        </div>
                        <?php
                            $count++;
                  }
                }
            ?>
            
           
                <div class="loader"></div>
   	<div id="backgroundPopup"></div>
                
            </div>
        </div>
        <div class="ss-right equalheight">
            <div id="available-apartments-header">AVAILABLE APARTMENTS <a class="view-all" href="/apartment-listings/">VIEW ALL</a></div>
            <div class="content mCustomScrollbar appartment-list-sec">
				<ul class="appartment-list" id="list-apart">
                    <!--<li><span>Building 2, apt. #237, 2nd floor</span> 2 beds, 2 baths, 1,200 sqft Starting at $1,600 <a href="#">Pre-lease now</a></li>
                    <li><span>Building 2, apt. #237, 2nd floor</span> 2 beds, 2 baths, 1,200 sqft Starting at $1,600 <a href="#">Pre-lease now</a></li>
                    <li><span>Building 2, apt. #237, 2nd floor</span> 2 beds, 2 baths, 1,200 sqft Starting at $1,600 <a href="#">Pre-lease now</a></li>
                    <li><span>Building 2, apt. #237, 2nd floor</span> 2 beds, 2 baths, 1,200 sqft Starting at $1,600 <a href="#">Pre-lease now</a></li>
                    <li><span>Building 2, apt. #237, 2nd floor</span> 2 beds, 2 baths, 1,200 sqft Starting at $1,600 <a href="#">Pre-lease now</a></li>
                    <li><span>Building 2, apt. #237, 2nd floor</span> 2 beds, 2 baths, 1,200 sqft Starting at $1,600 <a href="#">Pre-lease now</a></li>
                    <li><span>Building 2, apt. #237, 2nd floor</span> 2 beds, 2 baths, 1,200 sqft Starting at $1,600 <a href="#">Pre-lease now</a></li>
                    <li><span>Building 2, apt. #237, 2nd floor</span> 2 beds, 2 baths, 1,200 sqft Starting at $1,600 <a href="#">Pre-lease now</a></li>
                    <li><span>Building 2, apt. #237, 2nd floor</span> 2 beds, 2 baths, 1,200 sqft Starting at $1,600 <a href="#">Pre-lease now</a></li>
                    <li><span>Building 2, apt. #237, 2nd floor</span> 2 beds, 2 baths, 1,200 sqft Starting at $1,600 <a href="#">Pre-lease now</a></li>-->
                </ul>
			</div>
        </div>
    </div><!-- /suite-selector-->
</div> 
     
    
    
<?php endwhile; ?>                
<?php get_footer(); ?>	


<script>
    $(document).ready(function(){

        
        /* Binding Pricelist using PickList method of RealPage */
        $.ajax({
            method: "POST",
            url: "http://crestatglencoe.com/wp-content/themes/workhorse-boilerplate/soaplist.php",
            beforeSend: function(){
               
            }
          }).done(function(priceback) {
            alert(priceback);
              xmlDoc = $.parseXML( priceback ),
              $xml = $( xmlDoc ),
              $UnitObject = $xml.find( "PicklistItem" );
              $( "li.price ul.sub-price" ).empty();
              $UnitObject.each(function(index, element) {
                $( "li.price ul.sub-price" ).append('<li><input name="price" id="'+$(element).find('Value').text()+'"  value="'+$(element).find('Value').text()+'"  type="checkbox" ><label for="'+$(element).find('Value').text()+'"><span>'+$(element).find('Text').text()+'</span></label></li>');
            });
          });   
          /* Binding Pricelist using PickList method of RealPage (over) */
        
        

        $("ul.sub-bed li input:checkbox").on('click', function() {
            var $box = $(this);
            if ($box.is(":checked")) {
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                $(group).prop("checked", false);
                $box.prop("checked", true);
            } else {
                $box.prop("checked", false);
            }
        });

        $("ul.sub-bath li input:checkbox").on('click', function() {
            var $box = $(this);
            if ($box.is(":checked")) {
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                $(group).prop("checked", false);
                $box.prop("checked", true);
            } else {
                $box.prop("checked", false);
            }
        });

        setTimeout(function(){ 
                alert('bind');
                $("ul.sub-price li input:checkbox").on('click', function() {
                        
                var $box = $(this);
                if ($box.is(":checked")) {
                    var group = "input:checkbox[name='" + $box.attr("name") + "']";
                    $(group).prop("checked", false);
                    $box.prop("checked", true);
                } else {
                    $box.prop("checked", false);
                }
            });
        }, 2000);


       
      var bedVal = 0;
      var bathVal = 0;
      var priceVal = "T000000006";
      jQuery.ajax({
          method: "POST",
          url: "http://crestatglencoe.com/wp-content/themes/workhorse-boilerplate/soap.php",
          data: { bed: bedVal , bath: bathVal, price:priceVal }
        }).done(function(xml) {
            alert(xml);
            //jQuery("#xmlList").html(response);
             xmlDoc = $.parseXML( xml ),
              $xml = $( xmlDoc ),
              $UnitObject = $xml.find( "UnitObject" );
              $( "#list-apart" ).empty();
              $UnitObject.each(function(index, element) {
                $( "#list-apart" ).append('<li><span>'+$(element).find('Address').find('Address1').text()+' , Floor - '+$(element).find('UnitDetails').find('FloorNumber').text()+'</span>'+$(element).find('UnitDetails').find('Bedrooms').text()+' bed , '+$(element).find('UnitDetails').find('Bathrooms').text()+' bath , '+$(element).find('UnitDetails').find('RentSqFtCount').text()+' sqft , '+'Starting at $'+$(element).find('BaseRentAmount').text()+'<a href="#">Pre-lease now</a>'+'</li>');
            });
          });   
         
    
	$("#suite-selector input[type=checkbox]").click(function(){
		
		if($(this).attr('checked')){
			
			$('.'+$(this).attr('rel')).css('opacity','1');
			
		
		}else {
			
			$('.'+$(this).attr('rel')).css('opacity','0');
		}
	})
	
	$("li.ss-main").click(function(){
		$("li.ss-main").removeClass('ss-show-sub');
        $(this).toggleClass('ss-show-sub');	
        $(this).click(function(){ $(this).toggleClass('ss-show-sub');   });
	});	

});/* ready over */
    

    jQuery('ul.sub-bed li input[name=bed]').change(function(){
        var bedVal = jQuery( 'ul.sub-bed li input[name=bed]:checked').val();
        var bathVal = jQuery( 'ul.sub-bath li input[name=bath]:checked').val();
        var priceVal = jQuery( 'ul.sub-price li input[name=price]:checked' ).val();         
        jQuery("li.ss-show-sub").toggleClass('ss-show-sub'); 

        var labbed="";
        if(!bedVal)
        {
            labbed = "0+ Beds";
        }
        else if(bedVal == "1")
        {
            labbed = bedVal+" Bed";
        }
        else
        {
            labbed = bedVal+" Beds";
        }
        jQuery("li.bed span.title").text(labbed);

        if(!bathVal)
        {
            bathVal = 0;
        }
        if(!priceVal)
        {
            priceVal = "T000000001";
        }        
        jQuery.ajax({
          method: "POST",
          url: "http://crestatglencoe.com/wp-content/themes/workhorse-boilerplate/soap.php",
          data: { bed: bedVal , bath: bathVal, price:priceVal}
        }).done(function(xml) {
            alert(xml);
            //jQuery("#xmlList").html(response);
             xmlDoc = $.parseXML( xml ),
              $xml = $( xmlDoc ),
              $UnitObject = $xml.find( "UnitObject" );
              $( "#list-apart" ).empty();
              $UnitObject.each(function(index, element) {
                $( "#list-apart" ).append('<li><span>'+$(element).find('Address').find('Address1').text()+' , Floor - '+$(element).find('UnitDetails').find('FloorNumber').text()+'</span>'+$(element).find('UnitDetails').find('Bedrooms').text()+' bed , '+$(element).find('UnitDetails').find('Bathrooms').text()+' bath , '+$(element).find('UnitDetails').find('RentSqFtCount').text()+' sqft , '+'Starting at $'+$(element).find('BaseRentAmount').text()+'<a href="#">Pre-lease now</a>'+'</li>');
            });
          }); 
    });
    

    jQuery('ul.sub-bath li input[name=bath]').change(function(){
        var bathVal = jQuery( 'ul.sub-bath li input[name=bath]:checked' ).val();
        var bedVal = jQuery( 'ul.sub-bed li input[name=bed]:checked' ).val();        
        var priceVal = jQuery( 'ul.sub-price li input[name=price]:checked' ).val(); 
        jQuery("li.ss-show-sub").toggleClass('ss-show-sub'); 
        
        var labbed="";
        if(!bathVal)
        {
            labbed = "Baths";
        }
        else if(bathVal == "1")
        {
            labbed = bathVal+" Bath";
        }
        else
        {
            labbed = bathVal+" Baths";
        }
        jQuery("li.bath span.title").text(labbed);
       
         

        if(!bedVal)
        {
            bedVal = 0;
        }
        if(!priceVal)
        {
            priceVal = "T000000001";
        }
        jQuery.ajax({
          method: "POST",
          url: "http://crestatglencoe.com/wp-content/themes/workhorse-boilerplate/soap.php",
          data: { bed: bedVal , bath: bathVal, price:priceVal}
        }).done(function(xml) {
            alert(xml);
            //jQuery("#xmlList").html(response);
             xmlDoc = $.parseXML( xml ),
              $xml = $( xmlDoc ),
              $UnitObject = $xml.find( "UnitObject" );
              $( "#list-apart" ).empty();
              $UnitObject.each(function(index, element) {
                $( "#list-apart" ).append('<li><span>'+$(element).find('Address').find('Address1').text()+' , Floor - '+$(element).find('UnitDetails').find('FloorNumber').text()+'</span>'+$(element).find('UnitDetails').find('Bedrooms').text()+' bed , '+$(element).find('UnitDetails').find('Bathrooms').text()+' bath , '+$(element).find('UnitDetails').find('RentSqFtCount').text()+' sqft , '+'Starting at $'+$(element).find('BaseRentAmount').text()+'<a href="#">Pre-lease now</a>'+'</li>');
            });
          }); 
    });
    

    
    setTimeout(function(){ 
        jQuery('ul.sub-price li input[name=price]').change(function(){
            var priceVal = jQuery( 'ul.sub-price li input[name=price]:checked' ).val();
            var bathVal = jQuery( 'ul.sub-bath li input[name=bath]:checked' ).val();
            var bedVal = jQuery( 'ul.sub-bed li input[name=bed]:checked' ).val();
            jQuery("li.ss-show-sub").toggleClass('ss-show-sub');    
            if(typeof(priceVal)=="undefined") 
            {
                priceVal="T000000001";
            }

            if(typeof(bathVal)=="undefined") 
            {
                bathVal=0;
            }
            
            if(typeof(bedVal)=="undefined") 
            {
                bedVal=0;
            } 
            jQuery.ajax({
                method: "POST",
                url: "http://crestatglencoe.com/wp-content/themes/workhorse-boilerplate/soap.php",
                data: { bed: bedVal , bath: bathVal, price:priceVal}
                }).done(function(xml) {
                    alert(xml);
                    //jQuery("#xmlList").html(response);
                    xmlDoc = $.parseXML( xml ),
                    $xml = $( xmlDoc ),
                    $UnitObject = $xml.find( "UnitObject" );
                    $( "#list-apart" ).empty();
                    $UnitObject.each(function(index, element) {
                    $( "#list-apart" ).append('<li><span>'+$(element).find('Address').find('Address1').text()+' , Floor - '+$(element).find('UnitDetails').find('FloorNumber').text()+'</span>'+$(element).find('UnitDetails').find('Bedrooms').text()+' bed , '+$(element).find('UnitDetails').find('Bathrooms').text()+' bath , '+$(element).find('UnitDetails').find('RentSqFtCount').text()+' sqft , '+'Starting at $'+$(element).find('BaseRentAmount').text()+'<a href="#">Pre-lease now</a>'+'</li>');
                }); 
          });   
             
        }); 
    }, 3500);
    
    
    
    equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
    equalheight('.equalheight');
    var selectorHeight = $('#suite-selector #map-image').height();
    $('.appartment-list-sec').css("min-height", selectorHeight-30);

});


$(window).resize(function(){
    equalheight('.equalheight');
    var selectorHeight = $('#suite-selector #map-image').height();
    $('.appartment-list-sec').css("min-height", selectorHeight-30);

});
   
    
</script>	


	<script src="<?php bloginfo('template_directory'); ?>/_js/jquery.mCustomScrollbar.concat.min.js"></script>
    
    <?php /**********************************************************************XML Code**********************************************************************/ ?>

    <div id="xmlList"></div>
       