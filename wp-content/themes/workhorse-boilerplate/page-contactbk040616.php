<?php
/**
 	Template Name: Page Contact (backup)
 *

 */
global $bloginfo;
 
if(isset($_POST['fill_this_out']) && $_POST['fill_this_out']==""){
	
	$message = "";
	
	foreach($_POST as $key => $value)
		{
			if($key!="fill_this_out" && $key!="page_id"){
				
				$message .= str_replace('_',' ',strtoupper($key)) .": ".$value."\n\n";
			
			}
		}
	
	$sendto = get_field('send_to',$_POST['page_id']);

	$headers = "From:" . $bloginfo['name'] . " <no-reply@" . $_SERVER['SERVER_NAME']	. ">" . "\r\n" . "Reply-To: " . $_POST["email"] . "\r\n";
	wp_mail($sendto, 'Email From ' . $_POST["full_name"] . ' via Contact Form', $message, $headers );
	
	$post = array(
		'post_title'	=> get_the_title($_POST['page_id']).': '.$_POST['email'],
		'post_content'	=> $message,
		'post_status'	=> 'private',
		'post_type'	=> 'contact_form'  // Use a custom post type if you want to
	);
	
	$post_id = wp_insert_post($post);
 	
	echo get_field('thank_you_message',$_POST['page_id']);
	
} else {

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="extra-padding">

		<div class="inner">
				<article id="post-<?php the_ID(); ?>" class="contact-page">
					<h1 class="entry-title"><?php //the_title(); ?></h1>
					<div class="entry-content">
						<?php the_content(); ?>
                    	<div class="form-container">
							<div id="main-form" class="left">
								<?php 
							$link =  $_SERVER['REQUEST_URI'];
							$link_explode = explode("?",$link);
							wp_redirect('https://crestlascolinas.com/thankyou/'); exit;
							?>
				

								<form id="form_contact" action="<?php echo $link_explode[0] ?>" method="post">
									<div class="tour-left">
					
										<input type="text" name="full_name" id="full_name" placeholder="Name*" class="two-col req" /><input type="text" name="phone" id="phone" placeholder="Phone*"  class="two-col req last-col" />
										<input type="text" name="email" id="email" placeholder="Email*"  class="clearfix req-email" />
										<input type="text" name="subject" id="subject" placeholder="Subject*" class="req" />
										<textarea name="form_message" id="form_message" placeholder="Message*" class="req"></textarea>
										<div class="clearfix"></div>
						
						
										<input type="hidden" name="page_id" id="page_id" value="<?php the_ID(); ?>" />
						
									</div>
									<div class="tour-right">
				  
										<input type="text" id="appointment_date" name="appointment_date" placeholder="Appointment Date" class="datepicker">
		
				
        								<!--<input id="desired_tour_time" type="text" class="time ui-timepicker-input" placeholder="Desired Tour Time" autocomplete="off">
										-->
										<select name="desired_tour_time" id="desired_tour_time">
											<option value="No Time Selected" id="contact-enquirytype-1">Desired Tour Time</option>
                                            <option value="10am" id="contact-enquirytype-3">10 am</option>
                                            <option value="11am" id="contact-enquirytype-3">11 am</option>
                                            <option value="12pm" id="contact-enquirytype-3">12 pm</option>
                                            <option value="1pm" id="contact-enquirytype-3">1 pm</option>
                                            <option value="2pm" id="contact-enquirytype-3">2 pm</option>
                                            <option value="3pm" id="contact-enquirytype-3">3 pm</option>
                                            <option value="4pm" id="contact-enquirytype-3">4 pm</option>
                                            <option value="5pm" id="contact-enquirytype-3">5 pm</option>
										</select>
										<!--<input type="text" id="desired_floor_plan" name="desired_floor_plan" placeholder="Desired Floor Plan" />
										-->
                                        
                                        <?php $tabs_section = get_field('floorplan_type', $bloginfo['floorplans_page']);
										
										//var_dump($tabs_section);
										 ?>
										<select id="desired_floor_plan" name="desired_floor_plan">
                                        
											<option value="No Plan Selected" id="contact-enquirytype-1">Desired Floor Plan</option>
                                            
                                            <?php 
											
											if($tabs_section){
												
												foreach($tabs_section as $plan2){
													
													foreach($plan2['floorplan'] as $plan){
													
													$plan_name = $plan['name']. ' ('.$plan["number_of_bedrooms"].' / '.$plan["number_of_baths"]. ')';
													
													echo '<option value="'.$plan_name.'" class="contact-enquirytype-2">'.$plan_name.'</option>';
													
													}
													
												}
											
											
											}
											
											?>
                                            
										</select>
										
										<input type="text" id="planned_move_in_date" name="planned_move_in_date" placeholder="Planned Move in Date"  class="datepicker" />
								
									</div>
									<div class="clearfix"></div>
                                    <span class="fake-checkbox"><span></span></span>
									<!--input type="checkbox" name="schedule_a_tour" id="scedule_a_tour" /--> I'd like to schedule a tour
									
										<!-- <div class="fine-print"><p>   ( Tours will begin on 11/20. )</p></div>-->
								
									<input type="input" name="fill_this_out" id="fill_this_out" />
									<!--input type="submit" value="Submit" /-->
									<a class="orange-btn sumbit" href="/contact/" >Submit</a>                         
								</form>
                    			<div id="thank-you"></div>
                    			<div class="clearfix"></div>
                    	
                    		</div>
							<div class="right">
								<div id="map_canvas">
									<iframe src="<?php echo get_field('google_map_embed_link','option'); ?>" width="100%" height="250" frameborder="0" style="border:0"></iframe>
								</div>
								<a href="<?php echo get_field('google_map_link','option'); ?>" target="_blank">
									View in Google Maps
								</a>
						
								<?php echo $bloginfo['name'] ; ?><br />
								<?php echo get_field('street_address','option'); ?><br />
								<?php echo get_field('city_st_zip','option'); ?><br />
								<?php echo get_field('phone_#','option'); ?><br />
							</div>
							
							<div class="fine-print">
							
								<p>By submitting this form, you agree that we may communicate with you from time to time about what's happening at our community. We'll never sell your information. Contact leasing office for more information.</p>
            				</div>	
            			</div> <!-- form container -->
            			
            		</div><!-- .entry-content -->
	</article><!-- #post-## -->

</div>
	                
           
<div class="clearfix"></div>
</div>



<?php endwhile; ?>

<?php get_footer();

}?>
