<?php /** * Template Name: Neighborhood Page */ ?>
<?php get_header(); ?>

<?php $loop = new WP_Query( array( 'post_type' => 'hot_spot', 'posts_per_page' => -1 ) ); ?>
<?php 
	$property_longitude = get_field('property_longitude', 'option');
	$property_latitude = get_field('property_latitude', 'option');
	$map_icon = get_field('map_icon', 'option');

	
$locations = array();
$counter = 0;


while ( $loop->have_posts() ) : $loop->the_post();


	$locations[$counter]['longitude']=get_field('longitude');
	$locations[$counter]['latitude']=get_field('latitude');
	$locations[$counter]['street_address']=get_field('street_address');
	$locations[$counter]['city']=get_field('city');
	$locations[$counter]['zip_code']=get_field('zip_code');
	$locations[$counter]['website']=get_field('website');
	$locations[$counter]['title']=get_the_title();
	$locations[$counter]['terms'] = wp_get_post_terms($post->ID, 'hot_spot_categories', array("fields" => "names"));
	$locations[$counter]['terms_ids'] = wp_get_post_terms($post->ID, 'hot_spot_categories', array("fields" => "ids"));
	$locations[$counter]['pin'] = get_field('pin_image', 'hot_spot_categories_'.$locations[$counter]['terms_ids'][0]); 
	$counter++;

endwhile; wp_reset_query();


 ?>

<script src="https://maps.googleapis.com/maps/api/js"></script>


<script>
 function initialize() {
	 
	
	 
	 var iconBase = '<?php echo $map_icon ?>';
	 
	 var myLatlng = new google.maps.LatLng(<?php echo $property_latitude ?>,<?php echo  $property_longitude ?>);
	 
	var myCenterLatlng = new google.maps.LatLng(<?php echo $property_latitude ?>,<?php echo  $property_longitude ?>);
    var myOptions = {
	  zoom: 15,
	  center: myCenterLatlng,
	  mapTypeId: google.maps.MapTypeId.ROADMAP,
	  panControl: true,
	  zoomControl: true,
	  scaleControl: true

	};
	    map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
		marker = new google.maps.Marker({
		position: myLatlng,
		map: map,
		icon: iconBase
		});
		
		
		//alert(iconBase + 'crest.png');
		function isInfoWindowOpen(infoWindow){
    var map = infoWindow.getMap();
    return (map !== null && typeof map !== "undefined");
}
		var infowindow = new Array();
		window.mark = new Array();
		
		function openUp(whichOne){
				
				if (isInfoWindowOpen(infowindow[whichOne])){
					infowindow[whichOne].close(map, mark[whichOne]);
				} else {
					
					for (var i = 0; i < infowindow.length; i++) {
						
						infowindow[i].close(map, mark[whichOne]);
						
					}
					
					infowindow[whichOne].open(map, mark[whichOne]);
					var latLng = mark[whichOne].getPosition(); // returns LatLng object
				
				map.panTo(latLng)
				}
				
			}
		
		<?php if($locations){
			
			foreach($locations as $key => $location){ ?>
				
			var myLatlng<?php echo $key ?> = new google.maps.LatLng(<?php echo $location['latitude'] ?>, <?php echo $location['longitude'] ?>);
			mark[<?php echo $key ?>] = new google.maps.Marker({
				position: myLatlng<?php echo $key ?>,
				map: map,
				title: '<?php echo $location['title'] ?>',
				icon: '<?php echo $location['pin'] ?>'
			});
			
			infowindow[<?php echo $key ?>] = new google.maps.InfoWindow({
				content: '<div  class="info-window"><strong><?php echo $location['title'] ?></strong><br /><?php echo showIf($location['street_address'],'','<br />');
					echo showIf($location['city'],'',', ');
					echo showIf($location['state'],'',' ');
					echo showIf($location['zip_code'],'','<br />');
					echo showIf($location['website'],'<a href="'.$location['website'].'" target="_blank">','</a><br />'); ?></div>',
					maxWidth: 400
			});
			
			
			
			google.maps.event.addListener(mark[<?php echo $key ?>], 'click', function(){
				
				openUp(<?php echo $key ?>);	
				
				
			});
			
			
				
				
			<?php }
			
			
		}
		
		?>
		
		
		
	}
	
	google.maps.event.addDomListener(window, 'load', initialize);
</script>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div id="map_canvas" style="width:100%; height:100%;"></div>
<div id="map_menu">
<div class="map-section-inner">
<?php $terms = get_terms( 'hot_spot_categories'); 




if ($terms) {
	foreach($terms as $term) {
		echo '<div class="map-section">';
		echo '<h3>' . $term->name . '</h3>';
		
		echo '<div class="all-subs">';
		if ($locations) {
			foreach($locations as $key => $location) {
				
				if(in_array($term->name,$location['terms'])){
					
					echo '<div class="map-menu-location" onclick="google.maps.event.trigger(mark['.$key.'], \'click\');"><strong>'.$location['title'].'</strong><br />';
					
					
					
					
					echo showIf($location['street_address'],'','<br />');
					echo showIf($location['city'],'',', ');
					echo showIf($location['state'],'',' ');
					echo showIf($location['zip_code'],'','<br />');
					echo '<a href="https://www.google.com/maps/place//@'.$location['latitude'].','.$location['longitude'].',17z/" class="view-map" target="_blank">View map</a>';
					echo '</div>';
				}
				
			}
			
			echo '</div>';
		}

		echo '</div>';
	}
}
?>


</div>
</div>
<?php endwhile; ?>

<?php get_footer(); ?>

