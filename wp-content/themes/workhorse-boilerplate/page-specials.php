<?php /** * Template Name: Specials Page */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div class="gutter">
	<div class="inner">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>	
					<div class="main-pattern-section featured-page">
						<div class="entry-content clearfix">
							
									<?php //the_content(); ?>
								
								<div class="left specials-side-bar">
									<?php echo the_field('content_left_specials')?>		
								</div>
								<div class="right specials-text">
									<?php echo the_field('content_right_specials')?>		
								</div>
						</div><!-- .entry-content -->
					</div><!-- .main-pattern-section -->
				</article><!-- #post-## -->
                
                </div>
                </div>
<?php endwhile; ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>