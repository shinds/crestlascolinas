<?php
/**
 * Template Name:Colinas Suite Selector
 */ 
get_header(); ?>
	
	<style type="text/css">
		.restore-map a:hover { background-color: #666666; color: #ffffff; }
		.restore-map { bottom: 2%; left: 2%; position: absolute; }
		.restore-map a { background: #d6a282 none repeat scroll 0 0; border-radius: 28px; color: #ffffff; font-family: Arial; font-size: 16px; padding: 5px 10px; text-decoration: none; }
		 
	</style>

	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/colinas-map/style.css" >
  	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<div class="inner">
		    <div id="suite-selector" class="clearfix">
		        <div class="ss-left equalheight">
		            <div id="suite-selector-menu">
		                <ul>
		                    <li class="ss-main bed"><span class="title">0+beds</span> <span class="fa down">&#xf0d7;</span><span class="fa up">&#xf0d8;</span>
		                        <ul class="sub-bed suite-filter" >
		                            <li><input type="checkbox" id="bed1" name="bed" value="1" /><label for="bed1"><span>1</span></label></li>
		                            <li><input type="checkbox" id="bed2" name="bed" value="2" /><label for="bed2"><span>2</span></label></li>
		                        </ul>
		                    </li>
		                    <li class="ss-main bath"><span class="title">0+ baths</span> <span class="fa down">&#xf0d7;</span><span class="fa up">&#xf0d8;</span>
		                        <ul class ="sub-bath suite-filter" >
		                            <li><input type="checkbox" id="bath1" name="bath" value="1" /><label for="bath1"><span>1</span></label></li>
		                            <li><input type="checkbox" id="bath2" name="bath" value="2" /><label for="bath2"><span>2</span></label></li>
		                        </ul>
		                    </li>
		                    <li class="ss-main view"><span class="title">view</span> <span class="fa down">&#xf0d7;</span><span class="fa up">&#xf0d8;</span>
		                        <ul class ="sub-view">
		                            <li><input type="checkbox" name="view" value="pool" id="c1" /><label for="c1"><span>pool</span></label></li>
		                            <li><input type="checkbox" name="view" value="garden" id="c2" /><label for="c2"><span>garden</span></label></li>
		                            <li><input type="checkbox" name="view" value="street" id="c3" /><label for="c3"><span>street</span></label></li>
		                        </ul>
		                    </li>
		                    <li class="ss-main price">price <span class="fa down">&#xf0d7;</span><span class="fa up">&#xf0d8;</span>
		                        <ul class="sub-price">
		                            <!--<li><input type="checkbox" id="c4" /><label for="c4"><span>$500-$700</span></label></li>
		                            <li><input type="checkbox" id="c5" /><label for="c5"><span>$701-$1000</span></label></li>
		                            <li><input type="checkbox" id="c6" /><label for="c6"><span>$1001-$1500</span></label></li>-->
		                        </ul>
		                    </li>
		                </ul>
		            </div>


		            <div id="ss-map" class="main-area">
		              	<div class="map-sec">
				      		<div id="suite-building-1" class="single-block" style="display:none;"><img src="<?php bloginfo('template_url'); ?>/colinas-map/crunches/building-1.gif" /></div>
				     		<div id="suite-building-2" class="single-block" style="display:none;"><img src="<?php bloginfo('template_url'); ?>/colinas-map/crunches/building-2.gif" /></div>
				     		<div id="suite-building-3" class="single-block" style="display:none;"><img src="<?php bloginfo('template_url'); ?>/colinas-map/crunches/building-3.gif" /></div>
				     		<div id="suite-building-4" class="single-block" style="display:none;"><img src="<?php bloginfo('template_url'); ?>/colinas-map/crunches/building-4.gif" /></div>
				      		<img class='full-map' src="<?php bloginfo('template_url'); ?>/colinas-map/hover/suite-selector_04.gif" alt="main map bg">
			      			
					      	<div id="fullhover">
					     		<div class="suite-building suite-building-1"></div>  
				      			<div class="suite-building suite-building-2"></div>
				      			<div class="suite-building suite-building-3"></div>
				      			<div class="suite-building suite-building-4"></div>	
					      	</div>
					  	</div> 
					  	<div class="restore-map"><a>See All Buildings</a></div> 
		            
		            <?php
		                if(get_field('parent'))
		                {
		                  $count = 1;
		                  while(has_sub_field('parent'))
		                  {
		                    ?>
		                        <div class="popup-building-type-<?php echo $count; ?> popup-sec popup-type-<?php echo $count; ?>">
		                            <div class="close"></div>
		                            <div class="popup-content">
		                                <div class="popup-content-sec clearfix">
		                                    <div class="popup-left">
		                                        <h3><?php echo get_sub_field('building_name'); ?></h3>
		                                        <?php echo get_sub_field('amenities'); ?>
		                                    </div>
		                                    <div class="popup-right">
		                                        <img src="<?php echo get_sub_field('building_image'); ?>" alt="">
		                                    </div>
		                                </div>
		                                <div class="popup-btn"><a class="orange-btn" href="<?php echo get_sub_field('availability_link'); ?>">AVAILABLE APARTMENTS</a></div>
		                            </div>
		                        </div>
		                        <?php
                            		$count++;
		                  }
		                }
		            ?>
		            
		           
			    <div class="loader"></div>
			   	<div id="backgroundPopup"></div>
		                
		            </div>
		        </div>
		        <div class="ss-right equalheight">
		            <div id="my-loader" style="display:none;" ><img class="loader-img" src="<?php bloginfo('template_url'); ?>/img/load.gif"/></div> 
		            <div id="available-apartments-header">AVAILABLE APARTMENTS <a class="view-all" href="/apartment-listings/">VIEW ALL</a></div>
		            
		            <div class="content mCustomScrollbar appartment-list-sec">
		              <ul class="appartment-list" id="list-apart"></ul>
					 </div>
		        </div>
		    </div><!-- /suite-selector-->
		    <!--<p id='click'>CLICK</p>--><!-- CLICK -->
		</div> 
<?php endwhile; ?>                
    
    <?php
        $arr_view = array();
        $arr_view['garden']= get_field('garden_view');
        $arr_view['pool']= get_field('pool_view');
        $arr_view['street']= get_field('street_view');
    ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script>
 var views = jQuery.parseJSON('<?php echo json_encode($arr_view);?>');
    //$(document).ready(function(){
           
        /*TEMP COMMENT
        // Binding Pricelist using PickList method of RealPage 
        $.ajax({
            method: "POST",
            url: "http://crestatparkcentral.com/wp-content/themes/workhorse-boilerplate/parkprice.php",
            beforeSend: function(){
              $("#my-loader").show();
            }
          }).done(function(priceback) {
              xmlDoc = $.parseXML( priceback ),
              $xml = $( xmlDoc ),
              $UnitObject = $xml.find( "PicklistItem" );
              $( "li.price ul.sub-price" ).empty();
              $UnitObject.each(function(index, element) {
                $( "li.price ul.sub-price" ).append('<li><input name="price" id="'+$(element).find('Value').text()+'"  value="'+$(element).find('Text').text()+'"  type="checkbox" ><label for="'+$(element).find('Value').text()+'"><span> $'+$(element).find('Text').text().split('.00').join("").replace(' - ',' - $')+'</span></label></li>');
            });
			var lastPrice = $( "li.price ul.sub-price li:last input").val().split('-')[1].trim().split('.00').join("");
			$( "li.price ul.sub-price" ).append('<li><input name="price" id="lastPrice"  value="'+lastPrice+'-'+(lastPrice*100)+'"  type="checkbox" ><label for="lastPrice"><span> > $'+lastPrice+'</span></label></li>');
		         $("#my-loader").hide();
             bindEvents();
          });   
          // Binding Pricelist using PickList method of RealPage (over) 
		  		
        //checkbox will select only one value at a time

      // List call to retrieve data on ready 
       
      getData(0,0,0);
 	
	
	$("#suite-selector input[type=checkbox]").click(function(){
		if($(this).attr('checked')){
			$('.'+$(this).attr('rel')).css('opacity','1');
		}else {
			$('.'+$(this).attr('rel')).css('opacity','0');
		}
	})
	
	$("li.ss-main").hover(function(){
    $("li.ss-main").removeClass('ss-show-sub');
    $(this).toggleClass('ss-show-sub');	
    $(this).hover(function(){ $(this).toggleClass('ss-show-sub'); });
	});	
});
 

 
 
function mapData(bedVal,bathVal,priceVal,click){
  $.ajax({
    method: "POST",
    url: "http://crestatparkcentral.com/wp-content/themes/workhorse-boilerplate/park-soap.php",
    data: { bed: bedVal , bath: bathVal, price:priceVal, click:click },
      beforeSend: function(){
        $("#my-loader").show();
      }
  }).done(function(xml) {
    //jQuery("#xmlList").html(response);
     xmlDoc = $.parseXML( xml ),
      $xml = $( xmlDoc ),
      $UnitObject = $xml.find( "UnitObject" );
      $( "#list-apart" ).empty();
      //alert($UnitObject.length);
      var hightlight = "";
      var i = 0;
      $UnitObject.each(function(index, element) {
      var tempid = $(element).find('UnitNumber').text();
      //alert(tempid);
      if($.inArray(tempid,click)!='-1')
      {
        $( "#list-apart" ).append('<li class="'+getLiClass($(element).find('Address').find('UnitNumber').text())+'" data-price="'+$(element).find('BaseRentAmount').text()+'"><span>'+$(element).find('Address').find('Address1').text()+', Floor - '+$(element).find('UnitDetails').find('FloorNumber').text()+'</span>'+$(element).find('UnitDetails').find('Bedrooms').text()+' bed, '+$(element).find('UnitDetails').find('Bathrooms').text()+' bath, '+$(element).find('UnitDetails').find('RentSqFtCount').text()+' sqft, '+'Starting at $'+$(element).find('BaseRentAmount').text().replace('.0000','')+' <a href="#">Pre-lease now</a>'+'</li>');
      }
      filterViews();
    });

      $("#my-loader").hide();
      console.log('classes to make active');
      //console.log(hightlight/*.slice(0,-1)*///);
      /*console.log(click);
      //click.shift();
 	  setActive(click);     
    });       
}
 */

/* TEMP COMMENT 
function setActive(click){
	//setTimeout(function(){
		$("div.single-block div.suite").each(function(){
    		$(this).removeClass('active');
	  	});	  	 

		for(var i=0;i<click.length;i++)
		{
			var className = click[i];	
			var clstr = 'div.single-block div.suite.b-'+className;
			console.log(clstr);
			$(clstr).addClass('active');
		}
	//},1500);
}

function getData(bedVal,bathVal,priceVal){
	var click = [];
	$.ajax({
	  method: "POST",
	  url: "http://crestatparkcentral.com/wp-content/themes/workhorse-boilerplate/park-soap.php",
	  data: { bed: bedVal , bath: bathVal, price:priceVal},
      beforeSend: function(){
        $("#my-loader").show();
      }
	}).done(function(xml) {
		//jQuery("#xmlList").html(response);
		 xmlDoc = $.parseXML( xml ),
		  $xml = $( xmlDoc ),
		  $UnitObject = $xml.find( "UnitObject" );
		  $( "#list-apart" ).empty();
		  //alert($UnitObject.length);
		  var hightlight = "";
		  var i = 0;
		  $UnitObject.each(function(index, element) {
			$( "#list-apart" ).append('<li class="'+getLiClass($(element).find('Address').find('UnitNumber').text())+'" data-price="'+$(element).find('BaseRentAmount').text()+'"><span>'+$(element).find('Address').find('Address1').text()+', Floor - '+$(element).find('UnitDetails').find('FloorNumber').text()+'</span>'+$(element).find('UnitDetails').find('Bedrooms').text()+' bed, '+$(element).find('UnitDetails').find('Bathrooms').text()+' bath, '+$(element).find('UnitDetails').find('RentSqFtCount').text()+' sqft, '+'Starting at $'+$(element).find('BaseRentAmount').text().replace('.0000','')+' <a href="#">Pre-lease now</a>'+'</li>');
			//console.log($(element).find('UnitNumber').text());
			click.push($(element).find('UnitNumber').text());
			filterViews();
		});
      $("#my-loader").hide();
      setActive(click);
	  });       
}
*/   	


	/* Views filter logic */
	/* TEMP COMMENT
	function filterViews(){
		var click=[];
		if(jQuery( 'ul.sub-view li input[name=view]:checked').length){
			var viewVal = jQuery( 'ul.sub-view li input[name=view]:checked' ).val();
			$( "#list-apart li" ).not('.hiddenPrice').hide().addClass('hiddenView');
			$('li.view span.title').text(viewVal);
			$( "#list-apart li."+viewVal ).not('.hiddenPrice').show().removeClass('hiddenView');
			$( "#list-apart li:visible span").each(function(){
				var cont = $(this).text();
				var index = cont.indexOf("#");
				var flat = cont.substr(index+1,4);
				click.push(flat);
				setActive(click);     
			});
		}else{
			$('li.view span.title').text('view');
      		$( "#list-apart li.hiddenView" ).show().removeClass('hiddenView');

		}
		filterPrice();
	}
	function filterPrice(){
		var click = [];
		if(jQuery( 'ul.sub-price li input[name=price]:checked').length){
			var priceVal = jQuery( 'ul.sub-price li input[name=price]:checked' ).val().split('-');
			var minPrice = parseFloat(priceVal[0].trim());
			var maxPrice = parseFloat(priceVal[1].trim());
			$( '#list-apart li').not('.hiddenView').hide().addClass('hiddenPrice');
			var hightlight = "";
			$( '#list-apart li.hiddenPrice').each(function(i,e){
				if(parseFloat($(this).attr('data-price')) > minPrice && parseFloat($(this).attr('data-price')) <= maxPrice){
					var line = $(this).text();
					var index = line.indexOf('#');
					var unitid = line.substr(index+1,4);
					click.push(unitid);

					$(this).show().removeClass('hiddenPrice');	
				}
			});
			 
			//alert($.type(click));
			//alert(click);
			setActive(click);

		}else{	
			$( '#list-apart li.hiddenPrice').show().removeClass('hiddenPrice');
		}
	}
	
	function getLiClass(unitNumber){
		var liClass = "";
		$.each(views,function(k,v){
			var numbers = v.split(',');
			if($.inArray(unitNumber,numbers) > -1){
				liClass += k+" ";
			}
		});
		return liClass;
	}
	
	
  function bindBoxClicks(element){
	   var $box = $(element);
		if ($box.is(":checked")) {
			var group = "input:checkbox[name='" + $box.attr("name") + "']";
			$(group).prop("checked", false);
			$box.prop("checked", true);
		} else {
			$box.prop("checked", false);
		}
  }
	*/
    
    /* bind events */
    /* TEMP COMMENT
	function bindEvents(){
        
        //checkbox will select only one value at a time
        $("ul.sub-bed li input:checkbox").on('click', function() {
           bindBoxClicks($(this));
        });
        $("ul.sub-bath li input:checkbox").on('click', function() {
            bindBoxClicks($(this));
        });
        $("ul.sub-view li input:checkbox").on('click', function() {
            bindBoxClicks($(this));
			filterViews();
        });
		$("ul.sub-price li input:checkbox").on('click', function() {
           bindBoxClicks($(this));
		   filterPrice();
		});
   
        jQuery('ul.suite-filter li input[type=checkbox]').change(function(){
            var priceVal = 0;
			var bathVal = 0;
			var bedVal = 0;
			
			//if(jQuery('ul.sub-price li input[name=price]:checked').length){
				//priceVal = jQuery('ul.sub-price li input[name=price]:checked').val();
			//}

			//set timeout for clearing previous selection on click event above
			setTimeout(function(){ 
					if(jQuery('ul.sub-bath li input[name=bath]:checked').length){
		            	bathVal = jQuery('ul.sub-bath li input[name=bath]:checked').val();
		            	//alert(bathVal);
					}
					if(jQuery('ul.sub-bed li input[name=bed]:checked').length){
		            	bedVal = jQuery('ul.sub-bed li input[name=bed]:checked').val();
		            	//alert(bedVal);
					}
					 
		      		var bathText="";
					if(!bathVal)
					{
						bathText = "0+ Baths";
					}
					else if(bathVal == "1")
					{
						bathText = bathVal+" Bath";
					}
					else
					{
						bathText = bathVal+" Baths";
					}
					jQuery("li.bath span.title").text(bathText);
					
					
					var bedText="";
					if(!bedVal)
					{
						bedText = "0+ Beds";
					}
					else if(bedVal == "1")
					{
						bedText = bedVal+" Bed";
					}
					else
					{
						bedText = bedVal+" Beds";
					}
					jQuery("li.bed span.title").text(bedText);
		            
					jQuery("li.ss-show-sub").removeClass('ss-show-sub');    

		            getData(bedVal,bathVal,priceVal);
        	}, 300);
             
        }); 
    } 
    */
    /* bind PRICE events */           
    /* TEMP COMMENT
    equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
    equalheight('.equalheight');
    var selectorHeight = $('#suite-selector #map-image').height();
    $('.appartment-list-sec').css("min-height", selectorHeight-30);

});


$(window).resize(function(){
    equalheight('.equalheight');
    var selectorHeight = $('#suite-selector #map-image').height();
    $('.appartment-list-sec').css("min-height", selectorHeight-30);

});

var imgName="";
var myClick = ""; 
*/

$(document).ready(function(){
	$('div.restore-map a').hide();
	$('div.map-sec div#fullhover div').click(function(){
		var myClass = $(this).attr('class');
		imgName = myClass.substring(15);
		$('div.map-sec div').hide();//hiding
		$('.full-map').fadeOut('slow');
		setTimeout(function(){ $('div#'+imgName).fadeIn('slow'); },1000);
		$('div#'+imgName+' div.suite').show();
		setTimeout(function(){ $('div.restore-map a').show(); },1000);

		$("#"+imgName+" div.suite").unbind('click');
		$("#"+imgName+" div.suite").click(function(){
			var ar = [];		 
			var cname = jQuery(this).attr('class');
			ar = cname.split(" "); 
			//alert(ar); 
			for(var i=0; i<ar.length;i++)
			{
				ar[i] = ar[i].substr(2);
			}
			ar.shift();ar.shift();ar.shift();
			//alert(ar); 
			var click = ar;
			mapData(0,0,0,click);
		});			
	});
  	
  	
  	 
  	$('div.restore-map a').click(function(){
		$('div.map-sec div.single-block').fadeOut('slow');
		setTimeout(function(){ $('div.map-sec img.full-map').fadeIn('slow'); },500);
		//setTimeout(function(){ $('div.map-sec div#fullhover div.suite').show(); },1000);
		$('div.map-sec div').show();
		$('div.map-sec div.single-block').hide();
		$('div.restore-map a').hide();
		getData(0,0,0);
  	});
 
});

</script>	
	<script src="<?php bloginfo('template_directory'); ?>/_js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script>
		//var ar = [];	
		//jQuery(document).ready(function(){
 		/*var cname = jQuery(this).attr('class');
		ar = cname.split(" "); 
		//alert(ar); 
		for(var i=0; i<ar.length;i++)
		{
			ar[i] = ar[i].substr(2);
		}
		ar.shift();ar.shift();
		//alert(ar); 
		var click = ar;
		mapData(0,0,0,click);*/
		//});
	</script>

    
<?php get_footer(); ?>	