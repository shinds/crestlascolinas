<?php

/***************************************************************
Workhorse Digital Boilerplate functions.php

	TABLE OF CONTENTS
	Function										Line
	----------------------------------------------------
	1. Disable XMLRPC						
	2. Simplify the Admin Menu							
	3. Modify TinyMCE editor buttons (remove h1)
	
	Site Specific modifications
	Following

*****************************************************************/
##### 1. Disable XMLRPC


add_filter( 'xmlrpc_enabled', '__return_false' );

##### 2. Simplify the Admin Menu
// function remove_menus(){
//  Commented out for development 
//   remove_menu_page( 'index.php' );                  //Dashboard
//   remove_menu_page( 'edit.php' );                   //Posts
//   remove_menu_page( 'upload.php' );                 //Media
//   remove_menu_page( 'edit.php?post_type=page' );    //Pages
//   remove_menu_page( 'edit-comments.php' );          //Comments
//   remove_menu_page( 'themes.php' );                 //Appearance
//   remove_menu_page( 'plugins.php' );                //Plugins
//   remove_menu_page( 'users.php' );                  //Users
//   remove_menu_page( 'tools.php' );                  //Tools
//   remove_menu_page( 'options-general.php' );        //Settings
  
// }
// add_action( 'admin_menu', 'remove_menus' );
##### 3. Modify TinyMCE editor buttons (remove h1)

// THis doesn't work!!!
function customformatTinyMCE($init) {
	// Add block format elements you want to show in dropdown
	$init['theme_advanced_blockformats'] = 'p,h2,h3,h4';
	return $init;
}
// Modify Tiny_MCE init
add_filter('tiny_mce_before_init', 'customformatTinyMCE' );

add_theme_support( 'menus' );

$bloginfo['template_url']=get_bloginfo('template_url');
$bloginfo['name']=get_bloginfo( 'name' );



$bloginfo['url']=get_bloginfo('url');
$bloginfo['ms-name'][2]="Glencoe";
$bloginfo['ms-name'][3]="Park Central";
$bloginfo['ms-name'][4]="Las Colinas"; 

$bloginfo['da_site']="lascolinas";
$bloginfo['real_page']="3467733";
$bloginfo['floorplans_page']=68;	
$bloginfo['neighborhood_page']=70;
$bloginfo['contact_page']=71;

	
include('_includes/custom-fields.php');



function image_name_as_alt($whichone){
	$explode = explode('/',$whichone);
	$explode2 =  explode('.',$explode[(count($explode)-1)]);
	return str_replace ('-',' ',$explode2[0]); 
	
}

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_splash',
		'title' => 'Splash',
		'fields' => array (
			array (
				'key' => 'field_54222c03ebb2c',
				'label' => 'Logo',
				'name' => 'logo',
				'type' => 'image',
				'save_format' => 'id',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_54222cd814fbc',
				'label' => 'Rendering',
				'name' => 'rendering',
				'type' => 'image',
				'save_format' => 'id',
				'preview_size' => 'medium',
				'library' => 'all',
			),
			array (
				'key' => 'field_5424358c6a6b5',
				'label' => 'Amenities',
				'name' => 'amenities',
				'type' => 'textarea',
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => '',
				'formatting' => 'none',
			),
			array (
				'key' => 'field_5422d1f7a0bcc',
				'label' => 'Featured Color',
				'name' => 'featured_color',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5422d1f7a02xx',
				'label' => 'Send Email To',
				'name' => 'send_email_to',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5422d1f7a0bxx',
				'label' => 'Thank You Message',
				'name' => 'thank_you_message',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			
			array (
				'key' => 'field_542343e33966g',
				'label' => 'Street Address',
				'name' => 'street_address',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_542343e33967g',
				'label' => 'City, State ZIP',
				'name' => 'city_state_zip',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_542343e33968g',
				'label' => 'Phone Number',
				'name' => 'phone_number',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-splash.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

function contact_us_custom_init()
{
	$labels = array(
		'name' => 'Form Submissions',
		'singular_name' => 'Contact',
		'add_new' => 'Add New',
		'add_new_item' => 'Add New Form Submission',
		'edit_item' => 'Edit Form Submission',
		'new_item' => 'New Form Submission',
		'all_items' => 'All Form Submissions',
		'view_item' => 'View Form Submission',
		'search_items' => 'Search Form Submissions',
		'not_found' => 'No email found',
		'not_found_in_trash' => 'No books found in Trash',
		'parent_item_colon' => '',
		'menu_name' => 'Form Submissions'
	);
	$args = array(
		'labels' => $labels,
		'public' => false,
		'publicly_queryable' => false,
		'show_ui' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => array(
			'slug' => 'contact_submission'
		) ,
		'capability_type' => 'post',
		'has_archive' => false,
		'hierarchical' => false,
		'menu_position' => null,
		'supports' => array(
			'title',
			'editor'
		)
	);
	register_post_type('contact_form', $args);
}
add_action('init', 'contact_us_custom_init');

function headerLogo($frontpage,$image){
	global $bloginfo;
	if($frontpage){$hh='h1';}else{$hh='h4';}
	
	return '<'.$hh.' id="logo"><a href="'.$bloginfo['url'].'" title="'.$bloginfo['name'].'" rel="home"><img src="'.$image.'" /></a></'.$hh.'>';
	
	
};

function spitOutSocial($social){
	
		if($social){
			
			echo '<ul class="social-icons">';
			
			foreach($social as $item){
				
				
				echo '<li><a href="'.$item['link'].'" target="_blank" class="fa">'.$item['font_awesome_code'].'</a></li>';
				
			}
			
			echo "</ul>";
			
			
		}
	
}
if ( ! function_exists( 'boilerplate_widgets_init' ) ) :
	/**
	 * Register widgetized areas, including two sidebars and four widget-ready columns in the footer.
	 *
	 * To override boilerplate_widgets_init() in a child theme, remove the action hook and add your own
	 * function tied to the init hook.
	 *
	 * @since Twenty Ten 1.0
	 * @uses register_sidebar
	 */
	function boilerplate_widgets_init() {
		// Area 1, located at the top of the sidebar.
		register_sidebar( array(
			'name' => __( 'Primary Widget Area', 'boilerplate' ),
			'id' => 'primary-widget-area',
			'description' => __( 'The primary widget area', 'boilerplate' ),
			'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );

		// Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
		register_sidebar( array(
			'name' => __( 'Secondary Widget Area', 'boilerplate' ),
			'id' => 'secondary-widget-area',
			'description' => __( 'The secondary widget area', 'boilerplate' ),
			'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</li>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );

		// Area 3, located in the footer. Empty by default.
		register_sidebar( array(
			'name' => __( 'First Footer Widget Area', 'boilerplate' ),
			'id' => 'first-footer-widget-area',
			'description' => __( 'The first footer widget area', 'boilerplate' ),
			'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</li>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );

		// Area 4, located in the footer. Empty by default.
		register_sidebar( array(
			'name' => __( 'Second Footer Widget Area', 'boilerplate' ),
			'id' => 'second-footer-widget-area',
			'description' => __( 'The second footer widget area', 'boilerplate' ),
			'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</li>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );

		// Area 5, located in the footer. Empty by default.
		register_sidebar( array(
			'name' => __( 'Third Footer Widget Area', 'boilerplate' ),
			'id' => 'third-footer-widget-area',
			'description' => __( 'The third footer widget area', 'boilerplate' ),
			'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</li>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );

		// Area 6, located in the footer. Empty by default.
		register_sidebar( array(
			'name' => __( 'Fourth Footer Widget Area', 'boilerplate' ),
			'id' => 'fourth-footer-widget-area',
			'description' => __( 'The fourth footer widget area', 'boilerplate' ),
			'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
			'after_widget' => '</li>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		) );
	}
endif;
add_action( 'widgets_init', 'boilerplate_widgets_init' );

add_action( 'init', 'register_cpt_hot_spot' );

function register_cpt_hot_spot() {

    $labels = array( 
        'name' => _x( 'Hot Spots', 'hot_spot' ),
        'singular_name' => _x( 'Hot Spot', 'hot_spot' ),
        'add_new' => _x( 'Add New', 'hot_spot' ),
        'add_new_item' => _x( 'Add New Hot Spot', 'hot_spot' ),
        'edit_item' => _x( 'Edit Hot Spot', 'hot_spot' ),
        'new_item' => _x( 'New Hot Spot', 'hot_spot' ),
        'view_item' => _x( 'View Hot Spot', 'hot_spot' ),
        'search_items' => _x( 'Search Hot Spots', 'hot_spot' ),
        'not_found' => _x( 'No hot spots found', 'hot_spot' ),
        'not_found_in_trash' => _x( 'No hot spots found in Trash', 'hot_spot' ),
        'parent_item_colon' => _x( 'Parent Hot Spot:', 'hot_spot' ),
        'menu_name' => _x( 'Hot Spots', 'hot_spot' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => true,
        
        'supports' => array( 'title' ),
        'taxonomies' => array( 'hot_spot_categories' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        
        
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );

    register_post_type( 'hot_spot', $args );
}


add_theme_support( 'post-thumbnails' );

add_action( 'init', 'register_taxonomy_hot_spot_categories' );

function register_taxonomy_hot_spot_categories() {

    $labels = array( 
        'name' => _x( 'Hot Spot Categories', 'hot_spot_categories' ),
        'singular_name' => _x( 'Hot Spot Catefory', 'hot_spot_categories' ),
        'search_items' => _x( 'Search Hot Spot Categories', 'hot_spot_categories' ),
        'popular_items' => _x( 'Popular Hot Spot Categories', 'hot_spot_categories' ),
        'all_items' => _x( 'All Hot Spot Categories', 'hot_spot_categories' ),
        'parent_item' => _x( 'Parent Hot Spot Catefory', 'hot_spot_categories' ),
        'parent_item_colon' => _x( 'Parent Hot Spot Catefory:', 'hot_spot_categories' ),
        'edit_item' => _x( 'Edit Hot Spot Catefory', 'hot_spot_categories' ),
        'update_item' => _x( 'Update Hot Spot Catefory', 'hot_spot_categories' ),
        'add_new_item' => _x( 'Add New Hot Spot Catefory', 'hot_spot_categories' ),
        'new_item_name' => _x( 'New Hot Spot Catefory', 'hot_spot_categories' ),
        'separate_items_with_commas' => _x( 'Separate hot spot categories with commas', 'hot_spot_categories' ),
        'add_or_remove_items' => _x( 'Add or remove hot spot categories', 'hot_spot_categories' ),
        'choose_from_most_used' => _x( 'Choose from the most used hot spot categories', 'hot_spot_categories' ),
        'menu_name' => _x( 'Hot Spot Categories', 'hot_spot_categories' ),
    );

    $args = array( 
        'labels' => $labels,
        'public' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'show_admin_column' => false,
        'hierarchical' => true,

        'rewrite' => true,
        'query_var' => true
    );

    register_taxonomy( 'hot_spot_categories', array('hot_spot'), $args );
}


function showIf($content,$start,$end){
	
	if(trim($content)!==""){
		
		return $start.$content.$end;
		
	} else {
		
	 return "";	
	}
	
	
}
?>