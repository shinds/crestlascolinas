<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Boilerplate
 * @since Boilerplate 1.0
 */

get_header(); ?>
<?php
	/* Queue the first post, that way we know
	 * what date we're dealing with (if that is the case).
	 *
	 * We reset this later so we can run the loop
	 * properly with a call to rewind_posts().
	 */
	if ( have_posts() )
		//the_post();
?>
				

 

<?php 
	$pg = (get_query_var('paged')) ? get_query_var('paged') : 1;  
	global $wp_query; 
?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="inner clearfix">
						<h1 class="page-title"><?php
						if ( is_day() ) :
							printf( __( 'Daily Archives: %s', 'boilerplate' ), get_the_date() );
						elseif ( is_month() ) :
							printf( __( 'Monthly Archives: %s', 'boilerplate' ), get_the_date('F Y') );
						elseif ( is_year() ) :
							printf( __( 'Yearly Archives: %s', 'boilerplate' ), get_the_date('Y') );
						else :
							_e( 'Blog Archives', 'boilerplate' );
						endif;
					?></h1>
					<div class="blog-left">
						<?php while ( have_posts() ) : the_post(); ?>
							  <div class='full-bleed'>
								<a href="<?php echo get_permalink();?>"><h2><?php echo get_the_title(); ?></h2></a>
								<?php if ( has_post_thumbnail() ): ?>
								<div class="post-thumb"><img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>"></div>
								<?php endif; ?>
								<div class="post-cont">
									<p><?php echo get_the_excerpt(); ?> <a href="<?php echo get_permalink();?>">read more »</a>
								</div>
							</div>
						<?php endwhile; ?>
						<nav class="navigation nav-below">
							<div class="wp-pagenavi">
								<?php  
									global $wp_query;
								 	$paged = $pg;  
									$big = 999999999; // need an unlikely integer
							        echo paginate_links( array(
										'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
										'format' => '?page=%#%',
										'current' => max( 1,$paged),
										'end_size' => 2,
										'mid_size' => 1,
										'total' => $wp_query->max_num_pages,
										'prev_text'    => __('«'),
								        'next_text'    => __('»'),
										'type' => 'list'
									) ); 
								?> 
							</div>
						</nav>
					</div>
					<div class="blog-right">
						<?php dynamic_sidebar('Primary Widget Area'); ?>
					</div>
				</div>
		</article>
				 
<?php get_footer(); ?>