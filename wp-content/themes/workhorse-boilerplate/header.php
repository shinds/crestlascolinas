<?php global $bloginfo;  ?><!DOCTYPE html>
<!--[if lt IE 7 ]><html lang="en-US" class="no-js ie ie6 lte7 lte8 lte9"><![endif]-->
<!--[if IE 7 ]><html lang="en-US" class="no-js ie ie7 lte7 lte8 lte9"><![endif]-->
<!--[if IE 8 ]><html lang="en-US" class="no-js ie ie8 lte8 lte9"><![endif]-->
<!--[if IE 9 ]><html lang="en-US" class="no-js ie ie9 lte9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en-US" class="no-js"><!--<![endif]-->
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K5CK398');</script>
<!-- End Google Tag Manager -->

<script src="//statrack.leaselabs.com/dpx.js?cid=81449&action=100&segment=llcrestatlascolinassite&m=1&sifi_tuid=47096"></script>

<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
<title><?php wp_title('|');?></title>

<!-- Css for datepicker -->
<!-- 945=> Park Residence ID , 350 => Glencoer Suite Selector ID , 68=> Glencoe Residence ID -->
<?php if( is_page(945) || is_page(350) || is_page(68) ) { ?>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<?php } ?>
<!-- css for datepicker -->

<!-- Files for Circle Slider -->
<link rel="stylesheet" href="<?php echo $bloginfo['template_url']; ?>/circle-slider/css/normalize.css">
<link rel="stylesheet" href="<?php echo $bloginfo['template_url']; ?>/circle-slider/css/foundation.min.css">
<link rel="stylesheet" href="<?php echo $bloginfo['template_url']; ?>/circle-slider/css/style.css">
<link rel="stylesheet" href="<?php echo $bloginfo['template_url']; ?>/circle-slider/css/jquery.circular-carousel.css">
<link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">

<!-- End Circle Slider -->

<link rel="stylesheet" href="<?php echo $bloginfo['template_url']; ?>/_css/pikaday.css">



<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

<link href='https://fonts.googleapis.com/css?family=Unica+One' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Vollkorn:400,700italic,400italic,700' rel='stylesheet' type='text/css'>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<!-- Park Central-->
<link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>

<?php
	if(is_page(350))
	{
		?><link href='<?php bloginfo('template_url'); ?>/map/map.css' rel='stylesheet' type='text/css'><?php
	}
?>
	
<?php wp_head(); ?>
<?php if(preg_match('/(?i)msie [1-8]/',$_SERVER['HTTP_USER_AGENT'])) { ?>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>

<?php ;} ?>
	
	<link href="<?php echo $bloginfo['template_url']; ?>/slider/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo $bloginfo['template_url']; ?>/slider/owl.theme.css" rel="stylesheet">   
	
	<link rel="stylesheet" type="text/css" href="<?php echo $bloginfo['template_url']; ?>/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />	

    <!--link href="<?php echo $bloginfo['template_url']; ?>/_css/lightbox.css" rel="stylesheet">
    <link href="<?php echo $bloginfo['template_url']; ?>/_css/jqui.css" rel="stylesheet">
    <script src="<?php echo $bloginfo['template_url']; ?>/_js/lightbox.min.js"></script>
    <script src="<?php echo $bloginfo['template_url']; ?>/_js/jquery-ui._js"></script-->
	
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/_css/jquery.mCustomScrollbar.css">
	
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/shadowbox/shadowbox.css">
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/shadowbox/shadowbox.js"></script>
	<script type="text/javascript">
		Shadowbox.init();
	</script>
</head>
<body <?php body_class(); ?>>



<style>
#openUpThisOne.mobile-widget .entry-content{
	background:url('<?php echo $bloginfo['template_url'] ?>/_images/lascolinas/bg-repeat-lascolinas-light.jpg') repeat 152px top;
}
</style>





	<style type="text/css">

		#openUpThisOne {width:100%; height:100%; position:fixed; top:0px; left:0px; z-index:120000; background-color:rgba(0,0,0,.8);  display:none; }
		#widget-holder {max-width:960px; margin:0px auto; background-color:#fff; overflow:auto; max-height:90%; margin-top:2%;}
	</style>

<div id="openUpThisOne">
    <a class="close" href="javascript:void(0)" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">x</a>

	<div id="widget-holder">

		<div id="availabilityWidget">
		</DIV>

	</div>
	
</div>

<!-- ----------

SCRIPT element to load Online Leasing

Note: Both the "siteId" and "container" parameters are required.

The value of the "siteId" parameter must be the RealPage site ID for the property.

The value of the "container" parameter is the ID of the DIV element which should contain Online Leasing.

If the ID specified for "container" does not exist, Online Leasing will create it at the position of the script element.

---------- -->




	<div class="<?php echo $bloginfo['da_site'] ?>" id="all">
		<div id="mobile-menu">
			<div class="mobile-menu-content" id="mobile-slide-home">
				<?php wp_nav_menu( array( 'menu' => 'Mobile Menu') ); ?>
				 <?php 
					spitOutSocial(get_field('social_links','option'));
				?>
			</div>   

		</div>

		 

		<header id="header">
			<div class="gutter">
			<div class="inner">
				<a class="site-logo" href="/home/"></a>
				<div id="hamburger" class="fa">&#xf039;</div>
	
				<div id="menus-right">
	
					
		
					<div class="header-phone"><a href="tel:<?php echo get_field('phone_#','option'); ?>"><?php echo get_field('phone_#','option'); ?></a></div>
		
					<?php wp_nav_menu( array('menu' => 'Top Menu' )); ?>
					
					<?php /* spitOutSocial(); */?>
	
					<?php wp_nav_menu( array('menu' => 'Main Menu' )); ?>
	
				</div>
				
				
			
			</div>

		</div>


<div id="th-specials-banner"><a style="color: inherit;" href="/specials">Up to 6 weeks free PLUS $1000 gift card…limited time only</a></div>


	</header>
		<section id="content" role="main">
		
		