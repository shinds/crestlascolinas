<?php
/**
*Template Name:Blog Landing Page
**/
?>
<?php get_header(); ?>


<?php 
	$pg = get_query_var('paged');  
	if ( ! function_exists( 'my_pagination' ) ) :
		function my_pagination($j) {
			global $query,$content,$pg;
			$paged = ($pg && $_GET['type'] == $content->slug) ? $pg : 1;  
	        echo paginate_links( array(
				'base' => get_permalink(138).'%_%',
				'format' => 'page/%#%/',
				/*'add_args' => array( 'type' => $content->slug ),*/
				'current' => max( 1,$paged),
				'end_size' => 2,
				'mid_size' => 1,
				'total' => $query->max_num_pages,
				'prev_text'    => __('«'),
		        'next_text'    => __('»'),
				'type' => 'list'
			) );
		}
	endif;
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="inner clearfix">
		<div class="blog-left">
			<?php $args = array('paged' => $pg, 'post_type' => 'post', 'order' => 'desc', 'posts_per_page'=>'3');
				  $query = new WP_Query( $args ); ?>
			<?php if ( $query->have_posts() ) : ?>
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
			<div class='full-bleed'>
				<a href="<?php echo get_permalink();?>"><h2><?php echo get_the_title(); ?></h2></a>
				<?php if ( has_post_thumbnail() ): ?>
				<div class="post-thumb"><img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>"></div>
				<?php endif; ?>
				<div class="post-cont">
					<p><?php echo get_the_excerpt(); ?> <a href="<?php echo get_permalink();?>">read more »</a>
				</div>
			</div>
			<?php endwhile; ?>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
			<nav class="navigation nav-below">
				<div class="wp-pagenavi">
					<?php my_pagination($j);?>
				</div>
			</nav>
		</div>
		<div class="blog-right">
			<?php dynamic_sidebar('Primary Widget Area'); ?>
		</div>
	</div><!-- page close -->
</article><!-- #post-## -->				 
<?php endwhile; ?>
 
<?php get_footer(); ?>
