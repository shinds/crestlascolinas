<?php
/**
 * Template Name: page plans
 */
get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>




	<div class="inner">
	<div class="residences-inner">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php /*<h1 class="entry-title"><?php the_title(); ?></h1> */ ?>
				<div class="entry-content parents">
                
              <div class="tab-wrapper">
    <?php $tabs_section = get_field('floorplan_type');
		if($tabs_section){
			echo '<ul class="tabs">';
			$counter=0;
			foreach($tabs_section as $item){
				$first_image = wp_get_attachment_image_src( $item['floorplan'][0]['image'], 'large');;
				if($counter==0){$class=' class="selected"';}else{$class='';}
				echo '<li'.$class.' rel="'.$counter.'" data-image="'.$first_image[0].'">'.$item['floorplan_type'].'</li>';
				
				$counter++;
				
			}
			echo '<div class="clearfix"></div>';
			echo '</ul>';
			
		}
	
	 ?>
     
     <?php $tabs_section = get_field('floorplan_type');
		if($tabs_section){
			
			
			
			echo '<ul class="tab-sections">';
			$counter = 0;
			
			foreach($tabs_section as $item){
				
				
					
				
				echo '<li class="tab'.$counter.' clearfix ">';
					?>
					<?php //print "<pre>"; print_r($item['floorplan']); print "</pre>"; ?>
					<div class="floorplan-image-holder" style="text-align:center;">
						<img src = "" class="floorplan-image" />
						<div class="enlargeprint">Click to Enlarge &nbsp; <span class="print_pdf_box" style="display:none;">|&nbsp; <a  class="print_pdf_lnk" href="#" target="_blank">Print PDF</a></span></div>
						<div class="floors"></div>
					</div>
					<?php
					$section =  $item['floorplan'];
					if($section){
						echo '<ul class="rooms">';
						$counter2 =0;
						
						foreach($section as $item2){
							
							$image = wp_get_attachment_image_src($item2['image'], 'large');
							
							$data_image  = ' data-image="'.$image[0].'" ';
							
							if($item2['2nd_floor_image']){
								
							$image2 = wp_get_attachment_image_src($item2['2nd_floor_image'], 'large');
							
								$data_image  .= ' data-image2="'.$image2[0].'" ';
							
							}
							
							if($item2['3rd_floor_image']){
								
							$image3 = wp_get_attachment_image_src($item2['3rd_floor_image'], 'large');
							$data_image  .= ' data-image3="'.$image3[0].'" ';
							
							
							}

							if($item2['print_pdf'])
							{
								$data_image .= 'data-pdf="'.$item2['print_pdf'];
							}

							
							echo '<li '.$data_image.'"';
							
							
							
							
							if($counter2==0){echo ' class="selected-unit"';}
							
							echo '><h4>'.$item2['name'].'</h4>';
							
							echo '<div class="details">';
							
							if ($item2['number_of_bedrooms'] == 'Efficiency'){
								echo $item2['number_of_bedrooms'] . ' / ';
							} else {
								echo $item2['number_of_bedrooms'] . ' BD / ';
							}
							echo $item2['number_of_baths'].' BA | </strong>'. $item2['sq_feet'].' sq. ft.<br />';
							if ($item2['application_fee'])echo 'Application Fee: $'. $item2['application_fee'];
							if ($item2['deposit']) echo	'/ Deposit: $'. $item2['deposit'].' <br />';
							if ($item2['feature']) echo	$item2['feature'].' <br />';
							

							
							?>
							<div class="floorplan-image-holder-mobile" style="text-align:center;">
								<img src = "" class="floorplan-image" />
								<div class="enlargeprint">Click to Enlarge &nbsp; <span class="print_pdf_box" style="display:none;">|&nbsp; <a  class="print_pdf_lnk" href="#" target="_blank">Print PDF</a></div>
								<div class="floors"></div>
							</div>
							<?php
							echo '</div>';
							echo '</li>';
							
							$counter2++;
							
						}
						
						echo '</ul>';
					}
					
					$counter++;
				
				echo '</li>';
				
				
				
			}
			
			echo '<div class="clearfix"></div>';
			echo '</ul>';
			
		}
	
	 ?>
					</div>
 
   
    


	</div><!-- .entry-content -->           
	</article><!-- #post-## -->
		
  </div>
     </div>           <!-- .inner -->
                
<?php endwhile; ?>     


<?php get_footer(); ?>		