<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="inner clearfix">
						<div class='blog-left'>
							
							<div class='full-bleed'>
								<h2><?php echo get_the_title(); ?></h2>
								<?php if ( has_post_thumbnail() ): ?>
								<div class="post-thumb"><img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>"></div>
								<?php endif; ?>
								<div class="post-cont">
									<?php the_content(); ?>  
								</div>
							</div>
						</div>
						<div class='blog-right'>
						 	<?php dynamic_sidebar('Primary Widget Area'); ?>
						</div>
						<div class='clearfix'></div>
					</div>
				</article><!-- #post-## -->
<?php endwhile; ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>