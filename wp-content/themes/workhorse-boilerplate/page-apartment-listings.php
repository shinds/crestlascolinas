<?php
/**
 * Template Name: Apartment Listings
 */
get_header(); ?>



<?php
  /* FloorLevel: 3 Ground, 2 Mid, 1 Top */
  
    function getFloorData($level) 
    {
      $UserName = 'sthreshold1';
      $Password = 'Thresh0ld!';
      $SiteID = '3467908';
      $PmcID = '3467726';

      $xml = '<?xml version="1.0" encoding="utf-8"?>'.
              '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
                '<soap:Header>'.
                  '<UserAuthInfo xmlns="http://realpage.com/webservices">'.
                    '<UserName>'.$UserName.'</UserName>'.
                    '<Password>'.$Password.'</Password>'.
                    '<SiteID>'.$SiteID.'</SiteID>'.
                    '<PmcID>'.$PmcID.'</PmcID>'.
                    '<InternalUser>string</InternalUser>'.
                  '</UserAuthInfo>'.
                '</soap:Header>'.
                '<soap:Body>'.
                  '<List xmlns="http://realpage.com/webservices">'.
                    '<listCriteria>'.
                      '<ListCriterion>'.
                        '<Name>LimitResults</Name>'.
                        '<SingleValue>false</SingleValue>'.
                      '</ListCriterion>'. 
                      '<ListCriterion>'.
                        '<Name>FloorLevel</Name>'.
                        '<SingleValue>'.$level.'</SingleValue>'.
                      '</ListCriterion>'. 
                    '</listCriteria>'.
                  '</List>'.
                '</soap:Body>'.
              '</soap:Envelope>';

      $url = "http://onesite.realpage.com/WebServices/CrossFire/AvailabilityAndPricing/Unit.asmx?wsdl";

      $ch = curl_init();
         curl_setopt($ch, CURLOPT_URL, $url);
         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

      $headers =  array();
          array_push($headers, "Host: onesite.realpage.com");
          array_push($headers, "Content-Type: text/xml; charset=utf-8");
          array_push($headers, "Accept: text/xml");
          array_push($headers, "SOAPAction: http://realpage.com/webservices/List");
          if($xml != null) 
          {
              curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
              array_push($headers, "Content-Length: " . strlen($xml));
          }  

      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      $response = curl_exec($ch);
      $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      curl_close($ch);

      $xml = simplexml_load_string($response);
      $response = $xml->children('http://schemas.xmlsoap.org/soap/envelope/')
                         ->Body->children()
                             ->ListResponse;
      $units = $response->ListResult->UnitObject;

      $arr=array();
      $counter=0;
      foreach($units as $unit)
      {
        $arr[$counter]['bldngid'] = end($unit->Address->BuildingID);
        $arr[$counter]['flatno'] =  end($unit->Address->UnitNumber); 
        $arr[$counter]['bed'] = end($unit->UnitDetails->Bedrooms);  
        $arr[$counter]['bath'] = end($unit->UnitDetails->Bathrooms);  
        $arr[$counter]['area'] = end($unit->UnitDetails->RentSqFtCount);
        $arr[$counter]['rent'] = substr(end($unit->BaseRentAmount),0,-5); 
        $arr[$counter]['floor'] = end($unit->UnitDetails->FloorNumber);  
        $counter++;
      }

      return $arr;
    }

    $f1 = getFloorData(3); //Retriving all flats
     

    $newsize = sizeof($f1);
    $cnt = 0;
    $ctr1 = 0;
    $ctr2 = 0;
    $ctr3 = 0;
    while($cnt<$newsize)
    {
      if($f1[$cnt]['floor']=='1')
      {
          $c1[$ctr1] = $f1[$cnt];
          $ctr1++;
      }
      if($f1[$cnt]['floor']=='2')
      {
          $c2[$ctr2] = $f1[$cnt];
          $ctr2++;
      }
      if($f1[$cnt]['floor']=='3')
      {
          $c3[$ctr3] = $f1[$cnt];
          $ctr3++;
      }
      $cnt++;
    }
   
 
    $size = sizeof($f1);

    $size1= sizeof($c1);
    $size2= sizeof($c2);
    $size3= sizeof($c3);
    $cnt = 0;
    if(get_field('plans'))
    {
      while(has_sub_field('plans'))
      {
        $holder[$cnt]['flat'] = get_sub_field('plan_holder');
        $holder[$cnt]['thumb'] = get_sub_field('plan_thumbnail');
        $holder[$cnt]['large'] = get_sub_field('plan_large');
        $holder[$cnt]['sitemap'] = get_sub_field('plan_sitemap');
        $holder[$cnt]['pdf'] = get_sub_field('plan_pdf');
        $holder[$cnt]['desc'] = get_sub_field('plan_summary');
        $cnt++;
      }
    }
    //echo $cnt;

    //echo "<pre>"; print_r($holder); echo "</pre>";


?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="inner">
  <div class="aptlisting-sec clearfix">
    <div class="aptlisting-col aptlisting-col-1 equalheight">
      <div class="floor-title"><h2>FLOOR 1</h2></div>
        <?php
            for($i=0;$i<$size1;$i++)
            {
                $fid = $c1[$i]['flatno'];
                for($j=0;$j<$cnt;$j++)
                {
                  $pos = strpos($holder[$j]['flat'], $fid);
                  if($pos!==false)
                  {
                    $index = $j;  
                  } 
                }
                ?>
                <div class="apt-list clearfix">
                  <div class="apt-image"> 
                      <?php
                        if($index!=9999)
                        {
                          ?><a href="<?php echo $holder[$index]['large'];?>" rel="shadowbox"><img src="<?php echo $holder[$index]['thumb'];?>" alt=""><span>Click to Enlarge</span></a> <span><a href="<?php echo $holder[$index]['pdf']; ?>" target="_blank" >Print PDF</a></span><?php  
                        }
                        else
                        {
                          ?><img src="<?php bloginfo("template_url");?>/img/placeholder.jpg"><?php
                        }
                      ?>
                  </div>
                  <div class="apt-detail">
                    <span class="apt-name">Building <?php echo $c1[$i]['bldngid']; ?></span>
                    <span class="apt-number">Apt. #<?php echo $c1[$i]['flatno']; ?></span>
                    <span class="apt-room"><?php echo $c1[$i]['bed']; ?> beds, <?php echo $c1[$i]['bath']; ?> baths</span>
                    <span class="apt-sqft"><?php echo $c1[$i]['area']; ?> sqft</span>
                    <span class="apt-rate">Starting at $<?php echo $c1[$i]['rent']; ?></span>
                    <span class="apt-links"><a href="#">Pre-lease now</a>
                      <?php
                        if($index!=9999)
                        {
                          ?>
                              |  <a class="apt-popup-btn" id="floor1-sitemap<?php echo $i; ?>" href="#">Site map</a>
                          <?php
                        }
                      ?>
                    </span>
                  </div>
                  <!-- popup start -->
                  <div class="floor-popup-sec floor1-sitemap<?php echo $i; ?>">
                    <div class="close"></div>
                    <div class="floor-popup-content">
                      <div class="floor-opup-content-sec clearfix">
                          <div class="floor-popup-left">
                              <h3>APT. <?php echo $c1[$i]['flatno']; ?></h3>
                              <ul>
                                  <li><?php echo $c1[$i]['bed']; ?> beds, <?php echo $f1[$i]['bath']; ?> baths</li>
                                  <li>Hardwood Floors, W/D</li>
                                  <li><?php echo $c1[$i]['area']; ?> sq ft</li>
                              </ul>
                            <p><?php echo $holder[$index]['desc']; ?></p>
                          </div>
                          <div class="floor-popup-right">
                            <img src="<?php echo $holder[$index]['sitemap']; ?>" alt="">
                            <div class="availability-link"><a href="#">Click here to view availability</a></div>
                        </div>
                      </div>  
                    </div>
                  </div>
                  <!-- popup end -->
                </div>
              <?php
              $index=9999;
            }
        ?>
    </div>
    <div class="aptlisting-col aptlisting-col-2 equalheight">
      <div class="floor-title"><h2>FLOOR 2</h2></div>
      <?php
            for($i=0;$i<$size2;$i++)
            {
                $fid = $c2[$i]['flatno'];
                for($j=0;$j<$cnt;$j++)
                {
                  $pos = strpos($holder[$j]['flat'], $fid);
                  if($pos!==false)
                  {
                    $index = $j;  
                  } 
                }
                ?>
                <div class="apt-list clearfix">
                  <div class="apt-image"> 
                    <?php
                        if($index!=9999)
                        {
                          ?><a href="<?php echo $holder[$index]['large'];?>" rel="shadowbox"><img src="<?php echo $holder[$index]['thumb'];?>" alt=""><span>Click to Enlarge</span></a> <span><a href="<?php echo $holder[$index]['pdf']; ?>" target="_blank" >Print PDF</a></span><?php  
                        }
                        else
                        {
                          ?><img src="<?php bloginfo("template_url");?>/img/placeholder.jpg"><?php
                        }
                      ?>
                  </div>
                  <div class="apt-detail">
                    <span class="apt-name">Building <?php echo $c2[$i]['bldngid']; ?></span>
                    <span class="apt-number">Apt. #<?php echo $c2[$i]['flatno']; ?></span>
                    <span class="apt-room"><?php echo $c2[$i]['bed']; ?> beds, <?php echo $c2[$i]['bath']; ?> baths</span>
                    <span class="apt-sqft"><?php echo $c2[$i]['area']; ?> sqft</span>
                    <span class="apt-rate">Starting at $<?php echo $c2[$i]['rent']; ?></span>
                    <span class="apt-links"><a href="#">Pre-lease now</a>
                      <?php
                        if($index!=9999)
                        {
                          ?>
                              |  <a class="apt-popup-btn" id="floor1-sitemap<?php echo $i; ?>" href="#">Site map</a>
                          <?php
                        }
                      ?>
                    </span>
                  </div>
                  <!-- popup start -->
                  <div class="floor-popup-sec floor2-sitemap<?php echo $i; ?>">
                    <div class="close"></div>
                    <div class="floor-popup-content">
                      <div class="floor-opup-content-sec clearfix">
                          <div class="floor-popup-left">
                              <h3>APT. <?php echo $c2[$i]['flatno']; ?></h3>
                              <ul>
                                  <li><?php echo $c2[$i]['bed']; ?> beds, <?php echo $c2[$i]['bath']; ?> baths</li>
                                  <li>Hardwood Floors, W/D</li>
                                  <li><?php echo $c2[$i]['area']; ?> sq ft</li>
                              </ul>
                            <p><?php echo $holder[$index]['desc']; ?></p>
                          </div>
                          <div class="floor-popup-right">
                            <img src="<?php echo $holder[$index]['sitemap']; ?>" alt="">
                            <div class="availability-link"><a href="#">Click here to view availability</a></div>
                        </div>
                      </div>  
                    </div>
                  </div>
                  <!-- popup end -->
                </div>
              <?php
              $index=9999;
            }
        ?>
    </div>
    <div class="aptlisting-col aptlisting-col-3 equalheight">
      <div class="floor-title"><h2>FLOOR 3</h2></div>
        <?php
            for($i=0;$i<$size3;$i++)
            {
                $fid = $c3[$i]['flatno'];
                for($j=0;$j<$cnt;$j++)
                {
                  $pos = strpos($holder[$j]['flat'], $fid);
                  if($pos!==false)
                  {
                    $index = $j;  
                  }
                }
                ?>
                <div class="apt-list clearfix">
                  <div class="apt-image"> 
                      <?php
                        if($index!=9999)
                        {
                          ?><a href="<?php echo $holder[$index]['large'];?>" rel="shadowbox"><img src="<?php echo $holder[$index]['thumb'];?>" alt=""><span>Click to Enlarge</span></a> <span><a href="<?php echo $holder[$index]['pdf']; ?>" target="_blank" >Print PDF</a></span><?php  
                        }
                        else
                        {
                          ?><img src="<?php bloginfo("template_url");?>/img/placeholder.jpg"><?php
                        }
                      ?>
                  </div>
                  <div class="apt-detail">
                    <span class="apt-name">Building <?php echo $c3[$i]['bldngid']; ?></span>
                    <span class="apt-number">Apt. #<?php echo $c3[$i]['flatno']; ?></span>
                    <span class="apt-room"><?php echo $c3[$i]['bed']; ?> beds, <?php echo $c3[$i]['bath']; ?> baths</span>
                    <span class="apt-sqft"><?php echo $c3[$i]['area']; ?> sqft</span>
                    <span class="apt-rate">Starting at $<?php echo $c3[$i]['rent']; ?></span>
                    <span class="apt-links"><a href="#">Pre-lease now</a>
                      <?php
                        if($index!=9999)
                        {
                          ?>
                              |  <a class="apt-popup-btn" id="floor1-sitemap<?php echo $i; ?>" href="#">Site map</a>
                          <?php
                        }
                      ?>
                    </span>
                  </div>
                  <!-- popup start -->
                  <div class="floor-popup-sec floor2-sitemap<?php echo $i; ?>">
                    <div class="close"></div>
                    <div class="floor-popup-content">
                      <div class="floor-opup-content-sec clearfix">
                          <div class="floor-popup-left">
                              <h3>APT. <?php echo $c3[$i]['flatno']; ?></h3>
                              <ul>
                                  <li><?php echo $c3[$i]['bed']; ?> beds, <?php echo $f2[$i]['bath']; ?> baths</li>
                                  <li>Hardwood Floors, W/D</li>
                                  <li><?php echo $c3[$i]['area']; ?> sq ft</li>
                              </ul>
                            <p><?php echo $holder[$index]['desc']; ?></p>
                          </div>
                          <div class="floor-popup-right">
                            <img src="<?php echo $holder[$index]['sitemap']; ?>" alt="">
                            <div class="availability-link"><a href="#">Click here to view availability</a></div>
                        </div>
                      </div>  
                    </div>
                  </div>
                  <!-- popup end -->
                </div>
              <?php
            $index=9999;
            }
        ?>
    </div>
    
  </div>
  <!-- popup bg and loader -->
  <div class="loader"></div>
  <div id="backgroundPopup"></div>
  <!-- popup bg and loader -->
</div> 
<?php endwhile; ?>      
 

<?php get_footer(); ?>	