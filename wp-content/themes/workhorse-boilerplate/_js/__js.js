$(document).ready(function() {
	$("#home-slider").owlCarousel({
      navigation : false,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true
      });

	$("#owl-demo").owlCarousel({
        navigation : true,
        slideSpeed : 300,
        paginationSpeed : 400,
        autoHeight : true,
        singleItem : true
      });
/*	
	$('.bxslider').bxSlider({
        controls: true,
        pager: true,
        auto: true,
        pause: 7000

    });*/
    
    if($("body").hasClass("archive"))
    {
        $("ul#menu-main-menu li#menu-item-139").addClass("current_page_item");
        $("ul#menu-main-menu li#menu-item-139").addClass("current_menu_item");
    }


    var openMobile, closeMobile;
    openMobile = document.getElementById('hamburger');
    closeMobile = document.getElementById('all');

    FastClick.attach(openMobile);
    FastClick.attach(closeMobile);

    function toggleOpenClose() {
        //alert($("#mobile-menu").css('width'));
        var menuWidth = $("#mobile-menu").css('width');
        //alert($("#mobile-menu").css('left'));
        if ($("#mobile-menu").css('left') == "0px") {

            $("#mobile-menu").animate({
                'left': '-' + menuWidth
            }, 500, 'swing');
            $('body').removeClass("modal-open");

        } else {

            $("#mobile-menu").animate({
                'left': '0px'
            }, 500, 'swing');
            $('body').addClass("modal-open");


        }

    }

    var menuWidth = $("#mobile-menu").css('width');
    $("#mobile-menu").css('left', '-' + menuWidth);


    openMobile.addEventListener('click', function(event) {
        
		//alert('test');
		toggleOpenClose()
    }, false);

/*
    closeMobile.addEventListener('click', function(event) {


        if (!$("#mobile-menu").is(event.target) && $("#mobile-menu").has(event.target).length === 0 && $('body').hasClass("modal-open")) {
            toggleOpenClose();
        }
    }, false);
*/
	$('#reset-menu').click(function(){
		
		 $('#mobile-slide-home').animate({left: "0%"}, 500, function() {});
		 $('#sliding-sub-menu').animate({left: "100%"}, 500, function() {});
		
	});

    $('#mobile-menu a, #top-header a').not('.ignore').click(function(event) {

     event.preventDefault();
	 
	 
	 if($(this).parent().hasClass('menu-item-has-children') && $('body').hasClass('modal-open')){
		
		 var menuWidth = $('#mobile-slide-home').width();
		 //alert(menuWidth);
		 var newSubContent=$(this).parent().find('.sub-menu').html();
		 $('#sliding-sub-menu-content').html(newSubContent);
		 
		 $('#mobile-slide-home').animate({left: "-100%"}, 500, function() {});
		 $('#sliding-sub-menu').animate({left: "0%"}, 500, function() {});
		return false; 
		 
	 }
	 
     var daLink = $(this).attr('href');
     var daTarget = $(this).attr('target');

     if (daTarget == "_blank") {
         window.open(daLink);

     } else {


         if ($('body').hasClass('modal-open')) {

             toggleOpenClose();
         }

         $("#content").animate({
             'opacity': '0'
         }, 500, 'swing', function() {

             window.location.href = daLink;

         });
     }

 });
 
    $('.fill-height').each(function() {
        var parent = $('.fill-height').parent();
        var parentHeight = parent.height();
        parent.css('height', parentHeight + "");


    });

    $(document).keypress("e", function(e) {
        if (e.ctrlKey)
            window.location = '/wp-admin/post.php?post=' + theID + '&action=edit&message=1';

    });
		
	$('.left-menu li.menu-item-has-children>a').click(function(event){
		event.preventDefault();
		$(this).parent().find('.sub-menu').slideToggle();	
		$(this).parent().toggleClass('fa-caret-down');
		$(this).parent().toggleClass('fa-caret-up');
	});
	
	$('.left-menu .menu-item-has-children').each(function(){
		
		$(this).addClass('fa fa-caret-down');	
		
		$(this).find('.current-menu-item').each(function(){
			
			$(this).parent().parent().find('.sub-menu').slideToggle();	
			$(this).parent().parent().toggleClass('fa-caret-down');
			$(this).parent().parent().toggleClass('fa-caret-up');	
	
		})		
	});
	
	
	$('.scoll-link').click(function(event){
		event.preventDefault();
		
		$('html, body').animate({
			scrollTop: ($('a[name='+$(this).attr('href').replace('#','')+']').offset().top)-200
		}, 500);
		return false;
	});	
	
  /**** Contact Page ***/
 
	
  function openFakeRadio(thisone){
    thisone.toggleClass('selected');
    if(thisone.hasClass('selected')){
      thisone.prev().val('checked');
      $(".contact-page").addClass('with-tour');
    }
    else
    {
      thisone.prev().val('');
      $(".contact-page").removeClass('with-tour');
    }
  }

  $("#scedule_a_tour").click(function(){
	 
	   openFakeRadio($(this));
	  
  });
   


  $('.fake-radio').bind( "touchstart", function(e){
    var thisOneHere = $(this);
    setTimeout(function(){openFakeRadio(thisOneHere)}, 500);    
  });

  $( "#form_contact" ).submit(function( event ) {   
      var contact_form = $(this);
      event.preventDefault();
      var error=0;
      $(".req").each(function(){  
          if ($(this).val() == "" && $(this).is(":visible") )
          {
            $(this).addClass('required');
            error++;
          }
          else
          {
          $(this).removeClass('required');
          }
      });
  });

    


  /**** Contact Page ***/
	
	$("#map_menu h3").click(function(){

		$('.all-subs').slideUp();
		$(this).next().slideToggle();	
		
	});

  $("body").prepend('<div id="lightbox"><div class="its-a-table"><div class="lb-content"></div></div><div class="close-lightbox">X</div>'); 

var lightbox = $("#lightbox");
var lightbox_content = $("#lightbox .lb-content");

$("body").on("click",'#lightbox', function (e) {
      if( ! $( e.target).is('.lb-content img') ) {
            closeLightbox();
      }
});
	
function openLightbox(content){
  lightbox_content.html(content);
  lightbox.fadeIn(function(){
    lightbox_content.fadeIn();
  });
}

function closeLightbox(){
    lightbox_content.fadeOut(function(){
    lightbox.hide() 
  });
}


$(".tab-sections").on("click",'.floorplan-image', function (e) {
  var content = '<img src="'+$(this).attr('src')+'" />';
  openLightbox(content)
  
})

$('.second-content').click(function(){
  
  $(this).fadeOut();  
  
})    

  function switchImage(thisone){
    $('.floorplan-image').attr("src", "");
    $('.floorplan-image').attr("src", thisone.attr('data-image'));
  }
            
  $('.tabs>li').click(function(){
    $(this).siblings().removeClass('selected');
    $(this).addClass('selected');
    var openTab = $(this).attr('rel');
    $(".tab-sections>li").hide(); 
    $(".tab-sections>li.tab"+openTab).css('display','block');
    switchImage($(this));
    showFloors($('.tab'+$(this).attr('rel')+' .rooms li:first-child'));
  })
  
  function showFloors(thisone){
    var floors="";
    if(thisone.attr('data-image2')){
      floors+='<a href="#" class="selected-floor" data-image="'+thisone.attr('data-image')+'">1st Floor</a>&nbsp;|&nbsp;<a href="#" data-image="'+thisone.attr('data-image2')+'">2nd Floor</a>';  
    }
    
    if(thisone.attr('data-image3')){
      floors+='&nbsp;|&nbsp;<a href="#" data-image="'+thisone.attr('data-image3')+'">3rd Floor</a>';  
    }
    
    
    $(".floors").html(floors);
      
    
  }
  
  $(".tab-sections").on('click', '.rooms li', function (e) {
    
    switchImage($(this));
    
    $(this).siblings().removeClass('selected-unit');
    $(this).siblings().children('.details').slideUp();
    $(this).addClass('selected-unit');
    showFloors($(this));
    
    
  })
  
  $(".floors").on('click', 'a', function (e) {
    e.preventDefault();
    switchImage($(this));
    
  })
  
  var firstChildRoom = $(".tab0 .rooms li:first-child");
  switchImage(firstChildRoom);
  showFloors(firstChildRoom);

});