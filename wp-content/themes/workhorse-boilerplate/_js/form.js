$(document).ready(function(){
	
	$( ".wh-form" ).submit(function( event ) {
		  
		  var contact_form = $(this);
		  
		  event.preventDefault();
		  
		  var error=0;
		  
		  contact_form.find(".req").each(function(){  
				  
			  if ($(this).val() == "" && $(this).is(":visible") ){
				  
				  $(this).addClass('required');
				  error++;
			  }else{
				  $(this).removeClass('required');
			  }
			  
		  
		  });
		  
		  contact_form.find(".req-email").each(function(){  
				
			  if (!validateEmail($(this).val()) && $(this).is(":visible") ){
				 
				  $(this).addClass('required');
				  error++;
			  }else{
				  $(this).removeClass('required');
			  }
			  
		  
		  });
		  
		if(error==0){
			$.post( contact_form.attr('action'), contact_form.serialize(), function( data) {
				contact_form.next().show().text(data);
				
				
				contact_form.slideUp();
			});
		}
	});
	
	function validateEmail(email) { 
	    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	}
	
	
	
});