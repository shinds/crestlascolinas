function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

$(document).ready(function() {
	
	
	
	
	
	$('.threshold-work-img img').click(function(){
		var theSource = $(this).attr('src');
		
		$('#lightbox3 img').attr('src',theSource);
		$('#lightbox3 .lightbox2-content').show();
		$('#lightbox3').fadeIn();
	});
	
	if ((readCookie('alreadyseen') != 'yes' || cookieDisabled == 1) && popupIMG!="" ) {
		var hideOnMobileClass = '';
		if (popupMobile) {
			hideOnMobileClass = "hide-on-mobile"
		}
		var popContent = '<div id="lightbox2" class="lightbox2 '+hideOnMobileClass+'"><div  class="lightbox-table"><div class="lightbox2-inner"><div class="lightbox2-content"><div class="close-lightbox2">x</div>';
		if(popupURL!=""){
		popContent += '<a '+popupLinkTarget+' href="'+popupURL+'"><img src="'+popupIMG+'" style="display:block;" /></a>';
		}else{
			popContent += '<img src="'+popupIMG+'" style="display:block;" />';
		}
		
		popContent += '</div></div></div></div>';
		
		$('body').append(popContent);

		createCookie('alreadyseen','yes',cookieExpires);
	};
	
	window.lightboxContentHover = false;
	$(document).on("mouseenter", "#lightbox-content, .lightbox2-content",  function(event){
		
		lightboxContentHover = true;
		//alert('test');
	});
	
	$(document).on("mouseleave", "#lightbox-content, .lightbox2-content", function(event){
		
		lightboxContentHover = false;
		
	});
	
	function closeLightbox2(){
		
		$('#lightbox, .lightbox2').fadeOut(function(){
			
			$('#lightbox-content, .lightbox2-content').hide();
			
		});
	}
	
	$(document).on("click touchstart", "#lightbox, .lightbox2", function(event){
		
		if(!lightboxContentHover) {
			
			     closeLightbox2();
		} 
		
			
	});
	
	$('.close-lightbox2').click(function(){
		
		closeLightbox2();	
		
	});
	
	
	$('.fake-checkbox').click(function(){
		
		
		$('.fake-checkbox').toggleClass('checked');
		$(".contact-page").toggleClass('with-tour');
		
	});
	
	$('.parkcentral a.orange-btn').each(function(){
		
		var thisHtml = $(this).html();
		$(this).html('<span>'+thisHtml+'</span>');
		
	})
	
	$(".sumbit").click(function(event){
		event.preventDefault();
		$(this).parent().submit();
		
		
	});
	
	$("#home-slider").owlCarousel({
      navigation : false,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true
      });

	$("#owl-demo").owlCarousel({
        navigation : true,
        slideSpeed : 300,
        paginationSpeed : 400,
        autoHeight : true,
        singleItem : true
      });
/*	
	$('.bxslider').bxSlider({
        controls: true,
        pager: true,
        auto: true,
        pause: 7000

    });*/
    
    if($("body").hasClass("archive"))
    {
        $("ul#menu-main-menu li#menu-item-139").addClass("current_page_item");
        $("ul#menu-main-menu li#menu-item-139").addClass("current_menu_item");
    }


    var openMobile, closeMobile;
    openMobile = document.getElementById('hamburger');
    closeMobile = document.getElementById('all');

    FastClick.attach(openMobile);
    FastClick.attach(closeMobile);

    function toggleOpenClose() {
        //alert($("#mobile-menu").css('width'));
        var menuWidth = $("#mobile-menu").css('width');
        //alert($("#mobile-menu").css('left'));
        if ($("#mobile-menu").css('left') == "0px") {

            $("#mobile-menu").animate({
                'left': '-' + menuWidth
            }, 500, 'swing');
            $('body').removeClass("modal-open");

        } else {

            $("#mobile-menu").animate({
                'left': '0px'
            }, 500, 'swing');
            $('body').addClass("modal-open");


        }

    }


	$(window).bind('resize', function() {

       var menuWidth = $("#mobile-menu").css('width');
       
    $("#mobile-menu").css('left', '-' + menuWidth);
	
	   var windowWidth = $(window).width();

	   if (windowWidth<500){
		$("#call-now").attr("href", "tel:855.956.0610");   
	   }

    }).trigger('resize');
    


    openMobile.addEventListener('click', function(event) {
        
		//alert('test');
		toggleOpenClose()
    }, false);

/*
    closeMobile.addEventListener('click', function(event) {


        if (!$("#mobile-menu").is(event.target) && $("#mobile-menu").has(event.target).length === 0 && $('body').hasClass("modal-open")) {
            toggleOpenClose();
        }
    }, false);
*/
	$('#reset-menu').click(function(){
		
		 $('#mobile-slide-home').animate({left: "0%"}, 500, function() {});
		 $('#sliding-sub-menu').animate({left: "100%"}, 500, function() {});
		
	});

    $('#mobile-menu a, #top-header a').not('.ignore').click(function(event) {

     event.preventDefault();
	 
	 
	 if($(this).parent().hasClass('menu-item-has-children') && $('body').hasClass('modal-open')){
		
		 var menuWidth = $('#mobile-slide-home').width();
		 //alert(menuWidth);
		 var newSubContent=$(this).parent().find('.sub-menu').html();
		 $('#sliding-sub-menu-content').html(newSubContent);
		 
		 $('#mobile-slide-home').animate({left: "-100%"}, 500, function() {});
		 $('#sliding-sub-menu').animate({left: "0%"}, 500, function() {});
		return false; 
		 
	 }
	 
     var daLink = $(this).attr('href');
     var daTarget = $(this).attr('target');

     if (daTarget == "_blank") {
         window.open(daLink);

     } else {


         if ($('body').hasClass('modal-open')) {

             toggleOpenClose();
         }

         $("#content").animate({
             'opacity': '0'
         }, 500, 'swing', function() {

             window.location.href = daLink;

         });
     }

 });
 
    $('.fill-height').each(function() {
        var parent = $('.fill-height').parent();
        var parentHeight = parent.height();
        parent.css('height', parentHeight + "");


    });

    $(document).keypress("e", function(e) {
        if (e.ctrlKey)
            window.location = '/wp-admin/post.php?post=' + theID + '&action=edit&message=1';

    });
		
	$('.left-menu li.menu-item-has-children>a').click(function(event){
		event.preventDefault();
		$(this).parent().find('.sub-menu').slideToggle();	
		$(this).parent().toggleClass('fa-caret-down');
		$(this).parent().toggleClass('fa-caret-up');
	});
	
	$('.left-menu .menu-item-has-children').each(function(){
		
		$(this).addClass('fa fa-caret-down');	
		
		$(this).find('.current-menu-item').each(function(){
			
			$(this).parent().parent().find('.sub-menu').slideToggle();	
			$(this).parent().parent().toggleClass('fa-caret-down');
			$(this).parent().parent().toggleClass('fa-caret-up');	
	
		})		
	});
	
	
	$('.scoll-link').click(function(event){
		event.preventDefault();
		
		$('html, body').animate({
			scrollTop: ($('a[name='+$(this).attr('href').replace('#','')+']').offset().top)-200
		}, 500);
		return false;
	});	
	
  /**** Contact Page ***/
 
	
  function openFakeRadio(thisone){
    thisone.toggleClass('selected');
    if(thisone.hasClass('selected')){
      thisone.prev().val('checked');
      $(".contact-page").addClass('with-tour');
    }
    else
    {
      thisone.prev().val('');
      $(".contact-page").removeClass('with-tour');
    }
  }

  $("#scedule_a_tour").click(function(){
	 
	   openFakeRadio($(this));
	  
  });
   


  $('.fake-radio').bind( "touchstart", function(e){
    var thisOneHere = $(this);
    setTimeout(function(){openFakeRadio(thisOneHere)}, 500);    
  });

  $( "#form_contact" ).submit(function( event ) {  
 
      var contact_form = $(this);
      event.preventDefault();
       var error=0;
		  
		  $(".req").each(function(){  
				  
			  if ($(this).val() == "" && $(this).is(":visible") ){
				  
				  $(this).addClass('required');
				  error++;
			  }else{
				  $(this).removeClass('required');
			  }
			  
		  
		  });
		  
		  $(".req-email").each(function(){  
				
			  if (!validateEmail($(this).val()) && $(this).is(":visible") ){
				    
				  $(this).addClass('required');
				  error++;
			  }else{
				  $(this).removeClass('required');
			  }
			  
		  
		  });
		  
		if(error==0){
			$.post( contact_form.attr('action'), contact_form.serialize(), function( data) {
				$("#thank-you").text(data);
				$("#main-form").height($("#main-form").height());
				
				contact_form.slideUp();
			});
		}
  });



  

  /**** Contact Page ***/
	
	$("#map_menu h3").click(function(){

		$('.all-subs').slideUp();
		$(this).next().slideToggle();	
		
	});




  $("body").prepend('<div id="lightbox"><div class="its-a-table"><div class="lb-content"></div></div><div class="close-lightbox">X</div>'); 

var lightbox = $("#lightbox");
var lightbox_content = $("#lightbox .lb-content");

$("body").on("click",'#lightbox', function (e) {
      if( ! $( e.target).is('.lb-content img') ) {
            closeLightbox();
      }
});
	
function openLightbox(content){
  lightbox_content.html(content);
  lightbox.fadeIn(function(){
    lightbox_content.fadeIn();
  });
}

function closeLightbox(){
    lightbox_content.fadeOut(function(){
    lightbox.hide() 
  });
}


$(".tab-sections").on("click",'.floorplan-image', function (e) {
  var content = '<img src="'+$(this).attr('src')+'" />';
  openLightbox(content)
  
})

$(".tab-sections").on("click",'.enlargeprint', function (e) {
  var content = '<img src="'+$('.floorplan-image').attr('src')+'" />';
  openLightbox(content)
  
})

$('.second-content').click(function(){
  
  $(this).fadeOut();  
  
})    

  function switchImage(thisone){
    $('.floorplan-image').attr("src", "");
    $('.floorplan-image').attr("src", thisone.attr('data-image'));
  }
  
  function showpdflink(thisone){
    var datapdf = thisone.attr("data-pdf");

    if(thisone.attr("data-pdf"))
    {  
      $(".print_pdf_lnk").attr("href",thisone.attr("data-pdf"));
      $(".print_pdf_box").show();
    }
    else
    {
      $(".print_pdf_lnk").attr("href","#");
      $(".print_pdf_box").hide();
    }
  }          
  $('.tabs>li').click(function(){
    $(this).siblings().removeClass('selected');
    $(this).addClass('selected');
    var openTab = $(this).attr('rel');
    $(".tab-sections>li").hide(); 
    $(".tab-sections>li.tab"+openTab).css('display','block');
    switchImage($(this));
    showpdflink($('.tab'+$(this).attr('rel')+' .rooms li:first-child')); //Added 1/15
    showFloors($('.tab'+$(this).attr('rel')+' .rooms li:first-child'));
  })
  
  function showFloors(thisone){
    var floors="";
    if(thisone.attr('data-image2')){
      floors+='<a href="#" class="selected-floor" data-image="'+thisone.attr('data-image')+'">Modern</a>&nbsp;|&nbsp;<a href="#" data-image="'+thisone.attr('data-image2')+'">Fresh</a>';  
    }
    
    if(thisone.attr('data-image3')){
      floors+='&nbsp;|&nbsp;<a href="#" data-image="'+thisone.attr('data-image3')+'">More</a>';  
    }
    
    
    $(".floors").html(floors);
    //$("#print_pdf_box").attr("href",thisone.attr('data-pdf'));  
    
  }
  
  $(".tab-sections").on('click', '.rooms li', function (e) {
    
    switchImage($(this));
    showpdflink($(this));
    $(this).siblings().removeClass('selected-unit');
    $(this).siblings().children('.details').slideUp();
    $(this).addClass('selected-unit');    
    showFloors($(this));
    
    
  })
  
  $(".floors").on('click', 'a', function (e) {
    e.preventDefault();
    switchImage($(this));
    $('.selected-floor').removeClass('selected-floor');
    $(this).addClass('selected-floor')
    
  })
  
  var firstChildRoom = $(".tab0 .rooms li:first-child");
  showpdflink(firstChildRoom);
  switchImage(firstChildRoom);
  showFloors(firstChildRoom);

  $('.fancybox').fancybox({        
        'maxWidth': 700, // set the width
        'maxHeight': 500, // set the height
        'type': 'iframe', // tell the script to create an iframe
    });  

	$("#renters-voice").click(function(){
		var content = '<iframe src="'+rentersVoiceURL+'"></iframe>';
		openLightbox(content)
	}); 
	
	
	function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 

});




