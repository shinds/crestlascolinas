$(document).ready(function() {
	
/*	
	$('.bxslider').bxSlider({
        controls: true,
        pager: true,
        auto: true,
        pause: 7000

    });
*/	
   


    var openMobile, closeMobile;

    openMobile = document.getElementById('hamburger');
    closeMobile = document.getElementById('all');

    FastClick.attach(openMobile);
    FastClick.attach(closeMobile);



    function toggleOpenClose() {
        //alert($("#mobile-menu").css('width'));
        var menuWidth = $("#mobile-menu").css('width');
        //alert($("#mobile-menu").css('left'));
        if ($("#mobile-menu").css('left') == "0px") {

            $("#mobile-menu").animate({
                'left': '-' + menuWidth
            }, 500, 'swing');
            $('body').removeClass("modal-open");



        } else {

            $("#mobile-menu").animate({
                'left': '0px'
            }, 500, 'swing');
            $('body').addClass("modal-open");


        }

    }

    var menuWidth = $("#mobile-menu").css('width');
    $("#mobile-menu").css('left', '-' + menuWidth);


    openMobile.addEventListener('click', function(event) {
        
		//alert('test');
		toggleOpenClose()
    }, false);

/*
    closeMobile.addEventListener('click', function(event) {


        if (!$("#mobile-menu").is(event.target) && $("#mobile-menu").has(event.target).length === 0 && $('body').hasClass("modal-open")) {
            toggleOpenClose();
        }


    }, false);

*/
    


	$('#reset-menu').click(function(){
		
		 $('#mobile-slide-home').animate({left: "0%"}, 500, function() {});
		 $('#sliding-sub-menu').animate({left: "100%"}, 500, function() {});
		
	});

    $('#mobile-menu a, #top-header a').not('.ignore').click(function(event) {

     event.preventDefault();
	 
	 
	 if($(this).parent().hasClass('menu-item-has-children') && $('body').hasClass('modal-open')){
		
		 var menuWidth = $('#mobile-slide-home').width();
		 //alert(menuWidth);
		 var newSubContent=$(this).parent().find('.sub-menu').html();
		 $('#sliding-sub-menu-content').html(newSubContent);
		 
		 $('#mobile-slide-home').animate({left: "-100%"}, 500, function() {});
		 $('#sliding-sub-menu').animate({left: "0%"}, 500, function() {});
		return false; 
		 
	 }
	 
     var daLink = $(this).attr('href');
     var daTarget = $(this).attr('target');

     if (daTarget == "_blank") {
         window.open(daLink);


     } else {


         if ($('body').hasClass('modal-open')) {

             toggleOpenClose();
         }

         $("#content").animate({
             'opacity': '0'
         }, 500, 'swing', function() {

             window.location.href = daLink;

         });

     }




 });
 
    $('.fill-height').each(function() {
        var parent = $('.fill-height').parent();
        var parentHeight = parent.height();
        parent.css('height', parentHeight + "");


    });

    $(document).keypress("e", function(e) {
        if (e.ctrlKey)
            window.location = '/wp-admin/post.php?post=' + theID + '&action=edit&message=1';

    });
	
	
	
	$('.left-menu li.menu-item-has-children>a').click(function(event){
		event.preventDefault();
		$(this).parent().find('.sub-menu').slideToggle();	
		$(this).parent().toggleClass('fa-caret-down');
		$(this).parent().toggleClass('fa-caret-up');
	});
	
	$('.left-menu .menu-item-has-children').each(function(){
		
		$(this).addClass('fa fa-caret-down');	
		
		$(this).find('.current-menu-item').each(function(){
			
			$(this).parent().parent().find('.sub-menu').slideToggle();	
			$(this).parent().parent().toggleClass('fa-caret-down');
			$(this).parent().parent().toggleClass('fa-caret-up');	
			
			
			
		})
		
	});
	
	
	
	
	
	$('.scoll-link').click(function(event){
		event.preventDefault();
		
		$('html, body').animate({
			scrollTop: ($('a[name='+$(this).attr('href').replace('#','')+']').offset().top)-200
		}, 500);
		return false;
	});	
	
	
	


});

