<?php
/**
 * Template Name:Park Suite Selector
 */ 
get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="inner">
    <div id="suite-selector" class="clearfix">
        <div class="ss-left equalheight">
            <div id="suite-selector-menu">
                <ul>
                    <li class="ss-main bed"><span class="title">0+beds</span> <span class="fa down">&#xf0d7;</span><span class="fa up">&#xf0d8;</span>
                        <ul class="sub-bed suite-filter" >
                            <li><input type="checkbox" id="bed1" name="bed" value="1" /><label for="bed1"><span>1</span></label></li>
                            <li><input type="checkbox" id="bed2" name="bed" value="2" /><label for="bed2"><span>2</span></label></li>
                        </ul>
                    </li>
                    <li class="ss-main bath"><span class="title">0+ baths</span> <span class="fa down">&#xf0d7;</span><span class="fa up">&#xf0d8;</span>
                        <ul class ="sub-bath suite-filter" >
                            <li><input type="checkbox" id="bath1" name="bath" value="1" /><label for="bath1"><span>1</span></label></li>
                            <li><input type="checkbox" id="bath2" name="bath" value="2" /><label for="bath2"><span>2</span></label></li>
                        </ul>
                    </li>
                    <li class="ss-main view"><span class="title">view</span> <span class="fa down">&#xf0d7;</span><span class="fa up">&#xf0d8;</span>
                        <ul class ="sub-view">
                            <li><input type="checkbox" name="view" value="pool" id="c1" /><label for="c1"><span>pool</span></label></li>
                            <li><input type="checkbox" name="view" value="garden" id="c2" /><label for="c2"><span>garden</span></label></li>
                            <li><input type="checkbox" name="view" value="street" id="c3" /><label for="c3"><span>street</span></label></li>
                        </ul>
                    </li>
                    <li class="ss-main price">price <span class="fa down">&#xf0d7;</span><span class="fa up">&#xf0d8;</span>
                        <ul class="sub-price">
                            <!--<li><input type="checkbox" id="c4" /><label for="c4"><span>$500-$700</span></label></li>
                            <li><input type="checkbox" id="c5" /><label for="c5"><span>$701-$1000</span></label></li>
                            <li><input type="checkbox" id="c6" /><label for="c6"><span>$1001-$1500</span></label></li>-->
                        </ul>
                    </li>
                </ul>
            </div>
            <div id="ss-map">
                <!--<img src="<?php //echo $bloginfo['template_url'] ?>/_images/suite-selector/glencoe/site-map.jpg" id="map-image">-->
               <!--
                <div id="popup-building-type-1" class="pool building-type-1 building-1 building "></div>
                <div id="popup-building-type-2" class="pool building-type-1 building-2 building"></div>
                <div id="popup-building-type-3" class="pool street building-type-3 building-4 building"></div>

                <div id="popup-building-type-4" class="building-type-2 building-3 building"></div>
                <div id="popup-building-type-5" class="building-type-2 building-5 building"></div>
                <div id="popup-building-type-6" class="building-type-2 building-6 building"></div>

                <div id="popup-building-type-7" class="pool street building-type-4 building-7 building"></div>
                <div id="popup-building-type-8" class="pool street building-type-5 building-8 building"></div>
                <div id="popup-building-type-9" class="pool street building-type-6 building-9 building"></div>
                <div id="popup-building-type-10" class="pool building-type-7 building-10 building"></div>
                <div id="popup-building-type-11" class="pool building-type-7 building-11 building"></div>
                <div id="popup-building-type-12" class="pool building-type-7 building-12 building"></div>-->
                <div class="map-sec"><img src="<?php echo $bloginfo['template_url'] ?>/map/map.jpg" width="740" height="674" usemap="#Map" border="0" />
				    <div class="suite  building1 b-1107 b-1207 b-1307"></div>
				    <div class="suite  building2 b-1105 b-1205 b-1305"></div>
				    <div class="suite  building3 b-1103 b-1203 b-1303"></div>
				    <div class="suite  building4 b-1133 b-1233 b-1333"></div>
				    <div class="suite  building5 b-1135 b-1235 b-1335"></div>
				    <div class="suite  building6 b-1137 b-1237 b-1337"></div>
				    <div class="suite  building7 b-1139 b-1239 b-1339"></div>
				    <div class="suite  building8 b-1230 b-1330"></div>
				    <div class="suite  building9 b-1228 b-1328"></div>
				    <div class="suite  building10 b-1226 b-1326"></div>
				    <div class="suite  building11 b-1124 b-1224 b-1324"></div>
				    <div class="suite  building12 b-1102 b-1202 b-1302"></div>
				    <div class="suite  building13 b-1104 b-1204 b-1304"></div>
				    <div class="suite  building14 b-1106 b-1206 b-1306"></div>
				    <div class="suite  building15 b-1209 b-1309"></div>
				    <div class="suite  building16 b-1211 b-1311"></div>
				    <div class="suite  building17 b-1213 b-1313"></div>
				    <div class="suite  building18 b-1215 b-1315"></div>
				    <div class="suite  building19 b-1108 b-1208 b-1308"></div>
				    <div class="suite  building20 b-1110 b-1210 b-1310"></div>
				    <div class="suite  building21 b-1112 b-1212 b-1312"></div>
				    <div class="suite  building22 b-1114 b-1214 b-1314"></div>
				    <div class="suite  building23 b-1116 b-1216 b-1316"></div>
				    <div class="suite  building24 b-1229 b-1329"></div>
				    <div class="suite  building25 b-1231 b-1331"></div>
				    <div class="suite  building26 b-2201 b-2301"></div>
				    <div class="suite  building27 b-2203 b-2303"></div>
				    <div class="suite  building28 b-2205 b-2305"></div>
				    <div class="suite  building29 b-2207 b-2307"></div>
				    <div class="suite  building30 b-3201 b-3301"></div>
				    <div class="suite  building31 b-3203 b-3303"></div>
				    <div class="suite  building32 b-3205 b-3305"></div>
				    <div class="suite  building33 b-3107 b-3207 b-3307"></div>
				    <div class="suite  building34 b-2102 b-2202 b-2302"></div>
				    <div class="suite  building35 b-2104 b-2204 b-2304"></div>
				    <div class="suite  building36 b-2106 b-2206 b-2306"></div>
				    <div class="suite  building37 b-2108 b-2208 b-2308"></div>
				    <div class="suite  building38 b-2110 b-2210 b-2310"></div>
				    <div class="suite  building39 b-3102 b-3202 b-3302"></div>
				    <div class="suite  building40 b-3104 b-3204 b-3304"></div>
				    <div class="suite  building41 b-3106 b-3206 b-3306"></div>
				    <div class="suite  building42 b-3108 b-3208 b-3308"></div>
				    <div class="suite  building43 b-3110 b-3210 b-3310"></div>    
				    <div class="suite  building44 b-7101 b-7201"></div>
				    <div class="suite  building45 b-7103 b-7203"></div>
				    <div class="suite  building46 b-7105 b-7205"></div>
				    <div class="suite  building47 b-7107 b-7207"></div>
				    <div class="suite  building48 b-7109 b-7209"></div>
				    <div class="suite  building49 b-7111 b-7211"></div>
				    <div class="suite  building50 b-7113 b-7213"></div>
				    <div class="suite  building51 b-7102 b-7202"></div>
				    <div class="suite  building52 b-7104 b-7204"></div>
				    <div class="suite  building53 b-7106 b-7206"></div>
				    <div class="suite  building54 b-7108 b-7208"></div>
				    <div class="suite  building55 b-7110 b-7210"></div>
				    <div class="suite  building56 b-7112 b-7212"></div>
				    <div class="suite  building57 b-1217 b-1317"></div>
				    <div class="suite  building58 b-1219 b-1319"></div>
				    <div class="suite  building59 b-1221 b-1321"></div>
				    <div class="suite  building60 b-1223 b-1323"></div>
				    <div class="suite  building61 b-1225 b-1325"></div>
				    <div class="suite  building62 b-1227 b-1327"></div>
				    <div class="suite  building63 b-6101"></div>
				    <div class="suite  building64 b-6102"></div>
				    <div class="suite  building65 b-6103"></div>
				    <div class="suite  building66 b-6104"></div>
				    <div class="suite  building67 b-6105"></div>
				    <div class="suite  building68 b-5101"></div>
				    <div class="suite  building69 b-5102"></div>
				    <div class="suite  building70 b-5103"></div>
				    <div class="suite  building71 b-5104"></div>
				    <div class="suite  building72 b-4101"></div>
				    <div class="suite  building73 b-4102"></div>
				    <div class="suite  building74 b-4103"></div>
				    <div class="suite  building75 b-4104"></div>
				    <div class="suite  building76 b-3111 b-3211"></div>
				    <div class="suite  building77 b-3109 b-3209"></div>
				    <div class="suite  building78 b-3114 b-3214"></div>
				    <div class="suite  building79 b-3112 b-3212"></div>
				</div>
            
            <?php
                if(get_field('parent'))
                {
                  $count = 1;
                  while(has_sub_field('parent'))
                  {
                    ?>
                        <div class="popup-building-type-<?php echo $count; ?> popup-sec popup-type-<?php echo $count; ?>">
                            <div class="close"></div>
                            <div class="popup-content">
                                <div class="popup-content-sec clearfix">
                                    <div class="popup-left">
                                        <h3><?php echo get_sub_field('building_name'); ?></h3>
                                        <?php echo get_sub_field('amenities'); ?>
                                    </div>
                                    <div class="popup-right">
                                        <img src="<?php echo get_sub_field('building_image'); ?>" alt="">
                                    </div>
                                </div>
                                <div class="popup-btn"><a class="orange-btn" href="<?php echo get_sub_field('availability_link'); ?>">AVAILABLE APARTMENTS</a></div>
                            </div>
                        </div>
                        <?php
                            $count++;
                  }
                }
            ?>
            
           
	    <div class="loader"></div>
	   	<div id="backgroundPopup"></div>
                
            </div>
        </div>
        <div class="ss-right equalheight">
            <div id="my-loader" style="display:none;" ><img class="loader-img" src="<?php bloginfo('template_url'); ?>/img/load.gif"/></div> 
            <div id="available-apartments-header">AVAILABLE APARTMENTS <a class="view-all" href="/apartment-listings/">VIEW ALL</a></div>
            
            <div class="content mCustomScrollbar appartment-list-sec">
      			  
              <ul class="appartment-list" id="list-apart">
            </ul>
			 </div>
        </div>
    </div><!-- /suite-selector-->
    <!--<p id='click'>CLICK</p>--><!-- CLICK -->
</div> 
<?php endwhile; ?>                
    
    <?php
        $arr_view = array();
        $arr_view['garden']= get_field('garden_view');
        $arr_view['pool']= get_field('pool_view');
        $arr_view['street']= get_field('street_view');
    ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
var views = jQuery.parseJSON('<?php echo json_encode($arr_view);?>');
    $(document).ready(function(){
           
        /* Binding Pricelist using PickList method of RealPage */
        $.ajax({
            method: "POST",
            url: "http://crestatparkcentral.com/wp-content/themes/workhorse-boilerplate/parkprice.php",
            beforeSend: function(){
              $("#my-loader").show();
            }
          }).done(function(priceback) {
              xmlDoc = $.parseXML( priceback ),
              $xml = $( xmlDoc ),
              $UnitObject = $xml.find( "PicklistItem" );
              $( "li.price ul.sub-price" ).empty();
              $UnitObject.each(function(index, element) {
                $( "li.price ul.sub-price" ).append('<li><input name="price" id="'+$(element).find('Value').text()+'"  value="'+$(element).find('Text').text()+'"  type="checkbox" ><label for="'+$(element).find('Value').text()+'"><span> $'+$(element).find('Text').text().split('.00').join("").replace(' - ',' - $')+'</span></label></li>');
            });
			var lastPrice = $( "li.price ul.sub-price li:last input").val().split('-')[1].trim().split('.00').join("");
			$( "li.price ul.sub-price" ).append('<li><input name="price" id="lastPrice"  value="'+lastPrice+'-'+(lastPrice*100)+'"  type="checkbox" ><label for="lastPrice"><span> > $'+lastPrice+'</span></label></li>');
		         $("#my-loader").hide();
             bindEvents();
          });   
          /* Binding Pricelist using PickList method of RealPage (over) */
		  		
        /*checkbox will select only one value at a time*/

      /* List call to retrieve data on ready */
       
      getData(0,0,0);
 	
	
	$("#suite-selector input[type=checkbox]").click(function(){
		if($(this).attr('checked')){
			$('.'+$(this).attr('rel')).css('opacity','1');
		}else {
			$('.'+$(this).attr('rel')).css('opacity','0');
		}
	})
	
	$("li.ss-main").hover(function(){
    $("li.ss-main").removeClass('ss-show-sub');
    $(this).toggleClass('ss-show-sub');	
    $(this).hover(function(){ $(this).toggleClass('ss-show-sub'); });
	});	

});/* ready over */

 
 
function mapData(bedVal,bathVal,priceVal,click){
  $.ajax({
    method: "POST",
    url: "http://crestatparkcentral.com/wp-content/themes/workhorse-boilerplate/park-soap.php",
    data: { bed: bedVal , bath: bathVal, price:priceVal, click:click },
      beforeSend: function(){
        $("#my-loader").show();
      }
  }).done(function(xml) {
    //jQuery("#xmlList").html(response);
     xmlDoc = $.parseXML( xml ),
      $xml = $( xmlDoc ),
      $UnitObject = $xml.find( "UnitObject" );
      $( "#list-apart" ).empty();
      //alert($UnitObject.length);
      var hightlight = "";
      var i = 0;
      $UnitObject.each(function(index, element) {
      var tempid = $(element).find('UnitNumber').text();
      if($.inArray(tempid,click)!='-1')
      {
        $( "#list-apart" ).append('<li class="'+getLiClass($(element).find('Address').find('UnitNumber').text())+'" data-price="'+$(element).find('BaseRentAmount').text()+'"><span>'+$(element).find('Address').find('Address1').text()+', Floor - '+$(element).find('UnitDetails').find('FloorNumber').text()+'</span>'+$(element).find('UnitDetails').find('Bedrooms').text()+' bed, '+$(element).find('UnitDetails').find('Bathrooms').text()+' bath, '+$(element).find('UnitDetails').find('RentSqFtCount').text()+' sqft, '+'Starting at $'+$(element).find('BaseRentAmount').text().replace('.0000','')+' <a href="#">Pre-lease now</a>'+'</li>');
      }
      filterViews();
    });

      $("#my-loader").hide();
      console.log(hightlight.slice(0,-1));
      click.shift();
 	  setActive(click);     
    });       
}
 
function setActive(click){
	//setTimeout(function(){
		$("div.map-sec div").each(function(){
    	$(this).removeClass('active');
	  	});	  	 

		for(var i=0;i<click.length;i++)
		{
			 
			var className = click[i];	
			var clstr = 'div.map-sec div.b-'+className;
			console.log(clstr);
			$(clstr).addClass('active');
		}
	//},1500);
}


function getData(bedVal,bathVal,priceVal){
	var click = [];
	$.ajax({
	  method: "POST",
	  url: "http://crestatglencoe.com/wp-content/themes/workhorse-boilerplate/soap.php",
	  data: { bed: bedVal , bath: bathVal, price:priceVal},
      beforeSend: function(){
        $("#my-loader").show();
      }
	}).done(function(xml) {
		//jQuery("#xmlList").html(response);
		 xmlDoc = $.parseXML( xml ),
		  $xml = $( xmlDoc ),
		  $UnitObject = $xml.find( "UnitObject" );
		  $( "#list-apart" ).empty();
		  //alert($UnitObject.length);
		  var hightlight = "";
		  var i = 0;
		  $UnitObject.each(function(index, element) {
			$( "#list-apart" ).append('<li class="'+getLiClass($(element).find('Address').find('UnitNumber').text())+'" data-price="'+$(element).find('BaseRentAmount').text()+'"><span>'+$(element).find('Address').find('Address1').text()+', Floor - '+$(element).find('UnitDetails').find('FloorNumber').text()+'</span>'+$(element).find('UnitDetails').find('Bedrooms').text()+' bed, '+$(element).find('UnitDetails').find('Bathrooms').text()+' bath, '+$(element).find('UnitDetails').find('RentSqFtCount').text()+' sqft, '+'Starting at $'+$(element).find('BaseRentAmount').text().replace('.0000','')+' <a href="#">Pre-lease now</a>'+'</li>');
			//console.log($(element).find('UnitNumber').text());
			click.push($(element).find('UnitNumber').text());
			filterViews();
		});
      $("#my-loader").hide();
      setActive(click);
	  });       
}
    
	/* Views filter logic */
	function filterViews(){
		var click=[];
		if(jQuery( 'ul.sub-view li input[name=view]:checked').length){
			var viewVal = jQuery( 'ul.sub-view li input[name=view]:checked' ).val();
			$( "#list-apart li" ).not('.hiddenPrice').hide().addClass('hiddenView');
			$('li.view span.title').text(viewVal);
			$( "#list-apart li."+viewVal ).not('.hiddenPrice').show().removeClass('hiddenView');
			$( "#list-apart li:visible span").each(function(){
				var cont = $(this).text();
				var index = cont.indexOf("#");
				var flat = cont.substr(index+1,4);
				click.push(flat);
				setActive(click);     
			});
		}else{
			$('li.view span.title').text('view');
      		$( "#list-apart li.hiddenView" ).show().removeClass('hiddenView');

		}
		filterPrice();
	}
	function filterPrice(){
		var click = [];
		if(jQuery( 'ul.sub-price li input[name=price]:checked').length){
			var priceVal = jQuery( 'ul.sub-price li input[name=price]:checked' ).val().split('-');
			var minPrice = parseFloat(priceVal[0].trim());
			var maxPrice = parseFloat(priceVal[1].trim());
			$( '#list-apart li').not('.hiddenView').hide().addClass('hiddenPrice');
			var hightlight = "";
			$( '#list-apart li.hiddenPrice').each(function(i,e){
				if(parseFloat($(this).attr('data-price')) > minPrice && parseFloat($(this).attr('data-price')) <= maxPrice){
					var line = $(this).text();
					var index = line.indexOf('#');
					var unitid = line.substr(index+1,4);
					click.push(unitid);

					$(this).show().removeClass('hiddenPrice');	
				}
			});
			 
			//alert($.type(click));
			//alert(click);
			setActive(click);

		}else{	
			$( '#list-apart li.hiddenPrice').show().removeClass('hiddenPrice');
		}
	}
	
	function getLiClass(unitNumber){
		var liClass = "";
		$.each(views,function(k,v){
			var numbers = v.split(',');
			if($.inArray(unitNumber,numbers) > -1){
				liClass += k+" ";
			}
		});
		return liClass;
	}
	
	
  function bindBoxClicks(element){
	   var $box = $(element);
		if ($box.is(":checked")) {
			var group = "input:checkbox[name='" + $box.attr("name") + "']";
			$(group).prop("checked", false);
			$box.prop("checked", true);
		} else {
			$box.prop("checked", false);
		}
  }

    /* bind events */
	function bindEvents(){
        
        /*checkbox will select only one value at a time*/
        $("ul.sub-bed li input:checkbox").on('click', function() {
           bindBoxClicks($(this));
        });
        $("ul.sub-bath li input:checkbox").on('click', function() {
            bindBoxClicks($(this));
        });
        $("ul.sub-view li input:checkbox").on('click', function() {
            bindBoxClicks($(this));
			filterViews();
        });
		$("ul.sub-price li input:checkbox").on('click', function() {
           bindBoxClicks($(this));
		   filterPrice();
		});
   
        jQuery('ul.suite-filter li input[type=checkbox]').change(function(){
            var priceVal = 0;
			var bathVal = 0;
			var bedVal = 0;
			
			/*if(jQuery('ul.sub-price li input[name=price]:checked').length){
				priceVal = jQuery('ul.sub-price li input[name=price]:checked').val();
			}*/

			//set timeout for clearing previous selection on click event above
			setTimeout(function(){ 
					if(jQuery('ul.sub-bath li input[name=bath]:checked').length){
		            	bathVal = jQuery('ul.sub-bath li input[name=bath]:checked').val();
		            	//alert(bathVal);
					}
					if(jQuery('ul.sub-bed li input[name=bed]:checked').length){
		            	bedVal = jQuery('ul.sub-bed li input[name=bed]:checked').val();
		            	//alert(bedVal);
					}
					 
		      		var bathText="";
					if(!bathVal)
					{
						bathText = "0+ Baths";
					}
					else if(bathVal == "1")
					{
						bathText = bathVal+" Bath";
					}
					else
					{
						bathText = bathVal+" Baths";
					}
					jQuery("li.bath span.title").text(bathText);
					
					
					var bedText="";
					if(!bedVal)
					{
						bedText = "0+ Beds";
					}
					else if(bedVal == "1")
					{
						bedText = bedVal+" Bed";
					}
					else
					{
						bedText = bedVal+" Beds";
					}
					jQuery("li.bed span.title").text(bedText);
		            
					jQuery("li.ss-show-sub").removeClass('ss-show-sub');    

		            getData(bedVal,bathVal,priceVal);

        	}, 300);
             
        }); 
    }
    /* bind PRICE events */       
    
    
    equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
    equalheight('.equalheight');
    var selectorHeight = $('#suite-selector #map-image').height();
    $('.appartment-list-sec').css("min-height", selectorHeight-30);

});


$(window).resize(function(){
    equalheight('.equalheight');
    var selectorHeight = $('#suite-selector #map-image').height();
    $('.appartment-list-sec').css("min-height", selectorHeight-30);

});
   
    
</script>	


	<script src="<?php bloginfo('template_directory'); ?>/_js/jquery.mCustomScrollbar.concat.min.js"></script>

	<script>
		var ar = [];
		jQuery(document).ready(function(){
	 		jQuery("div.map-sec div").click(function(){
	 			var cname = jQuery(this).attr('class');
	 			ar = cname.split(" "); 
	 			//alert(ar); 
	 			for(var i=0; i<ar.length;i++)
	 			{
	 				ar[i] = ar[i].substr(2);
	 			}
	 			ar.shift();ar.shift();
	 			//alert(ar); 
	 			var click = ar;
	 			mapData(0,0,0,click);
	 		});
		});
	</script>

    
<?php get_footer(); ?>	