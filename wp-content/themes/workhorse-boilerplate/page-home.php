<?php /** * Template Name: Home Page */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div class="variable-font-size">
		<div id="slider-holder">
			
            	<?php $slides = get_field('photo'); 

				if($slides){ ?>
					<div id ="home-logo-holder">
						<img alt="Welcome to <?php echo $blog_title = get_bloginfo('name'); ?>" src="<?php echo get_field('hero_welcome_banner', 'option')?>"  />								
					</div>
					<div id="home-slider" class="owl-carousel owl-theme">
	
						<?php foreach($slides as $slide){ 
							$image = wp_get_attachment_image_src($slide['image'], 'full');
						?>
							<div class="item" style="background-image: url('<?php echo $image[0] ?>');">
								<img alt="<?php echo $blog_title = get_bloginfo('name'); ?>" src="<?php echo $image[0] ?>" />
							</div>
						<?php } ?>	
				<?php } ?>
        			</div>
		</div>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<div class="main-pattern-section">
						<div class="entry-content">
							<div class="inner clearfix">
									<?php //the_content(); ?>
								<div class="left">
									<?php echo the_field('content_left')?>	
								</div>
								<div class="right">
									<?php echo the_field('content_right')?>		
								</div>
							</div><!-- .inner -->
						</div><!-- .entry-content -->
					</div><!-- .main-pattern-section -->
					<div class="wood-grain-section content-box-sec" style="min-height:500px;">
							<div class="entry-content">
									<div class="inner clearfix" style="position:relative;">							
										<div class="content-box-left"><?php echo the_field('content_full_width')?></div>
										<div style="" id="bubble-holder">
											<!--<img src="/wp-content/themes/workhorse-boilerplate/_images/global/home-circles.png" />-->
											<div class="row">
											  <ul class="carousel">
                                              
                                              <?php $bubble_images = get_field('bubble_slider'); ?>
												<li class="item active" style="background-image:url(<?php $image =  wp_get_attachment_image_src( $bubble_images[0]['bubble_photo'], 'full'); echo $image[0]; ?>);"></li>
												<li class="item middle" style="background-image:url(<?php $image =  wp_get_attachment_image_src( $bubble_images[1]['bubble_photo'], 'full'); echo $image[0]; ?>);"></li>
												<li class="item" style="background-image:url(<?php $image =  wp_get_attachment_image_src( $bubble_images[2]['bubble_photo'], 'full'); echo $image[0]; ?>);""></li>

											  </ul>
											</div>	
									  </div>		
								</div><!-- .entry-content -->
							</div><!-- .inner -->
						</div><!-- .woodgrainsection -->

				</article><!-- #post-## -->
</div><!-- .variable-font-size-->
<?php endwhile; ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>