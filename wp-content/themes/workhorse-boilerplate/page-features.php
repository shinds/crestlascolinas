<?php /** * Template Name: Features Page */ ?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div class="gutter">
	<div class="inner">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>	
					<div class="main-pattern-section featured-page">
						<div class="entry-content clearfix">
							
									<?php //the_content(); ?>
								<div class="left">
									<div class="col1"><?php echo the_field('content_left_list_one')?></div>	
									<div class="col2"><?php echo the_field('content_left_list_two')?></div>
								</div>
								<div class="right">
									<?php echo the_field('content_right')?>		
								</div>
							
						</div><!-- .entry-content -->
					</div><!-- .main-pattern-section -->
				</article><!-- #post-## -->
                
                </div>
                </div>
<?php endwhile; ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>