<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div class="gutter">
	<div class="inner">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>	
					<div>
						<div class="entry-content clearfix">
							<h1 align="center"><?php the_title(); ?></h1>									
									<?php the_content(); ?>
						</div><!-- .entry-content -->
					</div><!-- .main-pattern-section -->
				</article><!-- #post-## -->
                
                </div>
                </div>
<?php endwhile; ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>