<?php /** * Template Name: Splash Page */ 
global $bloginfo; 
$current_blog = get_current_blog_id();
$featured_color=get_field( 'featured_color'); ?>
<?php the_post() ?>
<html lang="en">

<head>
    <meta charset="utf-8">

    <title>
        <?php echo $bloginfo[ 'name']; ?>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="<?php echo substr(get_the_content(),0,150) ?>...">
    <meta name="author" content="Lennar">

    <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/_css/reset.css">
    <style type="text/css">
	
	#lennar-community {
		
		color:#fff; 
		font-size:15px; line-height:16px;
		position:fixed;	bottom:10px;
		left:3%; text-transform:uppercase;
		letter-spacing:.2em;
		
		
		}
	
	
        .extra-padding {
            padding-left: 3%;
            padding-right: 3%;
        }
       
        #logo {
            margin: 20px 0px;
            width:auto;
            height: 112px;
        }
        body,
        html,
        #all {
            height: 100%;
        }
        body {
            font-family: Helvetica, Arial, sans-serif;
        }
        body {
            background-size: cover;
            background-position: center center;
        }
        a {
            color: #<?php echo $featured_color ?>
        }
        .center {
            text-align: center;
        }
        .inner {
            position: relative;
            max-width: 580px;
        }
        #content {
            font-size: 17px;
            line-height: 22px;
            font-weight: bold;
            color: #fff;
            letter-spacing: 1px;
            margin-bottom: 15px;
			text-align:left;
			padding:20px 10px 5px 10px;
			background: rgba(47,55,57, .6);
        }
		 #content p {margin-bottom:1em;}
		
	
        .tint {
            background: rgba(110, 175, 255, .4);
            padding: 2% 2% 100px 2%;
        }
        #form .btn {
            background-color: #<?php echo $featured_color ?>;
            color: #fff;
            text-transform: uppercase;
            max-width: 400px;
            width: 80%;
            margin: 0px auto;
            font-weight: bold;
            font-size: 18px;
            line-height: 19px;
            padding: 10px 0px;
            text-align: center;
        }
        #form {
            max-width: 600px;
            color: #fff;
            
        }
		
		.tab-content {display: none; margin-bottom:15px;}
		
		.typical-tab-content, .thank-you {width:90%; padding:20px 5% 30px 5%; background-color:#fff; color:#000;}
		.thank-you {display:none;}
        #form label {
            display: inline-block;
            background-color: #<?php echo $featured_color ?>;
            color: #fff;
            width: 20%;
            vertical-align: top;
            text-align: left;
            margin-bottom: 15px;
            height: 26px;
            padding-top: 5px;
            padding-left: 2%;
            padding-right: 2%;
        }
        #form input[type=text] {
            display: inline-block;
            width: 76%;
            padding-left: 2%;
            padding-right: 2%;
            vertical-align: top;
            border: 1px solid #<?php echo $featured_color ?>;
            height: 31px;
        }
        #form input[type=submit] {
            color: #fff;
            background-color: #<?php echo $featured_color ?>;
            border: none;
            outline: 3px solid #fff;
            float: right;
            padding: 10px;
            margin-right: 1px;
        }
        #form textarea {
            border: 1px solid #<?php echo $featured_color ?>;
            padding: 10px 2%;
            width: 100%;
            height: 200px;
            margin-bottom: 15px;
        }
        .btn-tab {
            text-transform: uppercase;
            color: #fff;
            background-color: #<?php echo $featured_color ?>;
            padding: 5px 5% 3px 5%;
            cursor: pointer;
			width:90%;
			text-align:right;
			margin-bottom:15px;
        }
		
		.btn-tab:after {
			content:'\25BA'
		}
		
		.active:after {
			content:'\25BC' !important;
		}
        .required {
            border: 1px solid red !important;
        }
        .shadow-text-blue {
            text-shadow: 1px 1px 1px #7c99b8;
        }
        .shadow-text-black {
            text-shadow: 1px 1px 1px #000000;
        }
		
		#address {position:fixed; bottom:0px; right:0px; text-transform:uppercase; color:#fff; letter-spacing:1px; font-weight:normal; font-size:18px; line-height:22px; padding:10px 20px; background-color:rgba(0,0,0,.6); max-width:850px; }
		#disclaimer {font-size:13px; line-height:18px; padding:20px 0px; text-transform:none;}
		#address li {list-style:none; padding:0px 10px; display:inline-block; border-left:2px solid #fff;}
		#address li a {color:inherit; text-decoration:none;}
		#address li:first-child {border-left:none;;}
		#mobile-image {display:none;}
		#amenities ul {list-style:none;}
		#amenities li {padding-bottom:4px;}
		
		.too-tall, .too-tall #all {height: auto !important;}
		.contact-fn {font-size:13px; line-height:15px; padding-top:10px;}
		#equal-housing {position:fixed; bottom:10px; right:10px; display:block; width:60px; height:auto;}
		@media (max-width: 1500px) {
		
		#address li {list-style:none; padding:5px 0px; display:block; border-left:none; text-align:right;}
		#address {max-width:400px;}
		}
		
		@media (max-width: 1120px) {
			#address {position:relative; bottom:auto; right:auto; text-align:left; margin-bottom:10px; padding:10px; max-width:none;}
			#address li {list-style:none; padding:2px 0px; border-left:none; font-size:14px; line-height:18px; text-align:left; }
		}
		
		@media (max-width: 720px) {
			#equal-housing {position:relative; bottom:auto; right:auto; display:block; width:60px; height:auto; float:right; margin:0px 20px 20px 0px;}
		}
		
        @media (max-width: 600px),(max-height: 500px) {
            body,
            html,
            #all {
                height: auto;
            }
            #all {
                padding-bottom: 100px;
            }
			#logo {
            
            height: 82px;
        }
		
		#address li { padding:0px 10px; display:block;}
		#amenities {padding:5px 2px;}
		.tab-content {display: block; background-color:transparent; color:#fff;}
		.btn-tab {text-align: left;}
		.btn-tab:after {
			content:'';
			
		}
		
		#content {
            font-size: 14px;
            line-height: 18px;
           
            letter-spacing: 0px;
          
			text-align:left;
        }
		body {background-image:none !important; background-color:#7c99b8;}
		#mobile-image {display:block; width:100%; height:auto;}
		#lennar-community {font-size:12px; position:relative; bottom:auto; left:auto; text-align:center;}
        }
       
    </style>
    <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->


</head>


<?php $logo=wp_get_attachment_image_src(get_field( 'logo'), 'large'); $image=wp_get_attachment_image_src(get_field( 'rendering'), 'full'); ?>

<body style="background-image:url('<?php echo $image[0]; ?>')">
    <div id="all">





        <div id="main-photo">

            
                <div class="extra-padding">

                    <div class="inner">
                        <img src="<?php echo $logo[0] ?>" alt="<?php echo image_name_as_alt($logo[0]) ?>" id="logo" />
                        
                        <img src="<?php echo $image[0] ?>" alt="<?php echo image_name_as_alt($image[0]) ?>" id="mobile-image" />
                        
                        
                         
						<div class="btn-tab active">Info&nbsp;&nbsp;</div>
                        <div id="content" class="typical-veritcal-padding shadow-text-blue tab-content" style="display:block;">
                            <?php the_content() ?>
                        </div>
                       

                        <?php $amenities = get_field('amenities'); ?>
						
                        <?php if($amenities){ ?>
						<div class="btn-tab">Amenities&nbsp;&nbsp;</div>

                        <div id="Amenities" class="tab-content typical-tab-content">
                        
                        <ul>	
                        
                          <?php 
						  
						  $amenities_explode = explode("\n",$amenities);
						  if($amenities_explode){
							  
							  foreach($amenities_explode as $amenity){
							  
							if(strlen($amenity)>4){
								
								if($amenity[0]=="*"){
									
									echo $amenity;
									
								;}else {
									echo '<li>'.$amenity.'</li>';
								
								;}  
							  }
						  }
						  }
						  ?>

</ul>
                        </div>
                        
                        <?php ;} ?>
                        
                        <div class="btn-tab">Sign up here for details&nbsp;&nbsp;</div>

                        <div id="form" class="tab-content">
                            <?php $link=$_SERVER[ 'REQUEST_URI']; $link_explode=explode( "?",$link); ?>
                            <form action="<?php echo $bloginfo['template_url'] ?>/_process/forms.php" method="post" class="wh-form"><input type="hidden" name="form-title" value="Splash Page Form"><input type="hidden" name="page_id" value="<?php the_ID(); ?>"><label>Name:</label><input type="text" name="full_name" class="req"><label>Email:</label><input type="text" name="email" class="req-email"><label>Phone:</label><input type="text" name="phone"><textarea placeholder="Message" name="Message"></textarea><br><input type="checkbox" style="margin-bottom:10px;" CHECKED value="Subscribe me" /> <span style="text-transform:uppercase; " class="shadow-text-black">Subscribe to Mailing List</span><input type="text" name="fill_this_out" class="fill-this-out" /><input type="submit" value="Submit"><div class="clearfix"></div><div class="contact-fn">By submitting this form, you agree that we may communicate with you from time to time via e-mail about what's happening at our community. We'll never sell your information. Contact leasing office for more information.</div></form><div class="thank-you"></div>

                        </div>
                        
                        
                        
                        <?php 
						$street_address = get_field('street_address');
						$city_state_zip = get_field('city_state_zip');
						$phone_number = get_field('phone_number');
						?>
                        
                        <?php if($street_address || $city_state_zip || $phone_number) { ?>
                        <div id="address">
                        	<ul>
                            	<?php 
								
								if($street_address){echo "<li>".$street_address."</li>";}
								if($city_state_zip){echo "<li>".$city_state_zip."</li>";}
								if($phone_number){echo '<li><a href="tel:'.$phone_number.'">'.$phone_number.'</a></li>';}
								 echo '<li>A Lennar Community</li>';
								?>
                                
							</ul>
                            <div id="disclaimer">
                            
                            
                            The owner and management company for this community comply fully with the provisions of the equal housing opportunity laws and nondiscrimination laws. The apartment homes have been designed and constructed to be accessible in accordance with those laws.
                            
                            
                         	</div>
                         </div>
                        
                        <?php ;} ?>
                        
                    </div>

					  
                      
                </div>
          
		
        <img alt="Equal Housing Opportunity" src="/wp-content/themes/workhorse-boilerplate/_images/global/logo-eqh.png" id="equal-housing" />
        
        </div>

    </div>
    
  
    <script>
        window.templateUrl = "<?php echo $bloginfo['template_url'] ?>";
    </script>
    <script type='text/javascript' src='//code.jquery.com/jquery-1.10.2.min.js?ver=3.9.1'></script>
    <script type='text/javascript' src="<?php echo $bloginfo['template_url']; ?>/_js/form.js"></script>
    <script>
        $(document).ready(function() {
			
			
			(function($) {
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);

	

            $('.btn-tab').click(function() {
				
				var windowWidth = $(window).width();
				
				if(windowWidth>600){
					
				var thisClicked = $(this);
				
					
				$('.tab-content').not($(this).next()).slideUp();	
				$('.btn-tab').not($(this)).removeClass('active');
					
				
                $(this).toggleClass('active');
                $(this).next().slideToggle(function(){
					
					if($('body').hasScrollBar()){
						
					$('body').addClass('too-tall');
					$('html').css('height','auto');
					}else{
						
						$('body').removeClass('too-tall');
						$('html').css('height','100%');
						
					}
					
				});
				
				}

            });




        });
    </script>
</body>

</html>

