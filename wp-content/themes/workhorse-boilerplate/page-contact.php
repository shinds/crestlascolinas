<?php
/**
 	Template Name: Page Contact
 *

 */

global $bloginfo;
 
if(isset($_POST['fill_this_out']) && $_POST['fill_this_out']==""){
	
	$message = "";
	
	foreach($_POST as $key => $value)
		{
			if($key!="fill_this_out" && $key!="page_id"){
				
				$message .= str_replace('_',' ',$key) .": ".$value."\n";
			
			}
		}
	
	$sendto = get_field('send_to',$_POST['page_id']);
	
	$headers = "From:" . $bloginfo['name'] . " <no-reply@" . $_SERVER['SERVER_NAME']	. ">" . "\r\n" . "Reply-To: " . $_POST["Email_Address"] . "\r\n";
	
	$move_in_date = $_POST['Appointment_Date_New'];
	if($move_in_date!=""){
		
		
		$old_date_timestamp = strtotime($move_in_date);
		$new_date = date('m/d/Y', $old_date_timestamp);  
		$move_in_date =  $new_date;
	}
	
	$lead_to_lease_message = 'First Name: '.$_POST['First_Name'].PHP_EOL;
	$lead_to_lease_message .= 'Last Name: '.$_POST['Last_Name'].PHP_EOL;
	$lead_to_lease_message .= 'Address: '.PHP_EOL;
	$lead_to_lease_message .= 'Address2: '.PHP_EOL;
	$lead_to_lease_message .= 'City: '.PHP_EOL;
	$lead_to_lease_message .= 'State: '.PHP_EOL;
	$lead_to_lease_message .= 'Zip: '.PHP_EOL;
	$lead_to_lease_message .= 'Home Phone: '.$_POST['Home_Phone'].PHP_EOL;
	$lead_to_lease_message .= 'Cell Phone: '.PHP_EOL;
	$lead_to_lease_message .= 'Work Phone: '.PHP_EOL;
	
	if(isset($_POST['Schedule_a_Tour']) && $_POST['Schedule_a_Tour']=="on"){
		
		$lead_to_lease_message .= 'Service: Apartment Tour'.PHP_EOL;
		
	}else{
		
		$lead_to_lease_message .= 'Service: '.PHP_EOL;
		
	}
	$lead_to_lease_message .= 'Date: '.PHP_EOL;
	$lead_to_lease_message .= 'Start Time: '.PHP_EOL;
	$lead_to_lease_message .= 'End Time: '.PHP_EOL;
	$lead_to_lease_message .= 'Email Address: '.$_POST['Email_Address'].PHP_EOL;
	$lead_to_lease_message .= 'Lead Channel: Property Website'.PHP_EOL;
	$lead_to_lease_message .= 'Lead Priority: 2'.PHP_EOL;
	$lead_to_lease_message .= 'Desired Move In: '.$move_in_date.PHP_EOL;
	$lead_to_lease_message .= 'Desired Lease Term: '.PHP_EOL;
	$lead_to_lease_message .= 'Desired Unit Type: '.PHP_EOL;
	$lead_to_lease_message .= 'Desired Bedrooms: '.$_POST['Desired_Floor_Plan'].PHP_EOL;
	$lead_to_lease_message .= 'Desired Bathrooms: '.PHP_EOL;
	$lead_to_lease_message .= 'Pets: '.PHP_EOL;
	$lead_to_lease_message .= 'Pet Types: '.PHP_EOL;
	$lead_to_lease_message .= 'Comments: '.$_POST['Comments'].PHP_EOL;
													
 	
	$to = 'CrestatLasColinasStation.LML@lead2lease.com'; $subject = '‐‐New Email Lead For ##Crest at Las Colinas Station##‐‐'; $message = $lead_to_lease_message; $headers2 = 'From: no-reply@las-colinas.0808project.com' . "\r\n" . 'Reply-To: no-reply@las-colinas.0808project.com'; wp_mail($to, $subject, $message, $headers2);
	
	
	//lascolinasleasing@lennar.com
	
	
	
	wp_mail($sendto, 'New Email Lead For Crest at Las Colinas', $message, $headers );
	
	$post = array(
		'post_title'	=> get_the_title($_POST['page_id']).': '.$_POST['Email_Address'],
		'post_content'	=> $message,
		'post_status'	=> 'private',
		'post_type'	=> 'contact_form'  // Use a custom post type if you want to
	);
	
	$post_id = wp_insert_post($post);
 	
	echo get_field('thank_you_message',$_POST['page_id']);
	
} else {

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div class="extra-padding">

    <div class="inner">
        <article id="post-<?php the_ID(); ?>" class="contact-page">
            <h1 class="entry-title"><?php //the_title(); ?></h1>
            <div class="entry-content">
                <?php the_content(); ?>
                <div class="form-container">
                    <div id="main-form" class="left">
                        <?php 
							$link =  $_SERVER['REQUEST_URI'];
							$link_explode = explode("?",$link);
							?>


                        <form id="form_contact" action="<?php echo $link_explode[0] ?>" method="post">
                            <div class="tour-left">

                                <input type="text" name="First_Name" id="First_Name" placeholder="First Name*" class="two-col req" />
                                <input type="text" name="Last_Name" id="Last_Name" placeholder="Last Name*" class="two-col req" />
                                <input type="text" name="Home_Phone" id="phone" placeholder="Phone*" class="two-col req" />
                                <input type="text" name="Email_Address" id="email" placeholder="Email*" class="two-col req-email" />
                                <input type="text" id="appointment_date_new" name="Appointment_Date_New" placeholder="Move-in Date" class="two-col datepicker" />
                                <select id="desired_floor_plan" class="two-col" name="Desired_Floor_Plan">
                                        
											<option value="No Plan Selected" id="contact-enquirytype-1">Desired Floor Plan</option>
											<option value="0" class="contact-enquirytype-2">Efficiency</option>
                                            <option value="1" class="contact-enquirytype-2">1</option>
                                            <option value="2" class="contact-enquirytype-2">2</option>                                            
										</select>
                                <!-- <input type="text" name="subject" id="subject" placeholder="Subject*" class="req" /> -->
                                <!-- removed since Lead to Lease requires specific subject -->
                                <textarea name="Comments" id="form_message" placeholder="Message*" class="req"></textarea>
                                <div class="clearfix"></div>

                                <input type="hidden" name="Lead_Channel" value="Property Website" />
                                <input type="hidden" name="page_id" id="page_id" value="<?php the_ID(); ?>" />

                            </div>
                            <!-- 
									<div class="tour-right">

										<div class="time-options">
										
										    <?php $appointmenthours = get_field('days', 'option');  ?>

										</div>
									
                                        
                                        <?php $tabs_section = get_field('floorplan_type', $bloginfo['floorplans_page']);
										
										
										 ?>
									</div>
 -->
                            <div class="clearfix"></div>


                            <!-- 
	<input class="fake-checkbox" type="checkbox" name="Schedule_a_Tour" />
									<label class="text-area-label"> I'd like to schedule a tour</label>
 -->


                            <div>
                                <input id="checkbox-1" class="checkbox-custom" name="Schedule_a_Tour" type="checkbox">
                                <label for="checkbox-1" class="checkbox-custom-label">I'd like to schedule a tour</label>
                            </div>



                            <div class="clearfix"></div>

                            <!--  <span class="fake-checkbox"><span></span></span> -->
                            <!--input type="checkbox" name="schedule_a_tour" id="scedule_a_tour" /-->
                            <!-- I'd like to schedule a tour -->

                            <!-- <div class="fine-print"><p>*tours will begin on 9/30.</p></div> -->

                            <input type="input" name="fill_this_out" id="fill_this_out" />
                            <!--input type="submit" value="Submit" /-->
                            <a class="orange-btn submit" href="/contact/">Submit</a>
                        </form>
                        <div id="thank-you"></div>
                        <div class="clearfix"></div>

                    </div>
                    <div class="right">
                        <div id="map_canvas">
                            <iframe src="<?php echo get_field('google_map_embed_link','option'); ?>" width="100%" height="250" frameborder="0" style="border:0"></iframe>
                        </div>
                        <a href="<?php echo get_field('google_map_link','option'); ?>" target="_blank">
									View in Google Maps
								</a>

                        <?php echo $bloginfo['name'] ; ?><br />
                        <a style="text-decoration: none; color: inherit;" href="https://goo.gl/maps/EhuLoHav9NG2" target="_blank"><?php echo get_field('street_address','option'); ?></a>
                        <a style="text-decoration: none; color: inherit;" href="https://goo.gl/maps/EhuLoHav9NG2" target="_blank"><?php echo get_field('city_st_zip','option'); ?></a>
                        <a style="text-decoration: none; color: inherit;" href="tel:8559120506" target="_blank"><?php echo get_field('phone_#','option'); ?></a><br />
                    </div>

                    <div class="fine-print">

                        <p>By submitting this form, you agree that we may communicate with you from time to time about what's happening at our community. We'll never sell your information. Contact leasing office for more information.</p>
                    </div>
                </div>
                <!-- form container -->

            </div>
            <!-- .entry-content -->
        </article>
        <!-- #post-## -->

    </div>


    <div class="clearfix"></div>
</div>

<?php endwhile; ?>
<?php get_footer();

}?>
