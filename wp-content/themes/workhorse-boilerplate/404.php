<?php get_header(); ?>
			<article id="post-0" class="post error404 not-found" role="main">
				<div class="inner clearfix">
					<h1 class="page-title" >Page Not Found</h1>
					<p>We're sorry, that page cannot be found.<br/>Perhaps you can begin with, <a href="http://crestatglencoe.com/home/">homepage</a>.</p>
				</div>
			</article>
<?php get_footer(); ?>
