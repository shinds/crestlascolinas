<?php
/**
 * Template Name: Apartment Listings
 */
get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

 
<div class="inner">

  <?php

    $UserName = 'sthreshold1';
    $Password = 'Thresh0ld!';
    $SiteID = '3467908';
    $PmcID = '3467726';


    if(get_field('floor1'))
    {
      $f1 = array();    
      $count = 0;
      while(has_sub_field('floor1'))
      {
          $id = get_sub_field('flat_id');
          $xml = '<?xml version="1.0" encoding="utf-8"?>'.
                  '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
                    '<soap:Header>'.
                      '<UserAuthInfo xmlns="http://realpage.com/webservices">'.
                        '<UserName>'.$UserName.'</UserName>'.
                        '<Password>'.$Password.'</Password>'.
                        '<SiteID>'.$SiteID.'</SiteID>'.
                        '<PmcID>'.$PmcID.'</PmcID>'.
                        '<InternalUser>string</InternalUser>'.
                      '</UserAuthInfo>'.
                    '</soap:Header>'.
                    '<soap:Body>'.
                      '<List xmlns="http://realpage.com/webservices">'.
                        '<listCriteria>'.
                          '<ListCriterion>'.
                            '<Name>UnitNumber</Name>'.
                            '<SingleValue>'.$id.'</SingleValue>'.
                          '</ListCriterion>'. 
                        '</listCriteria>'.
                      '</List>'.
                    '</soap:Body>'.
                  '</soap:Envelope>';

          $url = "http://onesite.realpage.com/WebServices/CrossFire/AvailabilityAndPricing/Unit.asmx?wsdl";

          $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, $url);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

          $headers =  array();
                array_push($headers, "Host: onesite.realpage.com");
                array_push($headers, "Content-Type: text/xml; charset=utf-8");
                array_push($headers, "Accept: text/xml");
                array_push($headers, "SOAPAction: http://realpage.com/webservices/List");
                if($xml != null) 
                {
                    curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
                    array_push($headers, "Content-Length: " . strlen($xml));
                }
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
          $response = curl_exec($ch);
          $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
          curl_close($ch);
          $doc = new DOMDocument('1.0', 'utf-8');
          $doc->loadXML( $response );
          
          $f1[$count]['id'] = get_sub_field('flat_id');
          $f1[$count]['thumb'] = get_sub_field('plan_thumbnail');
          $f1[$count]['large'] = get_sub_field('plan_large');
          $f1[$count]['pdf'] = get_sub_field('print_pdf');
          $f1[$count]['sitemap'] = get_sub_field('sitemap_image');
          $f1[$count]['sitemap-desc'] = get_sub_field('sitemap_description');
          $f1[$count]['bldngid'] = $doc->getElementsByTagName("BuildingNumber")->item(0)->nodeValue;
          $f1[$count]['fltid'] = $doc->getElementsByTagName("UnitNumber")->item(0)->nodeValue;
          $f1[$count]['bed'] = $doc->getElementsByTagName("Bedrooms")->item(0)->nodeValue;
          $f1[$count]['bath'] = $doc->getElementsByTagName("Bathrooms")->item(0)->nodeValue;
          $f1[$count]['area'] = $doc->getElementsByTagName("RentSqFtCount")->item(0)->nodeValue;
          $f1[$count]['range'] = $doc->getElementsByTagName("FloorPlanMarketRent")->item(0)->nodeValue;
 
        $count++;
      }
    }
    
    if(get_field('floor2'))
    {
      $f2 = array();    
      $count = 0;
      while(has_sub_field('floor2'))
      {
          $id = get_sub_field('flat_id');
          $xml = '<?xml version="1.0" encoding="utf-8"?>'.
                  '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
                    '<soap:Header>'.
                      '<UserAuthInfo xmlns="http://realpage.com/webservices">'.
                        '<UserName>'.$UserName.'</UserName>'.
                        '<Password>'.$Password.'</Password>'.
                        '<SiteID>'.$SiteID.'</SiteID>'.
                        '<PmcID>'.$PmcID.'</PmcID>'.
                        '<InternalUser>string</InternalUser>'.
                      '</UserAuthInfo>'.
                    '</soap:Header>'.
                    '<soap:Body>'.
                      '<List xmlns="http://realpage.com/webservices">'.
                        '<listCriteria>'.
                          '<ListCriterion>'.
                            '<Name>UnitNumber</Name>'.
                            '<SingleValue>'.$id.'</SingleValue>'.
                          '</ListCriterion>'. 
                        '</listCriteria>'.
                      '</List>'.
                    '</soap:Body>'.
                  '</soap:Envelope>';

          $url = "http://onesite.realpage.com/WebServices/CrossFire/AvailabilityAndPricing/Unit.asmx?wsdl";

          $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, $url);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

          $headers =  array();
                array_push($headers, "Host: onesite.realpage.com");
                array_push($headers, "Content-Type: text/xml; charset=utf-8");
                array_push($headers, "Accept: text/xml");
                array_push($headers, "SOAPAction: http://realpage.com/webservices/List");
                if($xml != null) 
                {
                    curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
                    array_push($headers, "Content-Length: " . strlen($xml));
                }
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
          $response = curl_exec($ch);
          $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
          curl_close($ch);
          $doc = new DOMDocument('1.0', 'utf-8');
          $doc->loadXML( $response );
          
          $f2[$count]['id'] = get_sub_field('flat_id');
          $f2[$count]['thumb'] = get_sub_field('plan_thumbnail');
          $f2[$count]['large'] = get_sub_field('plan_large');
          $f2[$count]['pdf'] = get_sub_field('print_pdf');
          $f2[$count]['sitemap'] = get_sub_field('sitemap_image');
          $f2[$count]['sitemap-desc'] = get_sub_field('sitemap_description');
          $f2[$count]['bldngid'] = $doc->getElementsByTagName("BuildingNumber")->item(0)->nodeValue;
          $f2[$count]['fltid'] = $doc->getElementsByTagName("UnitNumber")->item(0)->nodeValue;
          $f2[$count]['bed'] = $doc->getElementsByTagName("Bedrooms")->item(0)->nodeValue;
          $f2[$count]['bath'] = $doc->getElementsByTagName("Bathrooms")->item(0)->nodeValue;
          $f2[$count]['area'] = $doc->getElementsByTagName("RentSqFtCount")->item(0)->nodeValue;
          $f2[$count]['range'] = $doc->getElementsByTagName("FloorPlanMarketRent")->item(0)->nodeValue;
 
        $count++;
      }
    } 
  
    if(get_field('floor3'))
    {
      $f3 = array();    
      $count = 0;
      while(has_sub_field('floor3'))
      {
          $id = get_sub_field('flat_id');
          $xml = '<?xml version="1.0" encoding="utf-8"?>'.
                  '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
                    '<soap:Header>'.
                      '<UserAuthInfo xmlns="http://realpage.com/webservices">'.
                        '<UserName>'.$UserName.'</UserName>'.
                        '<Password>'.$Password.'</Password>'.
                        '<SiteID>'.$SiteID.'</SiteID>'.
                        '<PmcID>'.$PmcID.'</PmcID>'.
                        '<InternalUser>string</InternalUser>'.
                      '</UserAuthInfo>'.
                    '</soap:Header>'.
                    '<soap:Body>'.
                      '<List xmlns="http://realpage.com/webservices">'.
                        '<listCriteria>'.
                          '<ListCriterion>'.
                            '<Name>UnitNumber</Name>'.
                            '<SingleValue>'.$id.'</SingleValue>'.
                          '</ListCriterion>'. 
                        '</listCriteria>'.
                      '</List>'.
                    '</soap:Body>'.
                  '</soap:Envelope>';

          $url = "http://onesite.realpage.com/WebServices/CrossFire/AvailabilityAndPricing/Unit.asmx?wsdl";

          $ch = curl_init();
              curl_setopt($ch, CURLOPT_URL, $url);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
              curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

          $headers =  array();
                array_push($headers, "Host: onesite.realpage.com");
                array_push($headers, "Content-Type: text/xml; charset=utf-8");
                array_push($headers, "Accept: text/xml");
                array_push($headers, "SOAPAction: http://realpage.com/webservices/List");
                if($xml != null) 
                {
                    curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
                    array_push($headers, "Content-Length: " . strlen($xml));
                }
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
          $response = curl_exec($ch);
          $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
          curl_close($ch);
          $doc = new DOMDocument('1.0', 'utf-8');
          $doc->loadXML( $response );
          
          $f3[$count]['id'] = get_sub_field('flat_id');
          $f3[$count]['thumb'] = get_sub_field('plan_thumbnail');
          $f3[$count]['large'] = get_sub_field('plan_large');
          $f3[$count]['pdf'] = get_sub_field('print_pdf');
          $f3[$count]['sitemap'] = get_sub_field('sitemap_image');
          $f3[$count]['sitemap-desc'] = get_sub_field('sitemap_description');
          $f3[$count]['bldngid'] = $doc->getElementsByTagName("BuildingNumber")->item(0)->nodeValue;
          $f3[$count]['fltid'] = $doc->getElementsByTagName("UnitNumber")->item(0)->nodeValue;
          $f3[$count]['bed'] = $doc->getElementsByTagName("Bedrooms")->item(0)->nodeValue;
          $f3[$count]['bath'] = $doc->getElementsByTagName("Bathrooms")->item(0)->nodeValue;
          $f3[$count]['area'] = $doc->getElementsByTagName("RentSqFtCount")->item(0)->nodeValue;
          $f3[$count]['range'] = $doc->getElementsByTagName("FloorPlanMarketRent")->item(0)->nodeValue;
 
        $count++;
      }
    } 
   
  
  ?>    

  
  <?php 
    //echo "<pre>"; print_r($f1); echo "</pre>";
    //echo "<pre>"; print_r($f2); echo "</pre>";
    //echo "<pre>"; print_r($f3); echo "</pre>";
  ?>


  <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx Data Binded In Arrays xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-->





  <div class="aptlisting-sec clearfix">
    <div class="aptlisting-col aptlisting-col-1">
      <div class="floor-title"><h2>FLOOR 1</h2></div>
      <?php
        $size = sizeof($f1);
        for($i=0;$i<$size;$i++)
        {
          ?>
            <div class="apt-list clearfix">
              <div class="apt-image">
                <a href="<?php bloginfo('template_directory'); ?>/_images/apt-img-large.png" rel="shadowbox">
                  <img src="<?php bloginfo('template_directory'); ?>/_images/apt-img2.jpg" alt="">
                  <span>Click to Enlarge</span>
                </a>
              </div>
              <div class="apt-detail">
                <span class="apt-name"><?php echo $f1[$i]['bldngid']; ?></span>
                <span class="apt-number">Apt. #<?php echo $f1[$i]['fltid']; ?></span>
                <span class="apt-room"><?php echo $f1[$i]['bed']; ?> beds, <?php echo $f1[$i]['bath']; ?> baths</span>
                <span class="apt-sqft"><?php echo $f1[$i]['area']; ?> sqft</span>
                <span class="apt-rate">Starting at $<?php echo substr($f1[$i]['range'], 0, -5); ?></span>
                <span class="apt-links"><a href="#">Pre-lease now</a>  |  <a class="apt-popup-btn" id="floor1-sitemap<?php echo $i?>" href="#">Site map</a></span>
              </div>
               
              <div class="floor-popup-sec floor1-sitemap<?php echo $i?>">
                <div class="close"></div>
                  <div class="floor-popup-content">
                    <div class="floor-opup-content-sec clearfix">
                      <div class="floor-popup-left">
                        <h3>APT. <?php echo $f1[$i]['fltid']; ?></h3>
                        <ul>
                          <li><?php echo $f1[$i]['bed']; ?> bed<?php echo ($f1[$i]['bed']>1) ? "s" : ""; ?>, <?php echo $f1[$i]['bath']; ?> bath<?php echo ($f1[$i]['bath']>1) ? "s" : ""; ?></li>
                          <li>Hardwood Floors, W/D</li>
                          <li><?php echo $f1[$i]['area']; ?> sq ft</li>
                        </ul>
                        <p><?php echo $f1[$i]['sitemap-desc']; ?></p>
                      </div>
                      <div class="floor-popup-right">
                        <img src="<?php bloginfo('template_directory'); ?>/_images/sitemap.jpg" alt="">
                        <div class="availability-link"><a href="#">Click here to view availability</a></div>
                      </div>
                    </div>
                  </div>
                </div>
             
            </div>
          <?php
        }
      ?>
    </div>
    <div class="aptlisting-col aptlisting-col-2">
      <div class="floor-title"><h2>FLOOR 2</h2></div>
      <div class="apt-list clearfix">
        <div class="apt-image">
          <a href="<?php bloginfo('template_directory'); ?>/_images/apt-img-large.png" rel="shadowbox">
            <img src="<?php bloginfo('template_directory'); ?>/_images/apt-img2.jpg" alt="">
            <span>Click to Enlarge</span>
          </a>
        </div>
        <div class="apt-detail">
          <span class="apt-name">Building 2</span>
          <span class="apt-number">Apt. #237</span>
          <span class="apt-room">2 beds, 2 baths</span>
          <span class="apt-sqft">1,200 sqft</span>
          <span class="apt-rate">Starting at $1,600</span>
          <span class="apt-links"><a href="#">Pre-lease now</a>  |  <a class="apt-popup-btn" id="floor2-sitemap1" href="#">Site map</a></span>
        </div>
        <!-- popup start -->
        <div class="floor-popup-sec floor2-sitemap1">
                <div class="close"></div>
                <div class="floor-popup-content">
                    <div class="floor-opup-content-sec clearfix">
                        <div class="floor-popup-left">
                            <h3>APT. 201</h3>
                            <ul>
                                <li>2 bed, 2 bath</li>
                                <li>Hardwood Floors, W/D</li>
                                <li>950 sq ft</li>
                            </ul>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin efficitur, diam congue condimentum laoreet, ante tellus ultrices tellus.</p>
                        </div>
                        <div class="floor-popup-right">
                          <img src="<?php bloginfo('template_directory'); ?>/_images/sitemap.jpg" alt="">
                          <div class="availability-link"><a href="#">Click here to view availability</a></div>
                      </div>
                    </div>
                    
                </div>
                </div>
        <!-- popup end -->
      </div>
      <div class="apt-list clearfix">
        <div class="apt-image">
          <a href="<?php bloginfo('template_directory'); ?>/_images/apt-img-large.png" rel="shadowbox">
            <img src="<?php bloginfo('template_directory'); ?>/_images/apt-img2.jpg" alt="">
            <span>Click to Enlarge</span>
          </a>
        </div>
        <div class="apt-detail">
          <span class="apt-name">Building 2</span>
          <span class="apt-number">Apt. #237</span>
          <span class="apt-room">2 beds, 2 baths</span>
          <span class="apt-sqft">1,200 sqft</span>
          <span class="apt-rate">Starting at $1,600</span>
          <span class="apt-links"><a href="#">Pre-lease now</a>  |  <a class="apt-popup-btn" id="floor2-sitemap2" href="#">Site map</a></span>
        </div>
        <!-- popup start -->
        <div class="floor-popup-sec floor2-sitemap2">
                <div class="close"></div>
                <div class="floor-popup-content">
                    <div class="floor-opup-content-sec clearfix">
                        <div class="floor-popup-left">
                            <h3>APT. 202</h3>
                            <ul>
                                <li>2 bed, 2 bath</li>
                                <li>Hardwood Floors, W/D</li>
                                <li>950 sq ft</li>
                            </ul>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin efficitur, diam congue condimentum laoreet, ante tellus ultrices tellus.</p>
                        </div>
                        <div class="floor-popup-right">
                          <img src="<?php bloginfo('template_directory'); ?>/_images/sitemap.jpg" alt="">
                          <div class="availability-link"><a href="#">Click here to view availability</a></div>
                      </div>
                    </div>
                    
                </div>
                </div>
        <!-- popup end -->
      </div>
      <div class="apt-list clearfix">
        <div class="apt-image">
          <a href="<?php bloginfo('template_directory'); ?>/_images/apt-img-large.png" rel="shadowbox">
            <img src="<?php bloginfo('template_directory'); ?>/_images/apt-img2.jpg" alt="">
            <span>Click to Enlarge</span>
          </a>
        </div>
        <div class="apt-detail">
          <span class="apt-name">Building 2</span>
          <span class="apt-number">Apt. #237</span>
          <span class="apt-room">2 beds, 2 baths</span>
          <span class="apt-sqft">1,200 sqft</span>
          <span class="apt-rate">Starting at $1,600</span>
          <span class="apt-links"><a href="#">Pre-lease now</a>  |  <a class="apt-popup-btn" id="floor2-sitemap3" href="#">Site map</a></span>
        </div>
        <!-- popup start -->
        <div class="floor-popup-sec floor2-sitemap3">
                <div class="close"></div>
                <div class="floor-popup-content">
                    <div class="floor-opup-content-sec clearfix">
                        <div class="floor-popup-left">
                            <h3>APT. 203</h3>
                            <ul>
                                <li>2 bed, 2 bath</li>
                                <li>Hardwood Floors, W/D</li>
                                <li>950 sq ft</li>
                            </ul>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin efficitur, diam congue condimentum laoreet, ante tellus ultrices tellus.</p>
                        </div>
                        <div class="floor-popup-right">
                          <img src="<?php bloginfo('template_directory'); ?>/_images/sitemap.jpg" alt="">
                          <div class="availability-link"><a href="#">Click here to view availability</a></div>
                      </div>
                    </div>
                    
                </div>
                </div>
        <!-- popup end -->
      </div>
    </div>
    <div class="aptlisting-col aptlisting-col-3">
      <div class="floor-title"><h2>FLOOR 3</h2></div>
      <div class="apt-list clearfix">
        <div class="apt-image">
          <a href="<?php bloginfo('template_directory'); ?>/_images/apt-img-large.png" rel="shadowbox">
            <img src="<?php bloginfo('template_directory'); ?>/_images/apt-img1.jpg" alt="">
            <span>Click to Enlarge</span>
          </a>
        </div>
        <div class="apt-detail">
          <span class="apt-name">Building 2</span>
          <span class="apt-number">Apt. #237</span>
          <span class="apt-room">2 beds, 2 baths</span>
          <span class="apt-sqft">1,200 sqft</span>
          <span class="apt-rate">Starting at $1,600</span>
          <span class="apt-links"><a href="#">Pre-lease now</a>  |  <a class="apt-popup-btn" id="floor3-sitemap1" href="#">Site map</a></span>
        </div>
        <!-- popup start -->
        <div class="floor-popup-sec floor3-sitemap1">
                <div class="close"></div>
                <div class="floor-popup-content">
                    <div class="floor-opup-content-sec clearfix">
                        <div class="floor-popup-left">
                            <h3>APT. 301</h3>
                            <ul>
                                <li>2 bed, 2 bath</li>
                                <li>Hardwood Floors, W/D</li>
                                <li>950 sq ft</li>
                            </ul>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin efficitur, diam congue condimentum laoreet, ante tellus ultrices tellus.</p>
                        </div>
                        <div class="floor-popup-right">
                          <img src="<?php bloginfo('template_directory'); ?>/_images/sitemap.jpg" alt="">
                          <div class="availability-link"><a href="#">Click here to view availability</a></div>
                      </div>
                    </div>
                    
                </div>
                </div>
        <!-- popup end -->
      </div>
      <div class="apt-list clearfix">
        <div class="apt-image">
          <a href="<?php bloginfo('template_directory'); ?>/_images/apt-img-large.png" rel="shadowbox">
            <img src="<?php bloginfo('template_directory'); ?>/_images/apt-img1.jpg" alt="">
            <span>Click to Enlarge</span>
          </a>
        </div>
        <div class="apt-detail">
          <span class="apt-name">Building 2</span>
          <span class="apt-number">Apt. #237</span>
          <span class="apt-room">2 beds, 2 baths</span>
          <span class="apt-sqft">1,200 sqft</span>
          <span class="apt-rate">Starting at $1,600</span>
          <span class="apt-links"><a href="#">Pre-lease now</a>  |  <a class="apt-popup-btn" id="floor3-sitemap2" href="#">Site map</a></span>
        </div>
        <!-- popup start -->
        <div class="floor-popup-sec floor3-sitemap2">
                <div class="close"></div>
                <div class="floor-popup-content">
                    <div class="floor-opup-content-sec clearfix">
                        <div class="floor-popup-left">
                            <h3>APT. 302</h3>
                            <ul>
                                <li>2 bed, 2 bath</li>
                                <li>Hardwood Floors, W/D</li>
                                <li>950 sq ft</li>
                            </ul>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin efficitur, diam congue condimentum laoreet, ante tellus ultrices tellus.</p>
                        </div>
                        <div class="floor-popup-right">
                          <img src="<?php bloginfo('template_directory'); ?>/_images/sitemap.jpg" alt="">
                          <div class="availability-link"><a href="#">Click here to view availability</a></div>
                      </div>
                    </div>
                    
                </div>
                </div>
        <!-- popup end -->
      </div>
      <div class="apt-list clearfix">
        <div class="apt-image">
          <a href="<?php bloginfo('template_directory'); ?>/_images/apt-img-large.png" rel="shadowbox">
            <img src="<?php bloginfo('template_directory'); ?>/_images/apt-img1.jpg" alt="">
            <span>Click to Enlarge</span>
          </a>
        </div>
        <div class="apt-detail">
          <span class="apt-name">Building 2</span>
          <span class="apt-number">Apt. #237</span>
          <span class="apt-room">2 beds, 2 baths</span>
          <span class="apt-sqft">1,200 sqft</span>
          <span class="apt-rate">Starting at $1,600</span>
          <span class="apt-links"><a href="#">Pre-lease now</a>  |  <a class="apt-popup-btn" id="floor3-sitemap3" href="#">Site map</a></span>
        </div>
        <!-- popup start -->
        <div class="floor-popup-sec floor3-sitemap3">
                <div class="close"></div>
                <div class="floor-popup-content">
                    <div class="floor-opup-content-sec clearfix">
                        <div class="floor-popup-left">
                            <h3>APT. 303</h3>
                            <ul>
                                <li>2 bed, 2 bath</li>
                                <li>Hardwood Floors, W/D</li>
                                <li>950 sq ft</li>
                            </ul>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin efficitur, diam congue condimentum laoreet, ante tellus ultrices tellus.</p>
                        </div>
                        <div class="floor-popup-right">
                          <img src="<?php bloginfo('template_directory'); ?>/_images/sitemap.jpg" alt="">
                          <div class="availability-link"><a href="#">Click here to view availability</a></div>
                      </div>
                    </div>
                    
                </div>
                </div>
        <!-- popup end -->
      </div>
    </div>
    
  </div>
  <!-- popup bg and loader -->
  <div class="loader"></div>
  <div id="backgroundPopup"></div>
  <!-- popup bg and loader -->
</div> 
<?php endwhile; ?>                
<?php get_footer(); ?>	