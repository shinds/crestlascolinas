<?php
/**
 * Template Name:Park Suite Selector
 */ 
get_header(); ?>
	
  	<style>
		#ss-map {position: relative;}
		.map-sec {margin: 0 auto; position: relative; width: 740px; background-color: #efefef;}
		 .suite {background: rgba(255, 255, 255, 0.5); cursor: pointer; /*opacity: 0;*/ position: absolute;}
		#fullhover .building1 {background: url("/wp-content/themes/workhorse-boilerplate/images/map-5.png") no-repeat scroll left top rgba(0, 0, 0, 0); height: 279px; width: 143px; left: 50.5%;top: 24.2%;}
		#fullhover .building2 {background: url("/wp-content/themes/workhorse-boilerplate/images/map-6.png") no-repeat scroll left top rgba(0, 0, 0, 0); height: 131px; width: 185px; left: 69.6%;top: 56.1%;}
		#fullhover .building3 {background: url("/wp-content/themes/workhorse-boilerplate/images/map-7.png") no-repeat scroll left top rgba(0, 0, 0, 0); height: 116px; width: 41px; left: 87.8%;top: 33.6%;}
		#fullhover .building4 {height: 6%; left: 74.4%; top: 24.8%; width: 18%;}
		#fullhover .building5 {background: url("/wp-content/themes/workhorse-boilerplate/images/map-1.png") no-repeat scroll left top rgba(0, 0, 0, 0); height: 158px; width: 85px; left: 5.7%;top: 42.4%;}
		#fullhover .building6 {height: 6%; left: 14.7%; top: 62.9%; width: 18.5%;}
		#fullhover .building7 {background: url("/wp-content/themes/workhorse-boilerplate/images/map-3.png") no-repeat scroll left top rgba(0, 0, 0, 0); height: 98px; width: 40px; left: 35.6%;top: 52.9%;}
		#fullhover .building8 {height: 6%; left: 21.3%; top: 43.5%; width: 19.3%;}
	</style>

	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<div class="inner">
        	<div class="back-suite-button"><a href="/apartment-listings/">VIEW ALL</a></div>
		    <div id="suite-selector" class="clearfix">
		        <div class="ss-left equalheight">
		            <div id="suite-selector-menu">
		                <ul>
		                    <li class="ss-main bed"><span class="title">0+beds</span> <span class="fa down">&#xf0d7;</span><span class="fa up">&#xf0d8;</span>
		                        <ul class="sub-bed suite-filter" >
		                            <li><input type="checkbox" id="bed1" name="bed" value="1" /><label for="bed1"><span>1</span></label></li>
		                            <li><input type="checkbox" id="bed2" name="bed" value="2" /><label for="bed2"><span>2</span></label></li>
		                        </ul>
		                    </li>
		                    <li class="ss-main bath"><span class="title">0+ baths</span> <span class="fa down">&#xf0d7;</span><span class="fa up">&#xf0d8;</span>
		                        <ul class ="sub-bath suite-filter" >
		                            <li><input type="checkbox" id="bath1" name="bath" value="1" /><label for="bath1"><span>1</span></label></li>
		                            <li><input type="checkbox" id="bath2" name="bath" value="2" /><label for="bath2"><span>2</span></label></li>
		                        </ul>
		                    </li>
		                    <li class="ss-main view"><span class="title">view</span> <span class="fa down">&#xf0d7;</span><span class="fa up">&#xf0d8;</span>
		                        <ul class ="sub-view">
		                            <li><input type="checkbox" name="view" value="pool" id="c1" /><label for="c1"><span>pool</span></label></li>
		                            <li><input type="checkbox" name="view" value="garden" id="c2" /><label for="c2"><span>garden</span></label></li>
		                            <li><input type="checkbox" name="view" value="street" id="c3" /><label for="c3"><span>street</span></label></li>
		                        </ul>
		                    </li>
		                    <li class="ss-main price">price <span class="fa down">&#xf0d7;</span><span class="fa up">&#xf0d8;</span>
		                        <ul class="sub-price">
		                            <!--<li><input type="checkbox" id="c4" /><label for="c4"><span>$500-$700</span></label></li>
		                            <li><input type="checkbox" id="c5" /><label for="c5"><span>$701-$1000</span></label></li>
		                            <li><input type="checkbox" id="c6" /><label for="c6"><span>$1001-$1500</span></label></li>-->
		                        </ul>
		                    </li>
		                </ul>
		            </div>


		            <div id="ss-map" class="main-area">
		              	<div class="map-sec">
					      	<!-- Full map and crunches of map -->
					      	<img class='full-map' src="<?php bloginfo('template_url'); ?>/images/suite-selector_03.gif" alt="main map bg">
							<div id='building1' class="single-block" style="display:none;" >
								<img class="crunch" src="<?php bloginfo('template_url'); ?>/park-map/images/building-1.gif" alt="main map bg">
								 	<div  class="suite block1 building1 b-1101 b-1201 b-1301"></div>
						            <div  class="suite block1 building2 b-1202 b-1302"></div>
						            <div  class="suite block1 building3 b-1103 b-1203 b-1302"></div>
						            <div  class="suite block1 building4 b-1204 b-1304"></div>
						            <div  class="suite block1 building5 b-1105 b-1205 b-1305"></div>
						            <div  class="suite block1 building6 b-1206 b-1306"></div>
						            <div  class="suite block1 building7 b-1307"></div>
						            <div  class="suite block1 building8 b-1208 b-1308"></div>
						            <div  class="suite block1 building9 b-1309"></div>
						            <div  class="suite block1 building10 b-1210 b-1310"></div>
						            <div  class="suite block1 building11 b-1111 b-1211 b-1311"></div>
						            <div  class="suite block1 building12 b-1212 b-1312"></div>
						            <div  class="suite block1 building13 b-1113 b-1213 b-1313"></div>
						            <div  class="suite block1 building14 b-1214 b-1314"></div>
						            <div  class="suite block1 building15 b-1215 b-1315"></div>
						            <div  class="suite block1 building16 b-1216 b-1316"></div>
						            <div  class="suite block1 building17 b-1117 b-1217 b-1317"></div>
						            <div  class="suite block1 building18 b-1218 b-1318"></div>
						            <div  class="suite block1 building19 b-1119 b-1219 b-1319"></div>
						            <div  class="suite block1 building20 b-1120 b-1220 b-1320"></div>
						            <div  class="suite block1 building21 b-1121 b-1221 b-1321"></div>
						            <div  class="suite block1 building22 b-1122 b-1222 b-1322"></div>
						            <div  class="suite block1 building23 b-1123 b-1223 b-1323"></div>
						            <div  class="suite block1 building24 b-1224 b-1324"></div>
						            <div  class="suite block1 building25 b-1125 b-1225 b-1325"></div>
						            <div  class="suite block1 building26 b-1226 b-1326"></div>
						            <div  class="suite block1 building27 b-1127 b-1227 b-1327"></div>
						            <div  class="suite block1 building28 b-1228 b-1328"></div>
						            <div  class="suite block1 building29 b-1229 b-1329"></div>
						            <div  class="suite block1 building30 b-1230 b-1330"></div>
						            <div  class="suite block1 building31 b-1231 b-1331"></div>
						            <div  class="suite block1 building32 b-1232 b-1332"></div>
						            <div  class="suite block1 building33 b-1233 b-1333"></div>
						            <div  class="suite block1 building34 b-1234 b-1334"></div>
						            <div  class="suite block1 building35 b-1235 b-1335"></div>
						            <div  class="suite block1 building36 b-1236 b-1336"></div>
						            <div  class="suite block1 building37 b-1237 b-1337"></div>
						            <div  class="suite block1 building38 b-1238 b-1338"></div>
						            <div  class="suite block1 building39 b-1139 b-1239 b-1339"></div>
						            <div  class="suite block1 building40 b-1240 b-1340"></div>
						            <div  class="suite block1 building41 b-1141 b-1241 b-1341"></div>
						            <div  class="suite block1 building42 b-1142 b-1242 b-1342"></div>
						            <div  class="suite block1 building43 b-1143 b-1243 b-1343"></div>
						            <div  class="suite block1 building44 b-1144 b-1244 b-1344"></div>
						            <div  class="suite block1 building46 b-1146 b-1246 b-1346"></div>
						            <div  class="suite block1 building48 b-1148 b-1248 b-1348"></div>
						            <div  class="suite block1 building50 b-1150 b-1250 b-1350"></div>
							</div>
							<div id='building2' class="single-block" style="display:none;" >
								<img class="crunch" src="<?php bloginfo('template_url'); ?>/park-map/images/building-2.gif" alt="main map bg">
						            <div class="block2 suite building51 b-2101 b-2201 b-2301"></div>
						            <div class="block2 suite building52 b-2202 b-2302"></div>
						            <div class="block2 suite building53 b-2103 b-2203 b-2303"></div>
						            <div class="block2 suite building54 b-2204 b-2304"></div>
						            <div class="block2 suite building55 b-2105 b-2205 b-2305"></div>
						            <div class="block2 suite building56 b-2206 b-2306"></div>
						            <div class="block2 suite building57 b-2107 b-2207 b-2307"></div>
						            <div class="block2 suite building58 b-2208 b-2308"></div>
						            <div class="block2 suite building59 b-2109 b-2209 b-2309"></div>
						            <div class="block2 suite building60 b-2110 b-2210 b-2310"></div>
						            <div class="block2 suite building61 b-2111 b-2211 b-2311"></div>
						            <div class="block2 suite suite-small building62 b-2212 b-2312"></div>
						            <div class="block2 suite building63 b-2113 b-2213 b-2313"></div>
						            <div class="block2 suite suite-small building64 b-2214 b-2314"></div>
						            <div class="block2 suite suite-large building65 b-2115 b-2215 b-2315"></div>
						            <div class="block2 suite suite-small building66 b-2216 b-2316"></div>
						            <div class="block2 suite suite-large building67 b-2117 b-2217 b-2317"></div>
						            <div class="block2 suite suite-small building68 b-2218 b-2318"></div>
						            <div class="block2 suite suite-large building69 b-2119 b-2219 b-2319"></div>
						            <div class="block2 suite suite-small building70 b-2220 b-2320"></div>
						            <div class="block2 suite suite-large building71 b-2121 b-2221 b-2321"></div>
						            <div class="block2 suite suite-small building72 b-2222 b-2322"></div>
						            <div class="block2 suite suite-large building73 b-2123 b-2223 b-2323"></div>
						            <div class="block2 suite suite-large building74 b-2125 b-2225 b-2325"></div>
						            <div class="block2 suite suite-large building75 b-2127 b-2227 b-2327"></div>
						            <div class="block2 suite suite-large building76 b-2129 b-2229 b-2329"></div>
							</div>
							<div id='building3' class="single-block"  style="display:none;" >
								<img class="crunch" src="<?php bloginfo('template_url'); ?>/park-map/images/building-3.gif" alt="main map bg" >
									<div class="block3 suite building77 b-3101 b-3201 b-3301"></div>
						            <div class="block3 suite building78 b-3202 b-3302"></div>
						            <div class="block3 suite building79 b-3103 b-3203 b-3303"></div>
						            <div class="block3 suite building80 b-3204 b-3304"></div>
						            <div class="block3 suite building81 b-3105 b-3205 b-3305"></div>
						            <div class="block3 suite building82 b-3206 b-3306"></div>
						            <div class="block3 suite building83 b-3107 b-3207 b-3307"></div>
						            <div class="block3 suite building84 b-3208 b-3308"></div>
						            <div class="block3 suite building85 b-3109 b-3209 b-3309"></div>
						            <div class="block3 suite building86 b-3210 b-3310"></div>
						            <div class="block3 suite building87 b-3111 b-3211 b-3311"></div>
						            <div class="block3 suite building88 b-3212 b-3312"></div>
							</div>
							<div id='building4' class="single-block" style="display:none;" >
								<img class="crunch" src="<?php bloginfo('template_url'); ?>/park-map/images/building-4.gif" alt="main map bg" >
									<div class="block4 suite building89 b-4101 b-4201 b-4301"></div>
						        	<div class="block4 suite building90 b-4202 b-4302"></div>
						        	<div class="block4 suite building91 b-4103 b-4203 b-4303"></div>
						        	<div class="block4 suite building92 b-4204 b-4304"></div>
						        	<div class="block4 suite building93 b-4105 b-4205 b-4305"></div>
						        	<div class="block4 suite building94 b-4206 b-4306"></div>
						        	<div class="block4 suite building95 b-4107 b-4207 b-4307"></div>
						        	<div class="block4 suite building96 b-4208 b-4308"></div>
						        	<div class="block4 suite building97 b-4109 b-4209 b-4309"></div>
						        	<div class="block4 suite building98 b-4110 b-4210 b-4310"></div>
						        	<div class="block4 suite building99 b-4111 b-4211 b-4311"></div>
						        	<div class="block4 suite building100 b-4112 b-4212 b-4312"></div>
						        	<div class="block4 suite building101 b-4113 b-4213 b-4313"></div>
						        	<div class="block4 suite building102 b-4115 b-4215 b-4315"></div>
							</div>
							<div id='building7' class="single-block" style="display:none;" >
								<img class="crunch" src="<?php bloginfo('template_url'); ?>/park-map/images/building-7.gif" alt="main map bg" >
									<div class="block5 suite building103 b-5101 b-5201 b-5301"></div>
							    	<div class="block5 suite building104 b-5202 b-5302"></div>
							    	<div class="block5 suite building105 b-5103 b-5203 b-5303"></div>
							    	<div class="block5 suite building106 b-5204 b-5304"></div>
							    	<div class="block5 suite building107 b-5105 b-5205 b-5305"></div>
							    	<div class="block5 suite building108 b-5206 b-5306"></div>
							    	<div class="block5 suite building109 b-5107 b-5207 b-5307"></div>
							    	<div class="block5 suite building110 b-5208 b-5308"></div>
							    	<div class="block5 suite building111 b-5109 b-5209 b-5309"></div>
							    	<div class="block5 suite building112 b-5210 b-5310"></div>
							    	<div class="block5 suite building113 b-5111 b-5211 b-5309"></div>
							</div>
							<div id='building6' class="single-block" style="display:none;" >
								<img class="crunch" src="<?php bloginfo('template_url'); ?>/park-map/images/building-6.gif" alt="main map bg" >
									<div class="block6 suite building114 b-6101 b-6201 b-6301"></div>
      								<div class="block6 suite building115 b-6202 b-6302"></div>
      								<div class="block6 suite building116 b-6103 b-6203 b-6303"></div>
      								<div class="block6 suite building117 b-6204 b-6304"></div>
      								<div class="block6 suite building118 b-6105 b-6205 b-6305"></div>
      								<div class="block6 suite building119 b-6206 b-6306"></div>
      								<div class="block6 suite building120 b-6107 b-6207 b-6307"></div>
      								<div class="block6 suite building121 b-6208 b-6308"></div>
      								<div class="block6 suite building122 b-6109 b-6209 b-6309"></div>
      								<div class="block6 suite building123 b-6210 b-6310"></div>
      								<div class="block6 suite building124 b-6111 b-6211 b-6311"></div>
      								<div class="block6 suite building125 b-6212 b-6312"></div>
							</div>
							<div id='building5' class="single-block" style="display:none;" >
								<img class="crunch" src="<?php bloginfo('template_url'); ?>/park-map/images/building-5.gif" alt="main map bg" >
									<div class="block7 suite suite-71 building126 b-7113 b-7213 b-7313"></div>
						        	<div class="block7 suite suite-71 building127 b-7111 b-7211 b-7311"></div>
						        	<div class="block7 suite suite-71 building128 b-7109 b-7209 b-7309"></div>
						        	<div class="block7 suite suite-71 building129 b-7107 b-7207 b-7307"></div>
						        	<div class="block7 suite suite-71 building130 b-7105 b-7205 b-7305"></div>
						        	<div class="block7 suite suite-71 building131 b-7103 b-7203 b-7303"></div>
						        	<div class="block7 suite suite-71 building132 b-7101 b-7201 b-7301"></div>
						        	<div class="block7 suite suite-72 building133 b-7115 b-7215 b-7315"></div>
						        	<div class="block7 suite suite-72 building134 b-7117 b-7217 b-7317"></div>
						        	<div class="block7 suite suite-72 building135 b-7119 b-7219 b-7319"></div>
						        	<div class="block7 suite suite-72 building136 b-7110 b-7210 b-7310"></div>
						        	<div class="block7 suite suite-72 building137 b-7114 b-7214 b-7314"></div>
						        	<div class="block7 suite suite-72 building138 b-7116 b-7216 b-7316"></div>
						        	<div class="block7 suite suite-71 building139 b-7112 b-7212 b-7312"></div>
						        	<div class="block7 suite suite-73 building140 b-7208 b-7308"></div>
						        	<div class="block7 suite suite-73 building141 b-7206 b-7306"></div>
						        	<div class="block7 suite suite-73 building142 b-7204 b-7304"></div>
						        	<div class="block7 suite suite-73 building143 b-7202 b-7302"></div>

							</div>
							<div id='building8' class="single-block" style="display:none;" >
								<img class="crunch" src="<?php bloginfo('template_url'); ?>/park-map/images/building-8.gif" alt="main map bg" >
									<div class="block8 suite building144 b-8111 b-8211 b-8311"></div>
						        	<div class="block8 suite building145 b-8109 b-8209 b-8309"></div>
						        	<div class="block8 suite building146 b-8107 b-8207 b-8307"></div>
						        	<div class="block8 suite building147 b-8105 b-8205 b-8305"></div>
						        	<div class="block8 suite building148 b-8103 b-8203 b-8303"></div>
						        	<div class="block8 suite building149 b-8101 b-8201 b-8301"></div>
						        	<div class="block8 suite building150 b-8212 b-8312"></div>
						        	<div class="block8 suite building151 b-8210 b-8310"></div>
						        	<div class="block8 suite building152 b-8208 b-8308"></div>
						        	<div class="block8 suite building153 b-8206 b-8306"></div>
						        	<div class="block8 suite building154 b-8204 b-8304"></div>
						        	<div class="block8 suite building155 b-8202 b-8302"></div>
							</div>

					      	<!-- hover states of full map -->
					      	 
					      	<div id="fullhover">
						      	<div class="suite building1"></div>  
						      	<div class="suite building2"></div>
						      	<div class="suite building3"></div>
					      		<div class="suite building4"></div>
					      		<div class="suite building5"></div>  
						      	<div class="suite building6"></div>
						      	<div class="suite building7"></div>
						      	<div class="suite building8"></div>
					      	</div>
					      	<div class="available-unit-ids">The apartments that are highlighted are available.</div>
					  	</div> 
					  	
					  	<div class="restore-map"><a>See All Buildings</a></div>
		            
		            <?php
		                if(get_field('parent'))
		                {
		                  $count = 1;
		                  while(has_sub_field('parent'))
		                  {
		                    ?>
		                        <div class="popup-building-type-<?php echo $count; ?> popup-sec popup-type-<?php echo $count; ?>">
		                            <div class="close"></div>
		                            <div class="popup-content">
		                                <div class="popup-content-sec clearfix">
		                                    <div class="popup-left">
		                                        <h3><?php echo get_sub_field('building_name'); ?></h3>
		                                        <?php echo get_sub_field('amenities'); ?>
		                                    </div>
		                                    <div class="popup-right">
		                                        <img src="<?php echo get_sub_field('building_image'); ?>" alt="">
		                                    </div>
		                                </div>
		                                <div class="popup-btn"><a class="orange-btn" href="<?php echo get_sub_field('availability_link'); ?>">AVAILABLE APARTMENTS</a></div>
		                            </div>
		                        </div>
		                        <?php
                            		$count++;
		                  }
		                }
		            ?>
		            
		           
			    <div class="loader"></div>
			   	<div id="backgroundPopup"></div>
		                
		            </div>
		        </div>
		        <div class="ss-right equalheight">
		            <div id="my-loader" style="display:none;" ><img class="loader-img" src="<?php bloginfo('template_url'); ?>/img/load.gif"/></div> 
		             
		            <div id="available-apartments-header"><span class="available-apartment">AVAILABLE APARTMENTS</span> <div class="unitMoveDate"><input type="text" id="datepicker" placeholder="Movein Date"></div> <!--a class="view-all" href="/apartment-listings/">VIEW ALL</a--></div>
		            <div class="content mCustomScrollbar appartment-list-sec">
		      			  
		              <ul class="appartment-list" id="list-apart">
		            </ul>
					 </div>
		        </div>
		    </div><!-- /suite-selector-->
		    <!--<p id='click'>CLICK</p>--><!-- CLICK -->
		</div> 
<?php endwhile; ?>                
    
    <?php
        $arr_view = array();
        $arr_view['garden']= get_field('garden_view');
        $arr_view['pool']= get_field('pool_view');
        $arr_view['street']= get_field('street_view');
    ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<script>
 var views = jQuery.parseJSON('<?php echo json_encode($arr_view);?>');
    $(document).ready(function(){
           
        // Binding Pricelist using PickList method of RealPage 
        $.ajax({
            method: "POST",
            url: "http://crestatparkcentral.com/wp-content/themes/workhorse-boilerplate/parkprice.php",
            beforeSend: function(){
              $("#my-loader").show();
            }
          }).done(function(priceback) {
              xmlDoc = $.parseXML( priceback ),
              $xml = $( xmlDoc ),
              $UnitObject = $xml.find( "PicklistItem" );
              $( "li.price ul.sub-price" ).empty();
              $UnitObject.each(function(index, element) {
                $( "li.price ul.sub-price" ).append('<li><input name="price" id="'+$(element).find('Value').text()+'"  value="'+$(element).find('Text').text()+'"  type="checkbox" ><label for="'+$(element).find('Value').text()+'"><span> $'+$(element).find('Text').text().split('.00').join("").replace(' - ',' - $')+'</span></label></li>');
            });
			var lastPrice = $( "li.price ul.sub-price li:last input").val().split('-')[1].trim().split('.00').join("");
			$( "li.price ul.sub-price" ).append('<li><input name="price" id="lastPrice"  value="'+lastPrice+'-'+(lastPrice*100)+'"  type="checkbox" ><label for="lastPrice"><span> > $'+lastPrice+'</span></label></li>');
		         $("#my-loader").hide();
             bindEvents();
          });   
          // Binding Pricelist using PickList method of RealPage (over) 
		  		
        //checkbox will select only one value at a time

      // List call to retrieve data on ready 
       
      getData(0,0,0,'');
 		

		

	$("#suite-selector input[type=checkbox]").click(function(){
		if($(this).attr('checked')){
			$('.'+$(this).attr('rel')).css('opacity','1');
		}else {
			$('.'+$(this).attr('rel')).css('opacity','0');
		}
	})
	
	$("li.ss-main").hover(function(){
    $("li.ss-main").removeClass('ss-show-sub');
    $(this).toggleClass('ss-show-sub');	
    $(this).hover(function(){ $(this).toggleClass('ss-show-sub'); });
	});	
});
 

 
 
function mapData(bedVal,bathVal,priceVal,dateNeeded,click){
  $.ajax({
    method: "POST",
    url: "http://crestatparkcentral.com/wp-content/themes/workhorse-boilerplate/park-soap.php",
    data: { bed: bedVal , bath: bathVal, price:priceVal, dateNeededAPI:dateNeeded, click:click },
      beforeSend: function(){
        $("#my-loader").show();
      }
  }).done(function(xml) {
    //jQuery("#xmlList").html(response);
     xmlDoc = $.parseXML( xml ),
      $xml = $( xmlDoc ),
      $UnitObject = $xml.find( "UnitObject" );
      $( "#list-apart" ).empty();
      //alert($UnitObject.length);
      var hightlight = "";
      var i = 0;
      $UnitObject.each(function(index, element) {
      var tempid = $(element).find('UnitNumber').text();
      var floorplanid = $(element).find('FloorPlan').find('FloorPlanID').text();
		var floorplancode = $(element).find('FloorPlan').find('FloorPlanCode').text();	
		var availableDate = $(element).find('Availability').find('AvailableDate').text();
		var madeReadyDate = $(element).find('Availability').find('MadeReadyDate').text();	
		var availableDateBit = $(element).find('Availability').find('AvailableBit').text();
		var madeReadyDateBit = $(element).find('Availability').find('MadeReadyBit').text();	
      //alert(tempid);
      if($.inArray(tempid,click)!='-1')
      {
        $( "#list-apart" ).append('<li class="'+$(element).find('Address').find('UnitNumber').text().slice(-4)+'" data-price="'+$(element).find('BaseRentAmount').text()+'" data-availabledate="'+availableDate+'" data-madereadydate="'+madeReadyDate+'" data-availabledatebit="'+availableDateBit+'" data-madeReadydatebit="'+madeReadyDateBit+'" ><span>'+$(element).find('Address').find('Address1').text()+ ', ' + floorplancode + ' Cond ' + floorplanid +', Floor - '+$(element).find('UnitDetails').find('FloorNumber').text()+'</span>'+$(element).find('UnitDetails').find('Bedrooms').text()+' bed, '+$(element).find('UnitDetails').find('Bathrooms').text()+' bath, '+$(element).find('UnitDetails').find('RentSqFtCount').text()+' sqft, '+'Starting at $'+$(element).find('BaseRentAmount').text().replace('.0000','')+' <a href="javascript:void(0);" class="preLease" data-unitid="'+$(element).find('Address').find('UnitID').text()+'">Pre-lease now</a>'+'</li>');
      }
      filterViews();
    });

      var listapartLength = $("#list-apart li").length;
      if(listapartLength == 0)
      {
      	$( "#list-apart" ).append('<li>These units are not available.<br/><a href="'+window.location.href+'">Click here</a> to view all available units');
      	$(".available-unit-ids").html("").hide();
      }
      $("#my-loader").hide();
      //console.log('classes to make active');
      //console.log(hightlight/*.slice(0,-1)*/);
      //console.log(click);
      //click.shift();
 	  setActive(click);     
    });       
}
 
function setActive(click){	
	//setTimeout(function(){
		$("div.single-block div.suite").each(function(){
    		//$(this).removeClass('active');
	  	});	  	 

		for(var i=0;i<click.length;i++)
		{
			var className = click[i];	
			var clstr = 'div.single-block div.suite.b-'+className;
			//console.log(clstr);
			$(clstr).addClass('active');
		}
	//},1500);
}

function getData(bedVal,bathVal,priceVal,dateNeeded){	
	var click = [];
	$.ajax({
	  method: "POST",
	  url: "http://crestatparkcentral.com/wp-content/themes/workhorse-boilerplate/park-soap.php",
	  data: { bed: bedVal , bath: bathVal, price:priceVal, dateNeededAPI:dateNeeded},
      beforeSend: function(){
        $("#my-loader").show();
      }
	}).done(function(xml) {
		//jQuery("#xmlList").html(response);
		
		 xmlDoc = $.parseXML( xml ),
		  $xml = $( xmlDoc ),
		  $UnitObject = $xml.find( "UnitObject" );
		  $( "#list-apart" ).empty();
		  //alert($UnitObject.length);
		  var hightlight = "";
		  var i = 0;
		  $UnitObject.each(function(index, element) {
		  	
		  	var newClass = $(element).find('Address').find('UnitNumber').text().slice(-4);


		  	var floorplanid = $(element).find('FloorPlan').find('FloorPlanID').text();
		  	var floorplancode = $(element).find('FloorPlan').find('FloorPlanCode').text();		  	
		  	var availableDate = $(element).find('Availability').find('AvailableDate').text();
		  	var madeReadyDate = $(element).find('Availability').find('MadeReadyDate').text();	
			var availableDateBit = $(element).find('Availability').find('AvailableBit').text();
			var madeReadyDateBit = $(element).find('Availability').find('MadeReadyBit').text();
		  	newClass += " " + getLiClass($(element).find('Address').find('UnitNumber').text());

			$( "#list-apart" ).append('<li class="'+newClass+'" data-price="'+$(element).find('BaseRentAmount').text()+'" data-availabledate="'+availableDate+'" data-madereadydate="'+madeReadyDate+'" data-availabledatebit="'+availableDateBit+'" data-madeReadydatebit="'+madeReadyDateBit+'"><span>'+$(element).find('Address').find('Address1').text()+ ', ' + floorplancode + ' Cond ' + floorplanid +', Floor - '+$(element).find('UnitDetails').find('FloorNumber').text()+'</span>'+$(element).find('UnitDetails').find('Bedrooms').text()+' bed, '+$(element).find('UnitDetails').find('Bathrooms').text()+' bath, '+$(element).find('UnitDetails').find('RentSqFtCount').text()+' sqft, '+'Starting at $'+$(element).find('BaseRentAmount').text().replace('.0000','')+' <a href="javascript:void(0);" class="preLease" data-unitid="'+$(element).find('Address').find('UnitID').text()+'">Pre-lease now</a>'+'</li>');
			//console.log($(element).find('UnitNumber').text());
			//var unitids = $(element).find('Address').find('UnitNumber').text();
			//console.log(unitids.slice(-4));
			//unitids = unitids.match(/#-(\d*)/)[1];			
			newClass = "";
			click.push($(element).find('UnitNumber').text());			
			filterViews();
		});
      $("#my-loader").hide();
      //console.log(click);
      if(click.length == 0)
      {
      	$( "#list-apart" ).append('<li>There are no available units that match your request. Please <a href="'+window.location.href+'">click here</a> to see all available units.');
      }
      setActive(click);
      $("#fullhover .suite").each(function(){
 			var classname=$(this).attr("class");
 			classname = classname.split(" "); 			 			
			imgName = classname[1];			
			var availableUnitids = "";
			$("#"+imgName+" .active").each(function(){					
				var unitid = $(this).attr("class").match(/b-(\d*)/g);						
				for($i=0;$i<unitid.length;$i++)
				{
					availableUnitids += unitid[$i].match(/b-(\d*)/)[1] + ", ";
				}			
			});
			if(availableUnitids.length > 0)
			{
				//$("."+imgName).attr("data-hint","Available Units : " + rtrim(availableUnitids,", ")); 			
				$("."+imgName).attr("data-hint","Click to see available units.");
			}
			else
			{
				$("."+imgName).attr("data-hint","This building is unavailable.");	
			}
 		});
	  });       
}
   	


	/* Views filter logic */
	function filterViews(){
		
		var click=[];
		if(jQuery( 'ul.sub-view li input[name=view]:checked').length){
			var viewVal = jQuery( 'ul.sub-view li input[name=view]:checked' ).val();
			$( "#list-apart li" ).not('.hiddenPrice').hide().addClass('hiddenView');
			$('li.view span.title').text(viewVal);
			$( "#list-apart li."+viewVal ).not('.hiddenPrice').show().removeClass('hiddenView');
			$( "#list-apart li:visible span").each(function(){
				var cont = $(this).text();
				var index = cont.indexOf("#");
				var flat = cont.substr(index+1,4);
				
				click.push(flat);
				
				setActive(click);     
			});
			$("#removeMsg").remove();
			if(click.length == 0)
		    {		   
		    	 	
		    	$( "#list-apart" ).append('<li id="removeMsg">There are no available units that match your request. Please <a href="'+window.location.href+'">click here</a> to see all available units.');
		    }
		}else{
			$('li.view span.title').text('view');
      		$( "#list-apart li.hiddenView" ).show().removeClass('hiddenView');

		}
		filterPrice();
	}
	function filterPrice(){
		var click = [];
		if(jQuery( 'ul.sub-price li input[name=price]:checked').length){
			var priceVal = jQuery( 'ul.sub-price li input[name=price]:checked' ).val().split('-');
			var minPrice = parseFloat(priceVal[0].trim());
			var maxPrice = parseFloat(priceVal[1].trim());
			$( '#list-apart li').not('.hiddenView').hide().addClass('hiddenPrice');
			var hightlight = "";
			$( '#list-apart li.hiddenPrice').each(function(i,e){
				if(parseFloat($(this).attr('data-price')) > minPrice && parseFloat($(this).attr('data-price')) <= maxPrice){
					var line = $(this).text();
					var index = line.indexOf('#');
					var unitid = line.substr(index+1,4);
					click.push(unitid);					
					$(this).show().removeClass('hiddenPrice');	
				}
			});
			 
			//alert($.type(click));
			//alert(click);
			$("#removeMsg").remove();

			if(click.length == 0) 
		    {	   	
		    	
		    	$( "#list-apart" ).append('<li id="removeMsg">There are no available units that match your request. Please <a href="'+window.location.href+'">click here</a> to see all available units.');
		    }	
			setActive(click);

		}else{	
			$( '#list-apart li.hiddenPrice').show().removeClass('hiddenPrice');
		}
		
	}
	
	function getLiClass(unitNumber){
		var liClass = "";
		$.each(views,function(k,v){
			var numbers = v.split(',');
			if($.inArray(unitNumber,numbers) > -1){
				liClass += k+" ";
			}
		});
		return liClass;
	}
	
	
  function bindBoxClicks(element){
	   var $box = $(element);
		if ($box.is(":checked")) {
			var group = "input:checkbox[name='" + $box.attr("name") + "']";
			$(group).prop("checked", false);
			$box.prop("checked", true);
		} else {
			$box.prop("checked", false);
		}
  }
	
    
    /* bind events */
     
	function bindEvents(){
        
        //checkbox will select only one value at a time
        $("ul.sub-bed li input:checkbox").on('click', function() {
           bindBoxClicks($(this));
        });
        $("ul.sub-bath li input:checkbox").on('click', function() {
            bindBoxClicks($(this));
        });
        $("ul.sub-view li input:checkbox").on('click', function() {
            bindBoxClicks($(this));
			
			filterViews();
        });
		$("ul.sub-price li input:checkbox").on('click', function() {
           bindBoxClicks($(this));
		   filterPrice();
		});
   
        jQuery('ul.suite-filter li input[type=checkbox]').change(function(){
            var priceVal = 0;
			var bathVal = 0;
			var bedVal = 0;
			var neededMoveDate = $("#datepicker").val();
			var neededMoveDateArray = neededMoveDate.split('/');
			neededMoveDate = neededMoveDateArray[2]+"-"+neededMoveDateArray[0]+"-"+neededMoveDateArray[1];
			// if(jQuery('ul.sub-price li input[name=price]:checked').length){
			// 	priceVal = jQuery('ul.sub-price li input[name=price]:checked').val();
			// }



			//set timeout for clearing previous selection on click event above
			setTimeout(function(){ 
					if(jQuery('ul.sub-bath li input[name=bath]:checked').length){
		            	bathVal = jQuery('ul.sub-bath li input[name=bath]:checked').val();
		            	//alert(bathVal);
					}
					if(jQuery('ul.sub-bed li input[name=bed]:checked').length){
		            	bedVal = jQuery('ul.sub-bed li input[name=bed]:checked').val();
		            	//alert(bedVal);
					}
					 
		      		var bathText="";
					if(!bathVal)
					{
						bathText = "0+ Baths";
					}
					else if(bathVal == "1")
					{
						bathText = bathVal+" Bath";
					}
					else
					{
						bathText = bathVal+" Baths";
					}
					jQuery("li.bath span.title").text(bathText);
					
					
					var bedText="";
					if(!bedVal)
					{
						bedText = "0+ Beds";
					}
					else if(bedVal == "1")
					{
						bedText = bedVal+" Bed";
					}
					else
					{
						bedText = bedVal+" Beds";
					}
					jQuery("li.bed span.title").text(bedText);
		            
					jQuery("li.ss-show-sub").removeClass('ss-show-sub');    
					console.log(bedVal+" "+bathVal+" "+priceVal+" "+neededMoveDate);
		            getData(bedVal,bathVal,priceVal,neededMoveDate);
        	}, 300);
             
        }); 
    } 
    /* bind PRICE events */       
    
    
    equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
    equalheight('.equalheight');
    var selectorHeight = $('#suite-selector #map-image').height();
    $('.appartment-list-sec').css("min-height", selectorHeight-30);

});


$(window).resize(function(){
    equalheight('.equalheight');
    var selectorHeight = $('#suite-selector #map-image').height();
    $('.appartment-list-sec').css("min-height", selectorHeight-30);

});

var imgName="";
var myClick = ""; 
function rtrim(str, chr) {
  var rgxtrim = (!chr) ? new RegExp('\\s+$') : new RegExp(chr+'+$');
  return str.replace(rgxtrim, '');
}
$(document).ready(function(){
	$('div.restore-map a').hide();
	$('div.map-sec div#fullhover div').click(function(){
		var myClass = $(this).attr('class');
		imgName = myClass.substring(6);
		var availableUnitids = "";
		$("#"+imgName+" .active").each(function(){	
			var unitid = $(this).attr("class").match(/b-(\d*)/g);		
			for($i=0;$i<unitid.length;$i++)
			{
				availableUnitids += unitid[$i].match(/b-(\d*)/)[1] + ", ";
			}			
		});
		if(availableUnitids.length > 0)
		{
			$(".available-unit-ids").html("The apartments that are highlighted are available.").show();		
		}
		$('div.map-sec div').not( ".available-unit-ids" ).hide();
		$('.full-map').fadeOut('slow');
		setTimeout(function(){ $('div#'+imgName).fadeIn('slow'); },500);
		$('div#'+imgName+' div.suite').show();
		setTimeout(function(){ $('div.restore-map a').show(); },1000);
         equalheight('.equalheight');
        
        var leftHeight = $(".equalheight ").height();
        $(".mCustomScrollbar").height(leftHeight-88);
        
		$("#"+imgName+" div.suite").unbind('click');
		$("#"+imgName+" div.suite").click(function(){

			var ar = [];		 
			var cname = jQuery(this).attr('class');
			ar = cname.split(" "); 
			//alert(ar); 
			for(var i=0; i<ar.length;i++)
			{
				ar[i] = ar[i].substr(2);
			}
			ar.shift();ar.shift();ar.shift();
			//alert(ar); 
			var click = ar;
			var unitid = $(this).attr("class").match(/b-(\d*)/g);	
			availableUnitids = "";	
			for($i=0;$i<unitid.length;$i++)
			{
				availableUnitids += unitid[$i].match(/b-(\d*)/)[1] + ", ";
			}			
			//$(".available-unit-ids").html("Available Units : " + rtrim(availableUnitids,', ')).show();			
			$(".available-unit-ids").html("The apartments that are highlighted are available.").show();
			console.log("Map Clicked");
			mapData(0,0,0,'',click);
		});			
	});
  	
  	$('div.restore-map a').click(function(){
        
        		                
		$('div.map-sec div.single-block').fadeOut('slow');
		setTimeout(function(){ $('div.map-sec img.full-map').fadeIn('slow'); },500);
		//setTimeout(function(){ $('div.map-sec div#fullhover div.suite').show(); },1000);
		$('div.map-sec div').show();
		$('div.map-sec div.single-block').hide();
		$('div.restore-map a').hide();
		getData(0,0,0,'');
        
        $(".available-unit-ids").html("").hide();
        $(".mCustomScrollbar").height(645);
        setTimeout(function(){
          equalheight('.equalheight'); 
        }, 500);
        
  	});
});

</script>	
	<script src="<?php bloginfo('template_directory'); ?>/_js/jquery.mCustomScrollbar.concat.min.js"></script>
	<script>
		//var ar = [];	
		//jQuery(document).ready(function(){
 		/*var cname = jQuery(this).attr('class');
		ar = cname.split(" "); 
		//alert(ar); 
		for(var i=0; i<ar.length;i++)
		{
			ar[i] = ar[i].substr(2);
		}
		ar.shift();ar.shift();
		//alert(ar); 
		var click = ar;
		mapData(0,0,0,click);*/
		//});
	</script>

<?php /*start code for prelease */ ?>
<script>
	$(document).ready(function(){	
		$( "#datepicker" ).datepicker({minDate: +1,});
		$("#datepicker").datepicker("setDate", "1");	
		$('body').on('click','.preLease',function(){
			var unitid = $(this).data("unitid");
			var MoveDate = $("#datepicker").val();
			var deepLink = "http://crestatparkcentral.com/lease/?MoveInDate="+MoveDate+"&UnitId="+ unitid + "&SearchUrl=https%3A//crestatparkcentral.com/#k=98788";			
			window.location = deepLink;
		});
		$('body').on('change','#datepicker',function(){
			var priceVal = 0;
			var bathVal = 0;
			var bedVal = 0;
			var neededMoveDate = $("#datepicker").val();
			var neededMoveDateArray = neededMoveDate.split('/');
			neededMoveDate = neededMoveDateArray[2]+"-"+neededMoveDateArray[0]+"-"+neededMoveDateArray[1];
			// if(jQuery('ul.sub-price li input[name=price]:checked').length){
			// 	priceVal = jQuery('ul.sub-price li input[name=price]:checked').val();
			// }



			//set timeout for clearing previous selection on click event above
			//setTimeout(function(){ 
					if(jQuery('ul.sub-bath li input[name=bath]:checked').length){
		            	bathVal = jQuery('ul.sub-bath li input[name=bath]:checked').val();
		            	//alert(bathVal);
					}
					if(jQuery('ul.sub-bed li input[name=bed]:checked').length){
		            	bedVal = jQuery('ul.sub-bed li input[name=bed]:checked').val();
		            	//alert(bedVal);
					}
					 
		      		var bathText="";
					if(!bathVal)
					{
						bathText = "0+ Baths";
					}
					else if(bathVal == "1")
					{
						bathText = bathVal+" Bath";
					}
					else
					{
						bathText = bathVal+" Baths";
					}
					jQuery("li.bath span.title").text(bathText);
					
					
					var bedText="";
					if(!bedVal)
					{
						bedText = "0+ Beds";
					}
					else if(bedVal == "1")
					{
						bedText = bedVal+" Bed";
					}
					else
					{
						bedText = bedVal+" Beds";
					}
					jQuery("li.bed span.title").text(bedText);
		            
					jQuery("li.ss-show-sub").removeClass('ss-show-sub');    
					console.log(bedVal+" "+bathVal+" "+priceVal+" "+neededMoveDate);
		            getData(bedVal,bathVal,priceVal,neededMoveDate);
        	//}, 300);
			/*var changedDate = $(this).val(); // Get the changed date
			var findOneProperty = 0;
			//var changedDate = "6/26/2015";	
			console.log("findone " +findOneProperty);
			$('.appartment-list li').show();
			$('.appartment-list li').each(function(){
				var singleAvailableDate = $(this).attr('data-availabledate');
				var singleAvailableDateArray = singleAvailableDate.split('/');
				
				if( singleAvailableDateArray[0].length < 2 )
				{
					singleAvailableDateArray[0] = '0'+singleAvailableDateArray[0];
				}
				if( singleAvailableDateArray[1].length < 2 )
				{
					singleAvailableDateArray[1] = '0'+singleAvailableDateArray[1];
				}
				var singleAvailableDateBit = $(this).attr('data-availabledatebit');
				var singleMadeReadyDate = $(this).attr('data-madereadydate');
				var singleMadeReadyDateArray = singleMadeReadyDate.split('/');
	
				if( singleMadeReadyDateArray[0].length < 2 )
				{
					singleMadeReadyDateArray[0] = '0'+singleMadeReadyDateArray[0];
				}
				if( singleMadeReadyDateArray[1].length < 2 )
				{
					singleMadeReadyDateArray[1] = '0'+singleMadeReadyDateArray[1];
				}
				var singleMadeReadyDateBit = $(this).attr('data-madeReadydatebit');
				singleAvailableDate = singleAvailableDateArray.join('/');
				singleMadeReadyDate = singleMadeReadyDateArray.join('/');
				
				if( (changedDate >= singleAvailableDate || changedDate >= singleMadeReadyDate ) && singleAvailableDateBit == "true" )
				{
					$('#removeMsg').remove();
					$(this).show();
					findOneProperty = 1;
				}
				else
				{
					$(this).hide();
				}
				
			});

			if( findOneProperty != 0 )
			{
				$('#removeMsg').remove();
				console.log("In If");
			}
			else
			{
				console.log("In Else");
				$('#removeMsg').remove();
				$('.appartment-list li').hide();
				$( "#list-apart" ).append('<li id="removeMsg">There are no available units that match your request. Please <a href="javascript:void(0);" class="removeChangeDateFilter">click here</a> to see all available units.');
			}*/
			/*if( $(".appartment-list li[data-availabledate='"+changedDate+"']").length > 0 ) 
			{
				//console.log( $(".appartment-list li[data-availabledate='"+changedDate+"']").length);
				$('#removeMsg').remove();
				$('.appartment-list li').not("[data-availabledate='"+changedDate+"']").hide();
			}
			else
			{
				//console.log("There are no available units that match your request.");
				$('#removeMsg').remove();
				$('.appartment-list li').hide();
				$( "#list-apart" ).append('<li id="removeMsg">There are no available units that match your request. Please <a href="javascript:void(0);" class="removeChangeDateFilter">click here</a> to see all available units.');
			}*/
			
		});
		/*$('body').on('click','.removeChangeDateFilter',function(){
			$('#removeMsg').remove();
			$('.appartment-list li').show();
		});*/
	});
</script>
<?php /* end code for prelease */ ?>
    
<?php get_footer(); ?>	