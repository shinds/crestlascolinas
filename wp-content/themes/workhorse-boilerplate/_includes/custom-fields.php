<?php

if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_contact',
		'title' => 'Contact',
		'fields' => array (
			array (
				'key' => 'field_53a9e2b5d0b6a',
				'label' => 'Send to',
				'name' => 'send_to',
				'type' => 'text',
				'instructions' => 'Separate multiple email addresses with a comma.',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_53a9e2c9d0b6b',
				'label' => 'Thank you message',
				'name' => 'thank_you_message',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-contact.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
						array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' =>  'page-contact-new.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_contact-info',
		'title' => 'Contact Info',
		'fields' => array (
			array (
				'key' => 'field_54a42d2b8a8d0',
				'label' => 'Street Address',
				'name' => 'street_address',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_54a42d388a8d1',
				'label' => 'City, ST ZIP',
				'name' => 'city_st_zip',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_54a42d538a8d2',
				'label' => 'Phone #',
				'name' => 'phone_#',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_54a42f041402e',
				'label' => 'Google Map Link',
				'name' => 'google_map_link',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_54a4309b0a7aa',
				'label' => 'Google Map Embed Link',
				'name' => 'google_map_embed_link',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_54a43637cca0e',
				'label' => 'Property Longitude',
				'name' => 'property_longitude',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_54a43647cca0f',
				'label' => 'Property Latitude',
				'name' => 'property_latitude',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_54a439f3c149b',
				'label' => 'Map Icon',
				'name' => 'map_icon',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_55524e2269a5a',
				'label' => 'Renters Voice',
				'name' => 'renters_voice',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_specials',
		'title' => 'Specials',
		'fields' => array (
			array (
				'key' => 'field_546e70fc9d796',
				'label' => 'Content Left Specials',
				'name' => 'content_left_specials',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_546e71239d798',
				'label' => 'Content Right Specials',
				'name' => 'content_right_specials',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-specials.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_features',
		'title' => 'Features',
		'fields' => array (
			array (
				'key' => 'field_546e70fc9d795',
				'label' => 'Content Left List One',
				'name' => 'content_left_list_one',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_546e710d9d796',
				'label' => 'Content Left List Two',
				'name' => 'content_left_list_two',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_546e71239d797',
				'label' => 'Content Right',
				'name' => 'content_right',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-features.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_footer-logo',
		'title' => 'Footer Logo',
		'fields' => array (
			array (
				'key' => 'field_54985d4aa266a',
				'label' => 'Footer Logo',
				'name' => 'footer_logo',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_home-page',
		'title' => 'Home Page',
		'fields' => array (
			array (
				'key' => 'field_546cea91b781c',
				'label' => 'Content Left',
				'name' => 'content_left',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_546ceab2b781d',
				'label' => 'Content Right',
				'name' => 'content_right',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
			array (
				'key' => 'field_546ceabdb781e',
				'label' => 'Content Full Width',
				'name' => 'content_full_width',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-home.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_home-photos',
		'title' => 'Home Photos',
		'fields' => array (
			array (
				'key' => 'field_5474af522d969',
				'label' => 'Photo',
				'name' => 'photo',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_5474af9e6c3e3',
						'label' => 'Image',
						'name' => 'image',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'id',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-home.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_home-photos-bubble-slider',
		'title' => 'Home Photos - Bubble Slider',
		'fields' => array (
			array (
				'key' => 'field_548717ee4487e',
				'label' => 'Bubble Slider',
				'name' => 'bubble_slider',
				'type' => 'repeater',
				'instructions' => 'Add 3 photos 500px by 500px',
				'sub_fields' => array (
					array (
						'key' => 'field_548718214487f',
						'label' => 'Bubble Photo',
						'name' => 'bubble_photo',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'id',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
				),
				'row_min' => 3,
				'row_limit' => 3,
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-home.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_home-welcome-banner',
		'title' => 'Home Welcome Banner',
		'fields' => array (
			array (
				'key' => 'field_54999c8402214',
				'label' => 'Hero Welcome Banner',
				'name' => 'hero_welcome_banner',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_hot-spot-categories',
		'title' => 'Hot Spot Categories',
		'fields' => array (
			array (
				'key' => 'field_547f38acc58fc',
				'label' => 'Pin Image',
				'name' => 'pin_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'ef_taxonomy',
					'operator' => '==',
					'value' => 'hot_spot_categories',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_hot-spots',
		'title' => 'Hot Spots',
		'fields' => array (
			array (
				'key' => 'field_547f379a96bb2',
				'label' => 'Street Address',
				'name' => 'street_address',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_547f37d096bb3',
				'label' => 'City',
				'name' => 'city',
				'type' => 'text',
				'default_value' => 'Dallas',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_547f37d996bb4',
				'label' => 'State',
				'name' => 'state',
				'type' => 'select',
				'choices' => array (
					'AL' => 'Alabama',
					'AK' => 'Alaska',
					'AZ' => 'Arizona',
					'AR' => 'Arkansas',
					'CA' => 'California',
					'CO' => 'Colorado',
					'CT' => 'Connecticut',
					'DE' => 'Delaware',
					'DC' => 'District Of Columbia',
					'FL' => 'Florida',
					'GA' => 'Georgia',
					'HI' => 'Hawaii',
					'ID' => 'Idaho',
					'IL' => 'Illinois',
					'IN' => 'Indiana',
					'IA' => 'Iowa',
					'KS' => 'Kansas',
					'KY' => 'Kentucky',
					'LA' => 'Louisiana',
					'ME' => 'Maine',
					'MD' => 'Maryland',
					'MA' => 'Massachusetts',
					'MI' => 'Michigan',
					'MN' => 'Minnesota',
					'MS' => 'Mississippi',
					'MO' => 'Missouri',
					'MT' => 'Montana',
					'NE' => 'Nebraska',
					'NV' => 'Nevada',
					'NH' => 'New Hampshire',
					'NJ' => 'New Jersey',
					'NM' => 'New Mexico',
					'NY' => 'New York',
					'NC' => 'North Carolina',
					'ND' => 'North Dakota',
					'OH' => 'Ohio',
					'OK' => 'Oklahoma',
					'OR' => 'Oregon',
					'PA' => 'Pennsylvania',
					'RI' => 'Rhode Island',
					'SC' => 'South Carolina',
					'SD' => 'South Dakota',
					'TN' => 'Tennessee',
					'TX' => 'Texas',
					'UT' => 'Utah',
					'VT' => 'Vermont',
					'VA' => 'Virginia',
					'WA' => 'Washington',
					'WV' => 'West Virginia',
					'WI' => 'Wisconsin',
					'WY' => 'Wyoming',
				),
				'default_value' => 'TX',
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'field_547f384596bb5',
				'label' => 'Zip Code',
				'name' => 'zip_code',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_547f8eec2c09e',
				'label' => 'Phone',
				'name' => 'phone',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_547f386796bb7',
				'label' => 'Latitude',
				'name' => 'latitude',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_547f384f96bb6',
				'label' => 'Longitude',
				'name' => 'longitude',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_547f387696bb8',
				'label' => 'Website',
				'name' => 'website',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'hot_spot',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_photos',
		'title' => 'Photos',
		'fields' => array (
			array (
				'key' => 'field_54737caecb194',
				'label' => 'Photo',
				'name' => 'photo',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_54737cc8cb196',
						'label' => 'Image',
						'name' => 'image',
						'type' => 'image',
						'column_width' => '',
						'save_format' => 'url',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page_template',
					'operator' => '==',
					'value' => 'page-gallery.php',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_social-links',
		'title' => 'Social Links',
		'fields' => array (
			array (
				'key' => 'field_547dfdbcf3b37',
				'label' => 'Social Links',
				'name' => 'social_links',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_547dfdc9f3b38',
						'label' => 'Social Network',
						'name' => 'social_network',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_547dfdd1f3b39',
						'label' => 'Font Awesome Code',
						'name' => 'font_awesome_code',
						'type' => 'text',
						'instructions' => 'See Cheatsheet: http://fortawesome.github.io/Font-Awesome/cheatsheet/',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_547dfdfbf3b3a',
						'label' => 'Link',
						'name' => 'link',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
				),
				'row_min' => 0,
				'row_limit' => '',
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_floorplans',
		'title' => 'Floorplans',
		'fields' => array (
			array (
				'key' => 'field_53a9e30533898',
				'label' => 'Floorplan type (1 Bedroom, for example)',
				'name' => 'floorplan_type',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_53a9e32633899',
						'label' => 'Floorplan Type',
						'name' => 'floorplan_type',
						'type' => 'text',
						'column_width' => '',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_53a9e77c3389a',
						'label' => 'Floorplan',
						'name' => 'floorplan',
						'type' => 'repeater',
						'column_width' => '',
						'sub_fields' => array (
							array (
								'key' => 'field_53a9e7973389b',
								'label' => 'Name',
								'name' => 'name',
								'type' => 'text',
								'column_width' => 15,
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_5486cd95d2bdd',
								'label' => 'View Availability',
								'name' => 'view_availability',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_53a9e7b73389c',
								'label' => 'BR#',
								'name' => 'number_of_bedrooms',
								'type' => 'text',
								'column_width' => 5,
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_53a9e7d93389d',
								'label' => 'BA#',
								'name' => 'number_of_baths',
								'type' => 'text',
								'column_width' => 5,
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_53b3177dcd08c',
								'label' => 'sq feet',
								'name' => 'sq_feet',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_54876d15f341d',
								'label' => 'Application Fee',
								'name' => 'application_fee',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_54876d1bf341e',
								'label' => 'Deposit',
								'name' => 'deposit',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_549347f7db4ec',
								'label' => 'Feature',
								'name' => 'feature',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_53b317aecd08d',
								'label' => 'Stories',
								'name' => 'stories',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'html',
								'maxlength' => '',
							),
							array (
								'key' => 'field_53a9e7fb3389e',
								'label' => 'Modern Image',
								'name' => 'image',
								'type' => 'image',
								'column_width' => '',
								'save_format' => 'id',
								'preview_size' => 'thumbnail',
								'library' => 'all',
							),
							array (
								'key' => 'field_53b304f31a3f4',
								'label' => 'Fresh Image',
								'name' => '2nd_floor_image',
								'type' => 'image',
								'column_width' => '',
								'save_format' => 'id',
								'preview_size' => 'thumbnail',
								'library' => 'all',
							),
							array (
								'key' => 'field_53b32e03bc956',
								'label' => 'Other Image',
								'name' => '3rd_floor_image',
								'type' => 'image',
								'column_width' => '',
								'save_format' => 'id',
								'preview_size' => 'thumbnail',
								'library' => 'all',
							),
							array (
								'key' => 'field_5486f72c22774',
								'label' => 'Print PDF',
								'name' => 'print_pdf',
								'type' => 'text',
								'column_width' => '',
								'default_value' => '',
								'placeholder' => '',
								'prepend' => '',
								'append' => '',
								'formatting' => 'none',
								'maxlength' => '',
							),
							array (
								'key' => 'field_5528561ba7xx3',
								'label' => 'Hide',
								'name' => 'hide_floorplan',
								'type' => 'true_false',
								'message' => '',
								'default_value' => 0,
							),
						),
						'row_min' => '',
						'row_limit' => '',
						'layout' => 'row',
						'button_label' => 'Add Row',
					),
				),
				'row_min' => '',
				'row_limit' => '',
				'layout' => 'row',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'page',
					'operator' => '==',
					'value' => $bloginfo['floorplans_page'],
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 2,
	));
	
	
	register_field_group(array (
		'id' => 'acf_popup-options-page',
		'title' => 'Popup - Options Page',
		'fields' => array (
			array (
				'key' => 'field_551d5dcd1ce3e',
				'label' => 'Popup Image',
				'name' => 'popup_image',
				'type' => 'image',
				'save_format' => 'url',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_551d699748bf8',
				'label' => 'Popup Links To',
				'name' => 'popup_links_to',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_551d6bd374a4b',
				'label' => 'Popup Link Target',
				'name' => 'popup_link_target',
				'type' => 'true_false',
				'message' => '',
				'default_value' => 1,
			),
			array (
				'key' => 'field_551d698448bf7',
				'label' => 'Hide on Mobile',
				'name' => 'hide_on_mobile',
				'type' => 'true_false',
				'message' => '',
				'default_value' => 1,
			),
			array (
				'key' => 'field_551d63f082fac',
				'label' => 'Cookie Lasts For',
				'name' => 'cookie_lasts_for',
				'type' => 'number',
				'default_value' => 7,
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => '',
				'max' => '',
				'step' => '',
			),
			array (
				'key' => 'field_551d641269c9e',
				'label' => 'Popup Expires',
				'name' => 'popup_expires',
				'type' => 'date_picker',
				'date_format' => 'yymmdd',
				'display_format' => 'mm/dd/yy',
				'first_day' => 0,
			),
			array (
				'key' => 'field_551d6666e8ea6',
				'label' => 'Disable Cookie (For Testing)',
				'name' => 'disable_cookie',
				'type' => 'true_false',
				'message' => '',
				'default_value' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
	register_field_group(array (
		'id' => 'acf_hours_and_available_appointments',
		'title' => 'Hours and Available Appointments',
		'fields' => array (
			array (
				'key' => 'field_555e4829c9479',
				'label' => 'Days',
				'name' => 'days',
				'type' => 'repeater',
				'sub_fields' => array (
					array (
						'key' => 'field_555e4838c947a',
						'label' => 'Sunday',
						'name' => 'sunday',
						'type' => 'checkbox',
						'column_width' => '',
						'choices' => array (
							'10:00' => '10:00',
							'10:30' => '10:30',
							'11:00' => '11:00',
							'11:30' => '11:30',
							'12:00' => '12:00',
							'12:30' => '12:30',
							'1:00' => '1:00',
							'1:30' => '1:30',
							'2:00' => '2:00',
							'2:30' => '2:30',
							'3:00' => '3:00',
							'3:30' => '3:30',
							'4:00' => '4:00',
							'4:30' => '4:30',
							'5:00' => '5:00',
							'5:30' => '5:30',
						),
						'default_value' => '10:00
	10:30
	11:00
	11:30
	12:00
	12:30
	1:00
	1:30
	2:00
	2:30
	3:00
	3:30
	4:00
	4:30
	5:00
	5:30',
						'layout' => 'vertical',
					),
					array (
						'key' => 'field_555e4b987bc53',
						'label' => 'Monday',
						'name' => 'monday',
						'type' => 'checkbox',
						'column_width' => '',
						'choices' => array (
							'10:00' => '10:00',
							'10:30' => '10:30',
							'11:00' => '11:00',
							'11:30' => '11:30',
							'12:00' => '12:00',
							'12:30' => '12:30',
							'1:00' => '1:00',
							'1:30' => '1:30',
							'2:00' => '2:00',
							'2:30' => '2:30',
							'3:00' => '3:00',
							'3:30' => '3:30',
							'4:00' => '4:00',
							'4:30' => '4:30',
							'5:00' => '5:00',
							'5:30' => '5:30',
						),
						'default_value' => '10:00
	10:30
	11:00
	11:30
	12:00
	12:30
	1:00
	1:30
	2:00
	2:30
	3:00
	3:30
	4:00
	4:30
	5:00
	5:30',
						'layout' => 'vertical',
					),
					array (
						'key' => 'field_555e4b977bc52',
						'label' => 'Tuesday',
						'name' => 'tuesday',
						'type' => 'checkbox',
						'column_width' => '',
						'choices' => array (
							'10:00' => '10:00',
							'10:30' => '10:30',
							'11:00' => '11:00',
							'11:30' => '11:30',
							'12:00' => '12:00',
							'12:30' => '12:30',
							'1:00' => '1:00',
							'1:30' => '1:30',
							'2:00' => '2:00',
							'2:30' => '2:30',
							'3:00' => '3:00',
							'3:30' => '3:30',
							'4:00' => '4:00',
							'4:30' => '4:30',
							'5:00' => '5:00',
							'5:30' => '5:30',
						),
						'default_value' => '10:00
	10:30
	11:00
	11:30
	12:00
	12:30
	1:00
	1:30
	2:00
	2:30
	3:00
	3:30
	4:00
	4:30
	5:00
	5:30',
						'layout' => 'vertical',
					),
					array (
						'key' => 'field_555e4b967bc51',
						'label' => 'Wednesday',
						'name' => 'wednesday',
						'type' => 'checkbox',
						'column_width' => '',
						'choices' => array (
							'10:00' => '10:00',
							'10:30' => '10:30',
							'11:00' => '11:00',
							'11:30' => '11:30',
							'12:00' => '12:00',
							'12:30' => '12:30',
							'1:00' => '1:00',
							'1:30' => '1:30',
							'2:00' => '2:00',
							'2:30' => '2:30',
							'3:00' => '3:00',
							'3:30' => '3:30',
							'4:00' => '4:00',
							'4:30' => '4:30',
							'5:00' => '5:00',
							'5:30' => '5:30',
						),
						'default_value' => '10:00
	10:30
	11:00
	11:30
	12:00
	12:30
	1:00
	1:30
	2:00
	2:30
	3:00
	3:30
	4:00
	4:30
	5:00
	5:30',
						'layout' => 'vertical',
					),
					array (
						'key' => 'field_555e4b967bc50',
						'label' => 'Thursday',
						'name' => 'thursday',
						'type' => 'checkbox',
						'column_width' => '',
						'choices' => array (
							'10:00' => '10:00',
							'10:30' => '10:30',
							'11:00' => '11:00',
							'11:30' => '11:30',
							'12:00' => '12:00',
							'12:30' => '12:30',
							'1:00' => '1:00',
							'1:30' => '1:30',
							'2:00' => '2:00',
							'2:30' => '2:30',
							'3:00' => '3:00',
							'3:30' => '3:30',
							'4:00' => '4:00',
							'4:30' => '4:30',
							'5:00' => '5:00',
							'5:30' => '5:30',
						),
						'default_value' => '10:00
	10:30
	11:00
	11:30
	12:00
	12:30
	1:00
	1:30
	2:00
	2:30
	3:00
	3:30
	4:00
	4:30
	5:00
	5:30',
						'layout' => 'vertical',
					),
					array (
						'key' => 'field_555e4b957bc4f',
						'label' => 'Friday',
						'name' => 'friday',
						'type' => 'checkbox',
						'column_width' => '',
						'choices' => array (
							'10:00' => '10:00',
							'10:30' => '10:30',
							'11:00' => '11:00',
							'11:30' => '11:30',
							'12:00' => '12:00',
							'12:30' => '12:30',
							'1:00' => '1:00',
							'1:30' => '1:30',
							'2:00' => '2:00',
							'2:30' => '2:30',
							'3:00' => '3:00',
							'3:30' => '3:30',
							'4:00' => '4:00',
							'4:30' => '4:30',
							'5:00' => '5:00',
							'5:30' => '5:30',
						),
						'default_value' => '10:00
	10:30
	11:00
	11:30
	12:00
	12:30
	1:00
	1:30
	2:00
	2:30
	3:00
	3:30
	4:00
	4:30
	5:00
	5:30',
						'layout' => 'vertical',
					),
					array (
						'key' => 'field_555e4b937bc4e',
						'label' => 'Saturday',
						'name' => 'saturday',
						'type' => 'checkbox',
						'column_width' => '',
						'choices' => array (
							'10:00' => '10:00',
							'10:30' => '10:30',
							'11:00' => '11:00',
							'11:30' => '11:30',
							'12:00' => '12:00',
							'12:30' => '12:30',
							'1:00' => '1:00',
							'1:30' => '1:30',
							'2:00' => '2:00',
							'2:30' => '2:30',
							'3:00' => '3:00',
							'3:30' => '3:30',
							'4:00' => '4:00',
							'4:30' => '4:30',
							'5:00' => '5:00',
							'5:30' => '5:30',
						),
						'default_value' => '10:00
	10:30
	11:00
	11:30
	12:00
	12:30
	1:00
	1:30
	2:00
	2:30
	3:00
	3:30
	4:00
	4:30
	5:00
	5:30',
						'layout' => 'vertical',
					),
				),
				'row_min' => 1,
				'row_limit' => 1,
				'layout' => 'table',
				'button_label' => 'Add Row',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => 'acf-options',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
} ?>