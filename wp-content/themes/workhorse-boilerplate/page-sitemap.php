<?php 
/* Template Name: WordPress Sitemap */
?>
<?php get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			
			<?php //get_sidebar('Sitemap Menu'); ?>
 <div class="gutter">
	<div class="inner">
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<h1 class="entry-title"><?php the_title(); ?></h1>
					<ul>
  						<?php wp_list_pages('sort_column=menu_order&title_li='); ?>
					</ul>
				</article><!-- #post-## -->
	</div>
</div>	
	
<?php endwhile; ?>

<?php get_footer(); ?>